<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class VincularSector extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    { 
        Schema::create('vincular_sector', function (Blueprint $table) {
            $table->id('VINS_ID')->comment('Id de la tabla');
            $table->unsignedInteger('SECT_PRINCIPAL')->nullable()->comment('id del sector principal');
            $table->unsignedInteger('SECT_VINCULADO')->nullable()->comment('id del sector vinculado');
            $table->unsignedInteger('VINS_ACTIVO')->nullable()->comment('Estado del registro');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('vincular_sector');
    }
}
