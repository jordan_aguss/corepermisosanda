<div class="table-responsive col-12 m-0 p-0">
    <table class="table card-table table-vcenter text-nowrap " id="dataTable" width="100%" cellspacing="0">
        <thead class=" whiteSmoke">
            <tr class="small black">
                {{$header}}
            </tr>
        </thead>
        <tbody id="{{isset($IdTabla) ? $IdTabla : 'TablaEmpty'}}" >
            {{$body}}
        </tbody>
    </table>    
</div>    
