<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\ClasesExternas\MetodosGenerales;

class Notificacion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    { 
        Schema::create(MetodosGenerales::$Esquema . 'notificacion', function (Blueprint $table) {
            $table->id('NOTI_ID')->comment('ID DE LA TABLA');
            $table->unsignedInteger('EVAL_ID')->comment('id de evaluacion');
            $table->unsignedInteger('REFE_ID')->comment('id de referencia');
            $table->unsignedInteger('REFE_TIPO_TABLA')->comment('id de referencia');
            $table->string('NOMBRE_NOTIFICADOR')->comment('');
            $table->string('CARGO_NOTIFICADOR')->comment('');
            $table->string('NOTI_NOMBRE')->comment('');
            $table->string('NOTI_CORREO')->comment('');
            $table->string('IDENTIFICADOR_PROYECTO')->comment('');
            $table->unsignedInteger('TIPD_ID')->comment('');
            $table->datetime('NOTI_FECHADOC')->comment('');
            $table->string('NUMERO_REFERENCIA')->comment('');
            $table->string('NOTAS_ESTRACTO')->comment('');
            $table->datetime('NOTI_FECHA')->comment('');
            $table->string('EMAIL_ENVIADO_HTML')->comment('');
            $table->string('CONTENIDO_VINDULO_HTML')->comment('');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(MetodosGenerales::$Esquema . 'notificacion');
    }
}
