<?php

namespace App\Http\Controllers;

use App\Models\DetallePermiso;
use App\Models\Permiso;
use App\Models\Pregunta;
use App\Models\PreguntaSeccion;
use App\Models\Sucursal;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class AndaController extends Controller
{
    public function index(){

        $Info = new Sucursal();
        return view('anda.index', compact('Info'));
    }

    public function GetPreguntasForm($idForm, $codigo = ''){

        try{

            if(!isset($codigo)){
                throw new Exception('^No ha ingresado ningun codigo de referencia');
            }

            $permiso = Permiso::where('perm_codigo', $codigo)->first();

            if(!isset($permiso->perm_id)){
                $permiso = new Permiso();
                $permiso->usua_id = 1;
                $permiso->perm_fecha = date('Y-m-d');
                $permiso->form_id = $idForm;
                $permiso->esta_id = 0;
                $permiso->perm_codigo = $codigo;
                $permiso->save();
            }

            Session::put('nuevo_permiso', $permiso->perm_id);

            $Preguntas = $this->InfoPreguntas('s.secc_id')->join('seccion_formulario as sf', 'sf.secc_id', 's.secc_id')
            ->where('preg_padre', 1)->where('form_id', $idForm)->orderBy('preg_orden')->get();
            $hijas = true;

            return view('preguntas.form-preguntas', compact('Preguntas', 'hijas'));

        }catch(Exception $ex){
            return response()->json($ex->getMessage(), 500);
        }
    }

    public function UpdateOrden(){
        $ids = [34, 46, 49, 55, 58, 59, 62, 65, 71, 75, 73,76, 74, 145, 77, 78, 79, 80, 81, 82, 83,
        84, 85, 86, 87, 88, 89];

        for($i = 0; $i< count($ids); $i++){
            DB::update('update pregunta_seccion set preg_orden = ? where preg_id = ? and secc_id = 12', [$i, $ids[$i]]);
        }

    }

    private function InfoPreguntas($seccion = '', $select = [], $idpreg = 'pa.preg_id'){

        $info = array_merge(['p.*', DB::raw('if(isnull(pred_id), 0, 1) as DEPENDE'), 's.secc_id', 's.secc_nombre', 'perd_respuesta'], $select);

        return PreguntaSeccion::join('preguntas as p', 'p.preg_id', 'pregunta_seccion.preg_id')
        ->join('secciones as s', 's.secc_id', 'pregunta_seccion.secc_id')
        ->leftjoin('pregunta_accion as pa', $idpreg, 'p.preg_id')
        ->leftjoin('permiso_detalle as pd', function($join) use ($seccion){
             $join->on('pd.preg_id', 'p.preg_id');
              $join->where('perm_id', Session::get('nuevo_permiso'));
        })
        ->distinct()->select($info)->orderBy('s.secc_id');
    }

    public function GetPreguntasHijas($pregunta, $opcion, $seccion, $loading){

        $Preguntas = Pregunta::join('pregunta_accion as pa', 'pa.pred_id', 'preguntas.preg_id')->where('pa.preg_id', $pregunta)->where('pa.opci_id', $opcion)->get('*', '0 as secc_id');
        $hijas = true;

        if($loading){
            DB::delete('delete from permiso_detalle where preg_id in (select pred_id from pregunta_accion pa where preg_id = ? and opci_id != ?)
             and secc_id = ? and perm_id = ?', [$pregunta, $opcion, $seccion, Session::get('nuevo_permiso')]);
        }

        return view('preguntas.form-preguntas', compact('Preguntas', 'hijas'));
    }


    public function SavePreguntas(Request $request){


        foreach($request->campoPregunta as $id => $valor){
            try{
                $permiso = DetallePermiso::where('preg_id', $id)->where('perm_id', Session::get('nuevo_permiso'))->first();

                if(!isset($permiso->perm_id)){
                    $permiso = new DetallePermiso();
                    $permiso->preg_id = $id;
                    $permiso->perm_id = Session::get('nuevo_permiso');
                }

                $permiso->tipp_id = $request->tipo;
                $permiso->perd_respuesta = $valor;
                $permiso->secc_id = 1;
                $permiso->save();
            }catch(Exception $ex){
            }

        }
    }
}
