<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\ClasesExternas\MetodosGenerales;

class NumeroCorrelativo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    { 
        Schema::create(MetodosGenerales::$Esquema . 'numero_correlativo', function (Blueprint $table) {
            $table->id('NUMC_ID')->comment('ID DE LA TABLA');
            $table->string('NUMC_NOMBRE')->nullable()->comment('');
            $table->string('NUMC_INICIAL',20)->nullable()->comment('');
            $table->unsignedInteger('NUMC_ANIO')->nullable()->comment('');
            $table->unsignedInteger('NUMC_CORRELATIVO')->nullable()->comment('');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(MetodosGenerales::$Esquema . 'numero_correlativo');
    }
}
