<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\ClasesExternas\MetodosGenerales;

class SeccionFormulario extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    { 
        Schema::create(MetodosGenerales::$Esquema . 'seccion_formulario', function (Blueprint $table) {
            $table->id('SECF_ID')->comment('Id de la seccion');
            $table->string('SECF_NOMBRE')->comment('Nombre de la seccion');
            $table->unsignedInteger('SECF_ORDEN')->comment('Orden de aparicion');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(MetodosGenerales::$Esquema . 'seccion_formulario');
    }
}
