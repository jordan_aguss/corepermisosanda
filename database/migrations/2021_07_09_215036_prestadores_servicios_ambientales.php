<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class PrestadoresServiciosAmbientales extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    { 
        Schema::create('prestadores_servicios_ambientales', function (Blueprint $table) {
            $table->id();
            $table->datetime('FECHA_INGRESO')->nullable();
            $table->datetime('FECHA_SOLICITUD')->nullable();
            $table->datetime('fechaconst')->nullable();
            $table->text('NUMERO_REGISTRO',50)->nullable();
            $table->text('NOMBRE', 1000)->nullable();
            $table->text('GRADO_ACADEMICO', 1000)->nullable();
            $table->text('SEXO', 50)->nullable();
            $table->text('NUMERO_DOCUMENTO', 100)->nullable();
            $table->text('REPRESENTANTE_LEGAL', 50)->nullable();
            $table->text('NUMERO_DOCUMENTOR', 100)->nullable();
            $table->text('GIRO_ACTIVIDAD', 1000)->nullable();
            $table->text('NUMERO_ACTIVIDAD', 1000)->nullable();
            $table->text('DIRECCION_DOMICILIO', 1000)->nullable();
            $table->text('DIRECCION_NOTIFICACION', 1000)->nullable();
            $table->text('TELEFONO', 100)->nullable();
            $table->text('FAX', 100)->nullable();
            $table->text('E_MAIL', 100)->nullable();
            $table->text('NACIONALIDAD', 50)->nullable();
            $table->text('OBSERVACIONES', 1000)->nullable();
            $table->text('observ_Document', 4000)->nullable();
            $table->text('ACTUALIZODATOS', 10)->nullable();
            $table->unsignedInteger('TIPO_DOCUMENTO')->nullable();
            $table->unsignedInteger('TIPO_DOCUMENTOR')->nullable();
            $table->unsignedInteger('TPERSONA')->nullable();
            $table->unsignedInteger('NSOLICITUD')->nullable();
            $table->unsignedInteger('USUARIO')->nullable();
            $table->unsignedInteger('SELECCION')->nullable();
            $table->unsignedInteger('USUARIO_ACTUALIZO')->nullable();
            $table->unsignedInteger('PAR')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('prestadores_servicios_ambientales');
    }
}
