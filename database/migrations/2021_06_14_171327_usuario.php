<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\ClasesExternas\MetodosGenerales;

class Usuario extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
     { 
         Schema::create(MetodosGenerales::$Esquema . 'usuario', function (Blueprint $table) {
             $table->id('USUA_ID');
             $table->string('USUA_LOGIN',100);
             $table->string('USUA_PASS',100);
             $table->string('USUA_NOMBRE',100);
             $table->unsignedInteger('ROL_ID');
             $table->bigInteger('USUA_IDPADRE')->nullable();
             $table->unsignedInteger('USUA_ACTIVO');
             $table->string('TIPO_ID',1);
             $table->unsignedInteger('UNID_ID')->nullable();
             $table->unsignedInteger('OFIC_ID')->nullable();
             $table->unsignedInteger('USUA_CAMBIO_PASS')->nullable();
             $table->string('USUA_CADENA_VALIDACION',100)->nullable();
             $table->date('FECHA_INSERT');
             $table->string('CODIGO_EMPLEADO',7)->nullable();
         });
     }
 
     /**
      * Reverse the migrations.
      *
      * @return void
      */
     public function down()
     {
         Schema::drop(MetodosGenerales::$Esquema . 'usuario');
     }
}
