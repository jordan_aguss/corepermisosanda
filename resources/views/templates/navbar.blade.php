
    
    <nav class="topnav navbar navbar-expand shadow justify-content-between justify-content-sm-start navbar-light bg-white"
    id="sidenavAccordion">

        @can('agencia')
            <button class="btn btn-icon btn-transparent-dark order-1 order-lg-0 me-2 ms-lg-2 me-lg-0" id="sidebarToggle"><i
            data-feather="menu"></i></button>
         @endcan

        @if(Auth::user()->name != 'AGENCIA')
            <script>
               document.body.classList.toggle('sidenav-toggled');
               
            </script>
        <a class="navbar-brand pe-3 ps-4 ps-lg-2" href="#"> {{$Info->sucu_nombre}} </a>

        <a class="nav-link" id="permisoNavBar" type="button" onclick="LoadPage('{{route('permisos.index')}}')">Permisos</a>
      
               
        @endif

            

  

    <ul class="navbar-nav align-items-center ms-auto">

        <li class="nav-item dropdown no-arrow mx-1">
            <a class="nav-link colorSeaText" href="http://www.facebook.com/marn.gob.sv" target="_blank">
                <i class="fab fa-facebook-f"></i>
            </a>
        </li>

        <div class="topbar-divider"></div>

        <li class="nav-item dropdown no-arrow mx-1">
            <a class="nav-link colorSeaText" href="http://twitter.com/MARN_Oficial_SV" target="_blank">
                <i class="fab fa-twitter"></i>
            </a>
        </li>

        <div class="topbar-divider"></div>

        <li class="nav-item dropdown no-arrow mx-1">
            <a class="nav-link colorSeaText" href="http://www.youtube.com/MARNsv" target="_blank">
                <i class="fab fa-youtube"></i>
            </a>
        </li>


        <li class="nav-item no-caret d-none d-sm-block d-none me-3 ">
            Bienvenido/a <br> <b class="fw-bolder textGB"> {{Auth::user()->name}} </b>
        </li>

        <li class="nav-item dropdown no-caret dropdown-user me-3 me-lg-5 ">
            <a class="btn btn-icon btn-transparent-dark dropdown-toggle circle " id="navbarDropdownUserImage"
                href="javascript:void(0);" role="button" data-bs-toggle="dropdown" aria-haspopup="true"
                aria-expanded="false"><img class="img-fluid "
                    src="https://www.vhv.rs/dpng/d/436-4363443_view-user-icon-png-font-awesome-user-circle.png" /></a>
            <div class="dropdown-menu dropdown-menu-end border-0 shadow animated--fade-in-up"
                aria-labelledby="navbarDropdownUserImage">
                <h6 class="dropdown-header d-flex align-items-center">
                    <img class="dropdown-user-img "
                        src="https://www.vhv.rs/dpng/d/436-4363443_view-user-icon-png-font-awesome-user-circle.png" />
                    <div class="dropdown-user-details">
                        <div class="dropdown-user-details-name">{{Auth::user()->email}}
                        </div>
                </h6>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="{{ route('logout') }}">
                    <div class="dropdown-item-icon"><i type="submit" data-feather="log-out"></i></div>Salir
                    <form id="logoutForm" method="POST" action="{{ route('logout') }}" style="display: none;">
                        @csrf
                    </form>
                </a>
            </div>
        </li>

    </ul>

</nav>
