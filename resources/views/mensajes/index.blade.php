
    <div class="ml-5 mt-3 mb-5 mr-5 card-body bg-white shadow-lg">
        <div class="col-12 row">
            <div class="col-12 m-0 p-0 row mt-3 mb-3">
                <div class=" d-flex justify-content-between col-12">
                    <div class="font-weight-bold justify-content-start">Bandeja de mensajes</div>
                    <div class="font-weight-bold justify-content-end">{{count($Notificaciones)}} mensajes</div>
                </div>
                <hr class="sidebar-divider colorSea col-12 justify-content-start m-0">
            </div>
        </div>

        <label for="" hidden class="d-none" id="lblMensaje">¿Esta seguro de su confirmación?</label>
        <div class="col-12 row m-0 p-0" style="min-height: 700px;">
            @foreach ($Notificaciones as $Mensaje)
                <x-card_info>
                    @slot('Clases')
                        col-sl-6 col-md-6 col-sm-12
                    @endslot

                    @slot('Titulo')

                        {{$Mensaje->usua_nombre}}

                        @if (!$Mensaje->mens_visto)
                            <button class="btn btn-primary"
                                onclick="MakeRequestData('{{route('confirmar-lectura', [$Mensaje->mens_id])}}', '', true, '', 'GET',
                                2, '', true, false, params = ['NeedAsk/#lblMensaje'])">
                                <i class="fa fa-eye"></i> &nbsp; Confirmar lectura
                            </button>
                        @endif

                    @endslot


                    @slot('Body')
                        {{$Mensaje->mens_cuerpo}}
                    @endslot



                    @slot('Eventos')
                        Hora de notificación: &nbsp; {{$Mensaje->mens_fecha}}  {{$Mensaje->mens_hora}}
                    @endslot

                </x-card_info>
            @endforeach
        </div>

    </div>
