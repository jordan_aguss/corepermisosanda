<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class PreguntaAnalisis extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    { 
        Schema::create('pregunta_analisis', function (Blueprint $table) {
            $table->unsignedInteger('PREG_ID')->comment('Id de la pregunta');
            $table->unsignedInteger('ID_TABLA_ANALISIS_UNICO')->nullable()->comment('campo uniqueId de los objetos obtenidos en la respuesta VIGEA');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('pregunta_analisis');
    }
}
