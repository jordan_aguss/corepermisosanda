


<!DOCTYPE html>
<html lang="en">


<html>
    <head>
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css" integrity="sha384-zCbKRCUGaJDkqS1kPbPd7TveP5iyJE0EjAuZQTgFLD2ylzuqKfdKlfG/eSrtxUkn" crossorigin="anonymous">
    </head>
    
    
         <div class="card-body">

           <center><h5 > <p class="font-weight-bold"> NOTIFICACIÓN DE INICIO DE OBRA </p></h5 ></center>
            <center><img src="{{ storage_path() . '/app/public/images/logo_color.png' }}" width="45%" ></center>  <br> 
      

            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>
                            Fecha de Ingreso
                        </th>
                        <th>
                            Expendiente N.°
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td> <strong><p> {{$fecha_creacion}} </p></strong></td>
                        <td> <strong><p>  {{$perm_codigo}}</p></strong></td>
                    </tr>
                </tbody>
            </table>
    
     
           <center> <strong> <label for="">DATOS DEL INFORMANTE</label></strong></center>
 
            <table class="table table-bordered">
               <tbody>
                   <tr>
                       <td colspan="4" >Nombre:{{isset($preguntas[274] )  ? $preguntas[274] : '' }}</td>
                   
                   </tr>
                   <tr>
                       <td colspan="4"  >Tipo de Persona: {{isset($preguntas[287] )  ? $preguntas[287] : '' }} </td>
                   </tr>
                   <tr>
                       <td colspan="2">DUI: {{isset($preguntas[288] )  ? $preguntas[288] : '' }}</td>
                       <td colspan="2" >Tipo de Documento: {{isset($preguntas[289] )  ? $preguntas[289] : '' }}</td>
                   </tr>
                   <tr>
                       <td colspan="1">Nacionalidad: {{isset($preguntas[291] )  ? $preguntas[291] : '' }}</td>
                       <td colspan="2">Domicilio:  {{isset($preguntas[292] )  ? $preguntas[292] : '' }}</td>
                       <td colspan="1">Departamento: {{isset($preguntas[293] )  ? $preguntas[293] : '' }}</td>
                   </tr>

                   <tr>
                       <td colspan="2">Correo Eléctronico: {{isset($preguntas[294] )  ? $preguntas[294] : '' }}</td>
                       <td colspan="2">Télefono: {{isset($preguntas[295] )  ? $preguntas[295] : '' }}</td>
                   </tr>

                   <tr>
                       <td colspan="4">Razón Social (Si Aplica): {{isset($preguntas[296] )  ? $preguntas[296] : '' }}</td>
                   </tr>

                   <tr>
                       <td>Inscrito al N° CNR: {{isset($preguntas[297] )  ? $preguntas[297] : '' }}</td>
                       <td>Libro: {{isset($preguntas[298] )  ? $preguntas[298] : '' }}</td>
                       <td>Fecha: {{isset($preguntas[299] )  ? $preguntas[299] : '' }}</td>
                       <td>NIT: </td>
                   </tr>

                   <tr>
                       <td colspan="2">Nombre del Proyecto:{{isset($preguntas[300] )  ? $preguntas[300] : '' }} </td>
                       <td colspan="2">Tipo de Proyecto:  {{isset($preguntas[301] )  ? $preguntas[301] : '' }}</td>
                   </tr>
                   <tr>
                       <td>Propietario: {{isset($preguntas[302] )  ? $preguntas[302] : '' }}</td>
                       <td>Ubicación: {{isset($preguntas[303] )  ? $preguntas[303] : '' }} </td>
                       <td>Municipio: {{isset($preguntas[304] )  ? $preguntas[304] : '' }}</td>
                       <td>Departamento: {{isset($preguntas[305] )  ? $preguntas[305] : '' }}</td>
                   </tr>
               </tbody>
            </table>

            <hr>


            <center> <strong> <label for="">INFORMACIÓN</label></strong></center>

            <table class="table table-bordered"> 
                <tbody>
                    <tr>
                        <td colspan="4">Fecha Inicio: {{isset($preguntas[306] )  ? $preguntas[306] : '' }}</td>
                    </tr>
                    <tr>
                        <td>Nº {{isset($preguntas[307] )  ? $preguntas[307] : '' }}</td>
                        <td>Ref. Urb. {{isset($preguntas[308] )  ? $preguntas[308] : '' }}</td>
                        <td>Ref. Com. {{isset($preguntas[309] )  ? $preguntas[309] : '' }}</td>
                        <td>Fecha: {{isset($preguntas[310] )  ? $preguntas[310] : '' }}</td>
                    </tr>
                    <tr>
                        <td  colspan="4">Instalaciones Hidráulicas por las que solicitan recepción</td>
                    </tr>
                    <tr>
                        <td colspan="2"> Cantidad Acueducto {{isset($preguntas[312] )  ? $preguntas[312] : '' }}</td>
                        <td colspan="2"> Cantidad Alcantarillado {{isset($preguntas[311] )  ? $preguntas[311] : '' }}</td>
                    </tr>
                    <tr>
                        <td>Ubicación de las instalaciones a ser supervisadas  {{isset($preguntas[314] )  ? $preguntas[314] : '' }}</td>
                    </tr>
                  
                    <tr>
                        <td  colspan="2" rowspan="2">Longitud de Tuberías:  {{isset($preguntas[315] )  ? $preguntas[315] : '' }}</td>
                        <td colspan="2"> Cuando se trate de Aguas Negras  </td>
                      </tr>
                      <tr>
                        <td >Pendiente Aprobada {{isset($preguntas[317] )  ? $preguntas[317] : '' }} </td>
                        <td>Pendiente Real: {{isset($preguntas[318] )  ? $preguntas[318] : '' }}</td>
                        {{-- <td>Tipo de Agua  {{isset($preguntas[316] )  ? $preguntas[316] : '' }}</td> --}}
                      </tr>

                      <tr>
                          <td colspan="4"> Cantidad y Diámetro de las conexiones domicialiares, indicando las viviendas o lostes correspondientes: {{isset($preguntas[319] )  ? $preguntas[319] : '' }} </td>
                      </tr>
                      <tr>
                          <td colspan="4">Indicar cualquier otra instalación construida / instalada según proyecto aprobado</td>
                      </tr>
                </tbody>
            </table>


            <hr>

            <center> <strong> <label for="">FIRMA DEL INFORMATE</label></strong></center>

            <table>
                <tbody>
                    <tr>
                        <td> <br>
                            ___________________________________________________________________________ <br>
                            Nombre y firma del solicitante (Propietario, Representante Legal o Apoderado, Profesional responsalbe)
                        </td>
                    </tr>
                </tbody>
            </table>
         
     </div>
 </html>
 
 