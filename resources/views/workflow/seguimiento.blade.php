<div class="card mb-4">
    <div class="card-header">Lista de Permisos</div>
    <div class="card-body">
        <div class="dataTable-top">
            <button class="btn btn-green" type="button" onclick="LoadPage('{{route('permisos.form')}}')">Nuevo&nbsp;&nbsp;<i class="fas fa-plus"></i></button>
            <div class="dataTable-search">
                <input type="text" class="dataTable-input" placeholder="Buscar...">
            </div>
        </div>
        
        <x-table_empty>        
            @slot('header')
                <th>Name</th>
                    <th>Nº</th>
                    <th>Nombre permiso</th>
                    <th>Fecha creacion</th>
                    <th>Estado</th>
                    <th>Aciones</th>
            @endslot
            
            @slot('body')
            <tr>
                <td>Lael Greer</td>
                <td>Systems Administrator</td>
                <td>21</td>
                <td>2009/02/27</td>
                <td><div class="badge bg-primary text-white rounded-pill">En Proceso</div></td>
                <td>
                    <button class="btn btn-datatable btn-icon btn-transparent-dark me-2"><i class="fas fa-copy text-info"></i></button>
                    <button class="btn btn-datatable btn-icon btn-transparent-dark"><i class="fas fa-ellipsis-v text-gray"></i></button>
                </td>
            </tr>
            <tr>
                <td>Jonas Alexander</td>
                <td>Developer</td>
                <td>30</td>
                <td>2010/07/14</td>
                <td><div class="badge bg-primary text-white rounded-pill">En Proceso</div></td>
                <td>
                    <button class="btn btn-datatable btn-icon btn-transparent-dark me-2"><i class="fas fa-copy text-info"></i></button>
                    <button class="btn btn-datatable btn-icon btn-transparent-dark"><i class="fas fa-ellipsis-v text-gray"></i></button>
                </td>
            </tr>
            <tr>
                <td>Shad Decker</td>
                <td>Regional Director</td>
                <td>51</td>
                <td>2008/11/13</td>
                <td><div class="badge bg-primary text-white rounded-pill">En Proceso</div></td>
                <td>
                    <button class="btn btn-datatable btn-icon btn-transparent-dark me-2"><i class="fas fa-copy text-info"></i></button>
                    <button class="btn btn-datatable btn-icon btn-transparent-dark"><i class="fas fa-ellipsis-v text-gray"></i></button>
                </td>
            </tr>
            <tr>
                <td>Michael Bruce</td>
                <td>Javascript Developer</td>
                <td>29</td>
                <td>2011/06/27</td>
                <td><div class="badge bg-primary text-white rounded-pill">En Proceso</div></td>
                <td>
                    <button class="btn btn-datatable btn-icon btn-transparent-dark me-2"><i class="fas fa-copy text-info"></i></button>
                    <button class="btn btn-datatable btn-icon btn-transparent-dark"><i class="fas fa-ellipsis-v text-gray"></i></button>
                </td>
            </tr>
            @endslot
        </x-table_empty>
    </div>
</div>