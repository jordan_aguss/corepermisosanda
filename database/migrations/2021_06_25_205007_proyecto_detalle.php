<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\ClasesExternas\MetodosGenerales;

class ProyectoDetalle extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    { 
        Schema::create(MetodosGenerales::$Esquema . 'proyecto_detalle', function (Blueprint $table) {
            $table->id('PROD_ID')->comment('Id Proyecto detalle');
            $table->unsignedInteger('PROY_ID')->comment('ID DEL PROYECTO');
            $table->unsignedInteger('PREG_ID')->comment('ID DE LA PREGUNTA');
            $table->string('PROD_VALOR')->comment('VALOR DE LA PREGUNTA');
            $table->datetime('PROD_FECHAINSERT')->comment('Fecha de agregacion del registro');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(MetodosGenerales::$Esquema . 'proyecto_detalle');
    }
}
