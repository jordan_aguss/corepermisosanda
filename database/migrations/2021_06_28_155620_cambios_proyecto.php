<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\ClasesExternas\MetodosGenerales;

class CambiosProyecto extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    { 
        Schema::create(MetodosGenerales::$Esquema . 'cambios_proyecto', function (Blueprint $table) {
            $table->id('CAMP_ID')->comment('ID DE LA TABLA');
            $table->string('CAMP_CAMBIO',50)->comment('campo a cambiar');
            $table->string('CAMP_VALORACTUAL')->comment('campo a cambiar');
            $table->string('CAMP_VALORANTERIOR')->comment('campo a cambiar');
            $table->unsignedInteger('PROY_ID')->comment('');
            $table->string('CAMP_NOTAS')->comment('notas del cambio');
            $table->datetime('COMP_FINSERT')->comment('fecha de insercion del registro');
            $table->unsignedInteger('USUA_ID')->comment('');
            $table->unsignedInteger('CAMP_ACTIVO')->comment('');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(MetodosGenerales::$Esquema . 'cambios_proyecto');
    }
}
