var map;
var  geocoder;
var drawingmanager;
var rutaAnterior=null;
var validator;
var direccion="";
var areaPoligono=0;
var contador=1;
var urlAnalisis;
var urlPreguntas;
var Urls;
var Url;






    function alertas(a) {
        alert(a.id);
      }


    $("body").on("change", ".dependiente", function(){
        let idOpcion = $(this).val();
       
       // let idOpcion = $(this).attr('idp');
      // console.log('funciona');
        //console.log(idOpcion);

        if(idOpcion > 0){
            GetHijas(this, idOpcion);
        }
    });

    function GetHijas(item, Opcion, loading = 1){

        let idItem = $(item).attr("id");
        var idPregunta = idItem.replace("campoPregunta", "");
        $("#contenedor" + idPregunta).html('<div class="spinner-border text-warning small mt-1" role="status"></div>');
        let url = urlPreguntas + "/" + idPregunta + '/' + Opcion + '/' + $(item).closest('form').attr('seccion') + '/' + loading;
        $("#contenedor" + idPregunta).load(url);
    }


    function setUrls(urlanalisis, urlpreguntas,  urls, url){
        console.log(urls, url);
        urlAnalisis = urlanalisis;
        urlPreguntas = urlpreguntas;
        Urls = urls;
        Url = url;
    }

    function initMap() {

        map = new google.maps.Map(document.getElementById('map'), {
            center: {lat:13.708425, lng: -88.927586},
            zoom: 9,
            mapTypeId: 'hybrid'
        });

        geocoder = new google.maps.Geocoder();
        drawingManager = new google.maps.drawing.DrawingManager({
            drawingMode: google.maps.drawing.OverlayType.POLYGON,
            drawingControl: true,
            drawingControlOptions: {
            position: google.maps.ControlPosition.TOP_CENTER,
            drawingModes: [
                google.maps.drawing.OverlayType.POLYGON
            ]
            },
            markerOptions: {icon: '../Images/marcadoro.png'},
            circleOptions: {
            fillColor: '#ffff00',
            fillOpacity: 1,
            strokeWeight: 5,
            clickable: false,
            editable: true,
            zIndex: 1
            }
        });

        drawingManager.setMap(map);

        google.maps.event.addListener(drawingManager, 'overlaycomplete', function(event) {

            var coordenadas;
            var prepath = rutaAnterior;
            areaPoligono=0;
            $('#AreaProyecto').innerHTML = "Area:";

            if(prepath){
                prepath.setMap(null);
            }

                coordenadas = event.overlay.getPath().getArray();
                areaPoligono = google.maps.geometry.spherical.computeArea(event.overlay.getPath());
                $('#AreaProyecto').html("Area: "+ areaPoligono.toFixed(2)+ " m2");
                var pathsvigea="";
                for (var i = 0; i < coordenadas.length; i++) {
                    lat = coordenadas[i].lat();
                    lng = coordenadas[i].lng();
                    pathsvigea = pathsvigea + lng + "," + lat + ",";
                }

                //obtener el nombre del municipio
                geocoder.geocode({'location': coordenadas[0]}, function(results, status) {

                    if (status === google.maps.GeocoderStatus.OK) {
                    if (results[1]) {
                            direccion = results[1].formatted_address;
                        }
                    }
                });

                //fin obtener el nombre del municipio
                $('#CoordenadasVigea').val(coordenadas);


            MakeRequestData(urlAnalisis + '/' + pathsvigea +'/POINT', '#divDVigea');
            rutaAnterior = event.overlay;


        });
    }

    function VerificarProyecto(Url, IdForm){

        $('input').prop('required', true);
        var formS = $('.formSecundario');
        var form = $(IdForm)[0];
        $(formS).each(function(items_list){

            if(!formS[items_list].checkValidity()){
                let seccion = $(formS[items_list]).attr('seccion');
                console.log(seccion);
                formS[items_list].classList.add('was-validated');

                $('#tab-' + seccion).addClass('bg-danger');
            }
        });

        if (!form.checkValidity()) {
            form.classList.add('was-validated');
            ShowAlert('Error', 'Tiene campos sin llenar en el formulario', 'error');
        }else{
            return SendInfoForm(Url, IdForm);
        }
    }

    function SendInfoForm(Url, IdForm){

        window.location.href = Url;
        /*LoadingAlert();
        var Form = $(IdForm)[0];
        var FormSend = new FormData(Form);

        $.ajax({
            type: "GET",
            url: Url,
            data: FormSend,
            cache: false,
            processData: false,
            contentType: false,
            timeout: 800000,
            async: true,
            success: function(response) {
                ShowAlert('Exito', 'El proceso se ha realizado correctamente','success', false);

            },
            error: function(response){
                console.log(response);
                let r = jQuery.parseJSON(response.responseText);
                if(r.message.includes('^')){
                    ShowAlert('Error', r.message.replace('^',''),'error');
                }else{
                    ShowAlert('Error', 'Ocurrio un error al procesar la solicitud.','error');
                }
                return false;
            }
        }); */
    }


    function Quitar(item) {
        $(item).removeClass('bg-danger');
    }

    function QuitarRequired() {
        $('.enviado input').removeAttr("required");
    }


  