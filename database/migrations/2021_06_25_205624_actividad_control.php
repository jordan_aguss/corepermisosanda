<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\ClasesExternas\MetodosGenerales;

class ActividadControl extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    { 
        Schema::create(MetodosGenerales::$Esquema . 'actividad_control', function (Blueprint $table) {
            $table->id('ACTC_ID')->comment('ID DE LA TABLA');
            $table->unsignedInteger('ACTI_ID')->comment('ID DE LA PREGUNTA');
            $table->unsignedInteger('ACTI_ID_CONTROLADA')->comment('ID DE LA PREGUNTA');
            $table->unsignedInteger('SECD_ID')->comment('ID DEL SECTOR DETALLE');
            $table->unsignedInteger('CATE_ID')->comment('ID DE LA CATEGORIZACION');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(MetodosGenerales::$Esquema . 'actividad_control');
    }
}
