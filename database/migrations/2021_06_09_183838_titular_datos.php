<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\ClasesExternas\MetodosGenerales;

class TitularDatos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    { 
        Schema::create(MetodosGenerales::$Esquema . 'titular_datos', function (Blueprint $table) {
            $table->id('TITD_ID');
            $table->unsignedInteger('TITU_ID');
            $table->unsignedInteger('TITDA_ID');
            $table->string('TITD_NOMBRE',100);
            $table->string('TITD_APELLIDO', 100);
            $table->string('TITD_TELEFONO', 10)->nullable();
            $table->string('TITD_CELULAR', 10)->nullable();
            $table->string('TITD_CORREO', 100)->nullable();
            $table->string('TITD_FAX', 10)->nullable();
            $table->string('TITD_DUI', 15)->nullable();
            $table->string('TITD_ADJUNTO_DUI', 200)->nullable();
            $table->string('TITD_NIT', 20)->nullable();
            $table->string('TITD_ADJUNTO_NIT', 200)->nullable();
            $table->string('TITD_AUTORIZACION', 200)->nullable();
            $table->string('TITD_ADJUNTO_CREDENCIAL', 200)->nullable();
            $table->date('VIGENCIA_CREDENCIAL')->nullable();
            $table->string('TITD_ADJUNTO_LEYCREACION', 200)->nullable();
            $table->string('TITD_ADJUNTO_MODIFICACION', 200)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(MetodosGenerales::$Esquema . 'titular_datos');
    }
}
