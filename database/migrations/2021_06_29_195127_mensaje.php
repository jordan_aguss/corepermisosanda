<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\ClasesExternas\MetodosGenerales;

class Mensaje extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    { 
        Schema::create(MetodosGenerales::$Esquema . 'mensaje', function (Blueprint $table) {
            $table->id('MENS_ID')->comment('ID DE LA TABLA');
            $table->string('MENS_ASUNTO')->comment('asunto del mensaje');
            $table->string('MENS_DESCRIPCION')->comment('descripcion del mensaje');
            $table->datetime('MENS_FECHA')->comment('fecha que se creo el mensaje');
            $table->datetime('MENS_FECHACREACION')->comment('fecha que se creo el mensaje');
            $table->unsignedInteger('MENR_ID')->comment('id del emnsaje relacion');
            $table->unsignedInteger('MENS_PADRE')->comment('Id del mensaje padre');
            $table->unsignedInteger('USUA_IDEMISOR')->comment('Id del usuario que recibira el mensaje');
            $table->unsignedInteger('USUA_IDRECEPTOR')->comment('Id del usuario que envio el mensaje');
            $table->unsignedInteger('USUA_IDCREACION')->comment('Id del usuario que creo el mensaje');
            $table->unsignedInteger('MENS_LEIDO')->comment('Confirmacion de lectura');
            $table->unsignedInteger('MENS_RECIBO')->comment('');
            $table->unsignedInteger('OBJE_ID')->comment('Id del objeto');
            $table->string('MENS_ADJUNTO')->comment('url del documento adjunto al mensaje');
            $table->unsignedInteger('MENS_ACTIVO')->comment('Estado del mensaje');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(MetodosGenerales::$Esquema . 'mensaje');
    }
}
