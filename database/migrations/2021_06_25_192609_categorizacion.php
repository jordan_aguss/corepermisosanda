<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\ClasesExternas\MetodosGenerales;

class Categorizacion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    { 
        Schema::create(MetodosGenerales::$Esquema . 'categorizacion', function (Blueprint $table) {
            $table->id('CATE_ID')->comment('Id de la categorizacion');
            $table->string('CATE_NOMBRE')->comment('Nombre de la categoria');
            $table->unsignedInteger('CATE_IMPACTO')->comment('Impacto de la categorizacion');
            $table->string('CATE_DESCRIPCION')->comment('Descripcion de la categorizacion');
            $table->unsignedInteger('CATE_VALORMAXIMO')->comment('Valor maximo de la categoria');
            $table->string('CATE_TIPO')->comment('Tipo de la categoria');
            $table->unsignedInteger('CATE_ACTIVO')->comment('Estado de la categoria');
            $table->string('CATE_TIPO_PROCESO_PUBLICO')->comment('Falta saber que hace');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(MetodosGenerales::$Esquema . 'categorizacion');
    }
}
