<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class PreguntaAccion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    { 
        Schema::create('pregunta_accion', function (Blueprint $table) {
            $table->id('PREA_ID')->comment('Id de la opcion');
            $table->unsignedInteger('PREG_ID')->comment('Id de la pregunta');
            $table->unsignedInteger('PREO_ID')->comment('Id de la opcion de las preguntas');
            $table->unsignedInteger('PREG_ID_DEPENDE')->comment('Id de la pregunta dependiente');
            $table->unsignedInteger('TIPO_SECCION_PREGUNTA')->comment('');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('pregunta_accion');
    }
}
