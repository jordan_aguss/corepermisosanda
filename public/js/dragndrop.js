
    $('#InfoFree').sortable({
        group: {
            name: "info-view",
            pull: true,
            put: true
        },
        animation: 150,
        easing: "cubic-bezier(0.895, 0.03, 0.685, 0.22)",
        // ghostClass: "active",
        chosenClass: "active",
        //dragClass: "invisible"
        dataIdAttr: "data-identificador",
        store: {
            set: function(sortable){
                const orden = sortable.toArray();
            }
        },
    });

    $('#InfoAdd').sortable({
        group: {
            name: "info-view",
            pull: true,
            put: true
        },
        animation: 150,
        easing: "cubic-bezier(0.895, 0.03, 0.685, 0.22)",
        handle: ".fas",
        filter: ".titulo",
        // ghostClass: "active",
        chosenClass: "active",
        //dragClass: "invisible",
        
        onAdd: (evento) => { 
            
            console.log(evento.item) 
            console.log($($(evento.item).children()[0]).attr('idPregunta'));
        },
    });
    