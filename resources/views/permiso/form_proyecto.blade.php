@include('permiso.pago.modal-pago')
<form class="card" id="FormPermiso">


    <div class="card-body" hide>
        @csrf
        <h5 class="col-auto m-0 mt-3 mb-3" ><b>Crear Solicitud</b></h5>
        <div class="col-12 m-0 p-0 row mt-3 mb-3" hidden>
            <div class="font-weight-bold">- Información general del proyecto</div>
            <hr class="sidebar-divider colorSea col-12 justify-content-start m-0">
        </div>
        <div class="col-12 mb-2" >
            <span class="text-black black small"><b>Permiso a solicitar</b></span>

            <input type="hidden" name="SelectForm" value="{{$id}}">

            <div class="invalid-feedback small">
                Selecciona el tipo de proyecto.
            </div>
        </div>


        <style>.scrolling-wrapper{
            overflow-x: auto;
        }</style>
        <div class="col-12 p-0 m-0 row" id="divPreguntas">

        </div>

        <div class="col-12 m-0 p-0 row mt-3 mb-3">
            <div class="font-weight-bold">Acciones</div>
            <hr class="sidebar-divider colorSea col-12 justify-content-start m-0">
        </div>
        <div class="col-12 p-0 m-0" id="divDVigea"></div>

        @if ($pago)
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal2">
                <i class="fas fa-solid fa-credit-card"></i>&nbsp;  Realizar Pago
            </button>
        @endif

        <button id="btnGuardar" class="btn btn-success" type="button" onclick="MakeRequestData('{{route('guardar-permiso')}}', '#bodyContent', true, '', 'POST', 2, '#FormPermiso', true)">
            <i class="fas fa-save"></i> &nbsp; Guardar
        </button>
    </div>
</form>


<script src="{{asset('js/AddProyecto.js')}}"></script>


<script async defer
src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB_KBybVlLgRyMt_P6Ky5_IMtnptXpvCUU&signed_in=true&libraries=drawing&callback=initMap">


</script>

<script>

    @if($pago)
        $('#btnGuardar').prop('disabled', true);
    @endif

    MakeRequestData('{{route('preguntas-form', [$id])}}', '#divPreguntas' );

    setUrls("", "{{route('get-preguntas-hijas')}}", "{{route('prem-save-preguntas')}}", "{{route('prem-save-pregunta')}}");

    function saveLastSection(){
        let tab = $("a.nav-item.nav-link.active").data("tab");
        console.log(tab);
        console.log("LLEGO");
        MakeRequestData('{{route("prem-save-preguntas")}}', '', false, '', 'POST', 2, '#FormTab-' + tab);
    }


    
$('body').on('click', '.btnNext', function() {


$('input').prop('required', false);

let tab = $(this).attr('tabItem');   
let flag = false;
//console.log($(`#FormTab-${tab} input`));

                $(`#FormTab-${tab} input`).each(function () {
                   nameInput = $(this).attr('id');
                    let pregs = [156,157,158,246,422,423,424,425,164,165]

                   // console.log(nameInput);
                   pregs.forEach(element => {

                        if( $('#campoPregunta'+element).val() == '' ||  $('#campoPregunta'+element+' option:selected').val() == '') {
                            $('#campoPregunta'+element).addClass('is-invalid');
                                if($('#campoPregunta'+element).is(':disabled')){
                                    $('#campoPregunta'+element).removeClass('is-invalid');
                                }
                        }else{
                            $('#campoPregunta'+element).removeClass('is-invalid');
                        }
                        flag = true;
                        return false;
                   });   
                });

if(flag == true){
    return false;
}
let next =  (parseInt(tab) + 1);
let item = '.tabSection-' + next;

$('.tabPage-' + tab).removeClass('active show');
$('.tabPage-' + next ).addClass('active show');
$(item).removeClass('disabled');
$('.tabSection-' + tab).removeClass('active');
$(item).addClass('active');
$('.formSecundario').removeAttr("required");

SetDataResult(this, 3, '');
$('#FormTab-' + tab).addClass('enviado');

console.log($(this).attr('tabItem'));
$("html,body").animate({scrollTop: $('.tabSection-' + tab).offset().top}, 1000);
MakeRequestData('{{route('prem-save-preguntas')}}', '', false, '', 'POST', 2, '#FormTab-' + tab);
});



$('body').on('change', '.enviado input', function(){
console.log('Mensaje');
MakeRequestData('{{route('prem-save-pregunta')}}', '', false, '', 'POST', 2, '', false, false,
['seccion/' + $(this).closest('form').attr('seccion'), 'nombre/' + $(this).attr('id').replace('campoPregunta', ''), 'valor/' + $(this).val()]);
});



</script>
