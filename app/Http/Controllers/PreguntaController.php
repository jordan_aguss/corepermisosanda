<?php

namespace App\Http\Controllers;

use App\Models\Departamento;
use App\Models\DetallePermiso;
use App\Models\Municipio;
use App\Models\PermisoFormulario;
use App\Models\Pregunta;
use App\Models\PreguntaSeccion;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Log;

class PreguntaController extends Controller
{
    public function GetPreguntasForm($idForm, $nuevo = 1){

        $Preguntas = $this->InfoPreguntas('s.secc_id', [], 'pa.preg_id', $idForm)->join('seccion_formulario as sf', 'sf.secc_id', 's.secc_id')
        ->where('preg_padre', 1)->where('sf.form_id', $idForm)->get();

        $departamento = Departamento::all();
        $municipio = Municipio::all();
        $selectDisable = '';

        return view('preguntas.form-preguntas', compact('Preguntas', 'departamento', 'municipio','selectDisable'));
    }

    private function InfoPreguntas($seccion = '', $select = [], $idpreg = 'pa.preg_id', $form_id = 7){

        $info = array_merge(['p.*', DB::raw('if(isnull(pred_id), 0, 1) as DEPENDE'), 's.secc_id', 's.secc_nombre', 'perd_respuesta', 'vl.vali_clase'], $select);

        return PreguntaSeccion::join('preguntas as p', 'p.preg_id', 'pregunta_seccion.preg_id')
        ->join('secciones as s', 's.secc_id', 'pregunta_seccion.secc_id')
        ->leftjoin('validaciones as vl', 'vl.vali_id', 'p.vali_id')
        ->leftjoin('pregunta_accion as pa', $idpreg, 'p.preg_id')
        ->leftjoin('permisos_formulario as pf', function($query) use ($form_id){
            $query->where('pf.perm_id', Session::get('nuevo-permiso'));
            $query->where('pf.form_id', $form_id);
        })
        ->leftjoin('permiso_detalle as pd', function($join) use ($seccion){
            $join->on('pd.preg_id', 'p.preg_id');
            $join->on('pd.secc_id', $seccion);
            $join->on('pd.perf_id', 'pf.perf_id');
        })
        ->distinct()->select($info)->orderBy('secf_orden')->orderBy('preg_orden');
    }

    public function GetPreguntasHijas($pregunta, $opcion, $seccion, $loading){

        $Preguntas = Pregunta::join('pregunta_accion as pa', 'pa.pred_id', 'preguntas.preg_id')->where('pa.preg_id', $pregunta)->where('pa.opci_id', $opcion)->get('*', '0 as secc_id');
        $hijas = true;
        return view('preguntas.form-preguntas', compact('Preguntas', 'hijas'));
    }


    public function guardarAjunto(Request $request){
            $id = 'IDAdjuntos';
          //  $request->file('campoPregunta[156]')->storeAs('public', $id);
          $request->campoPregunta[156]->move(storage_path('uploads'), $id.'.pdf');
            //$file = $request->file('campoPregunta[156]');
           // \Storage::disk('local')->put($id,  \File::get($file));
    }

    public function descargarAdjunto($ruta){

        return response()->download(storage_path().'/uploads/'.$ruta);
    }

    public function descargarAdjuntoPublico($ruta){

        return response()->download(storage_path().'/uploads/'.$ruta);
    }

    public function departamentos($id){

        $depa = Departamento::where('DEPA_NOMBRE', $id)->first()->DEPA_ID;
        $muni = Municipio::where('depa_id', $depa)->get();
        $finalText = '';
        foreach($muni as $key){
            $text = '<option value="'.$key->muni_nombre.'" >'.$key->muni_nombre.'</option>';
            $finalText = $finalText.$text;
        }
        //Log::info($finalText);
        return $finalText;
    }

    public function actualizarPermisos($permID, $formID = '', $tipo = 0){

        Session::put('nuevo-permiso', $permID);

        $secciones = [22 => 11, 23 => 9, 43 => 11];

        $PFormulario = PermisoFormulario::where('perm_id', $permID)->where('form_id', $formID)->first();

        $valorSelect = DetallePermiso::where('perf_id', $PFormulario->perf_id)->where('preg_id', 189)->first()->perd_respuesta;

        $Preguntas = $this->InfoPreguntas('s.secc_id', [], 'pa.preg_id', $formID)->join('seccion_formulario as sf', 'sf.secc_id', 's.secc_id')
        ->where('preg_padre', 1)->where('sf.form_id', $formID)->where('sf.secc_id', '!=', $secciones[$valorSelect])->get();

        $depaValor = DetallePermiso::where('perf_id', $PFormulario->perf_id)->where('preg_id', 181)->first()->perd_respuesta;
        $depaValorF = DetallePermiso::where('perf_id', $PFormulario->perf_id)->where('preg_id', 196)->first()->perd_respuesta;
        $muniValorF = DetallePermiso::where('perf_id', $PFormulario->perf_id)->where('preg_id', 195)->first()->perd_respuesta;

        $departamento = Departamento::all();
        $municipio = Municipio::all();

        $selectDisable = 'disabled';

        return view('permiso.editar', compact('Preguntas', 'departamento', 'municipio', 'depaValor', 'depaValorF','muniValorF', 'tipo', 'selectDisable', 'formID', 'PFormulario'));

    }






}
