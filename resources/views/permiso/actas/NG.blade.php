<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <div style="width:800px; margin:0 auto;">
        <p style="text-align: right;">San Salvador,{{isset( $preguntas[430] )  ? $preguntas[430] : '' }}</p>
        <p style="text-align: right;">Ref Urb: {{isset( $preguntas[430] )  ? $preguntas[430] : '' }}<br /><br /></p>
        <p style="text-align: left;">Ingeniero<br /><strong>"SOLICITANTE"<br /></strong>Presente.
        </p>
        <p style="text-align: left;">&nbsp;</p>
        <p style="text-align: Justify;">De acuerdo al Acta N {{isset( $preguntas[431] )  ? $preguntas[431] : '' }}
            del comite de factibilidades de esta fecha, celebrado en las oficinas centrales, se acordo autorizar la
            factibilidad de conexión de servicio de aguas negras hacia la red de ANDA, para un terreno propiedad
            de  {{isset($preguntas[193] )  ? $preguntas[193] : '' }} ubicado en  {{isset($preguntas[194] )  ? $preguntas[194] : '' }}    Municipio de {{isset($preguntas[195] )  ? $preguntas[195] : '' }}
            Departamento de {{isset($preguntas[196] )  ? $preguntas[196] : '' }} , en el cual se proyecta la construcción de {{isset($preguntas[419] )  ? $preguntas[419] : '' }}</p>
        <p style="text-align: left;">&nbsp;</p>
        <p style="text-align: left;">&nbsp;</p>
        <p style="text-align: Justify;">al respecto hago de su conocimiento que de acuerdo a opinión de la
            Dirección Región <br /><strong>NO ES POSIBLE ACCEDER A LO SOLICITADO</strong> en
            vista que {{isset($preguntas[419] )  ? $preguntas[419] : '' }}&nbsp;&nbsp;</p>
        <p style="text-align: left;">&nbsp;</p>
        <p style="text-align: left;">&nbsp;</p>
        <p style="text-align: left;">&nbsp;</p>
        <p style="text-align: left;">&nbsp;</p>
        <p style="text-align: left;">Atentamente,</p>
        <p style="text-align: left;">&nbsp;</p>
        <p style="text-align: left;">&nbsp;</p>
        <p style="text-align: left;">&nbsp;</p>
    </div>
</body>

</html>
