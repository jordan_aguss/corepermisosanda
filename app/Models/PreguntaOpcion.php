<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PreguntaOpcion extends Model
{
    use HasFactory;
    protected $table = "pregunta_opcion"; 
    protected $primaryKey = 'PREO_ID';
    public $incrementing = true;
}
