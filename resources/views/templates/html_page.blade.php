<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">

        <meta name="_token" content="{{csrf_token()}}"/>
        <title>{{isset($Info->sucu_nombre) ? $Info->sucu_nombre : ''}}</title>
        <link rel="stylesheet" type="text/css" href="{{asset('css/bootstrap.min.css')}}">
        <link rel="stylesheet" type="text/css" href="{{asset('css/sea.css')}}">
        <link href="{{asset('css/styles.css')}}" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="{{asset('css/bootstrap-select.min.css')}}">
        <link rel="stylesheet" type="text/css" href="{{asset('css/form.css')}}">

        <script src="https://kit.fontawesome.com/a076d05399.js"></script>
        <link rel="icon" type="image/png" sizes="16x16" href="{{asset('images/logo.jpeg')}}">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.4/Chart.min.js"
        crossorigin="anonymous"></script>


        <script src="https://cdnjs.cloudflare.com/ajax/libs/feather-icons/4.28.0/feather.min.js"
            crossorigin="anonymous"></script>


    </header>
    <body class="nav-fixed p-0">

        @yield('Data')


        <div aria-hidden="true" tabindex="-1" role="dialog" class="modal modal-blur fade p-0" id="BigModalInfo" name="BigModalInfo"
            data-bs-backdrop='static'>
            <div class="modal-dialog modal-lg p-2">
                <!-- CONTENIDO DEL MODAL -->
                <div class="modal-content">
                    <div id="CuerpoBigModal" class="p-3">

                    </div>
                </div>
            </div>
        </div>
    </body>
    <script src="{{ asset("js/form.js") }}" type="text/javascript"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.bundle.min.js"
    crossorigin="anonymous"></script>
    <script src="{{asset('js/jquery.min.js')}}"></script>

    <script src="https://kit.fontawesome.com/35b4dfd2b7.js" crossorigin="anonymous"></script>
    <script src="{{asset('js/bootstrap-validate.js')}}"></script>
    <script src="{{asset('js/scripts.js')}}"></script>
    <script src="{{asset('js/bootstrap.bundle.min.js')}}"></script>
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script src="{{ asset("js/bootstrap-select.js") }}" type="text/javascript"></script>
    <script src="{{asset('js/admin.min.js')}}"></script>
    <script src="{{asset('js/jquery.mask.js')}}"></script>
    <script src="{{asset('js/MetodosWeb.js')}}"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/jquery.validate.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/jquery.validate.min.js"></script>

   


    <!--
    <script src="{{asset('js/Sortable.min.js')}}"></script>
    <script src="{{asset('js/sortable-jquery.js')}}"></script>
    <script src="{{asset('js/dragndrop.js')}}"></script> -->


    <script>
        setUrls("", "{{route('get-preguntas-hijas')}}");
    </script>

    @yield('scripts')

</html>
