<?php

namespace App\Http\Controllers;

use App\Models\DetallePermiso;
use App\Models\Formulario;
use App\Models\Permiso;
use App\Models\PermisoFormulario;
use App\Models\Sucursal;
use App\Models\Pregunta;
use App\Models\PreguntaSeccion;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;
use Barryvdh\DomPDF\Facade as PDF;
use App\Models\User;
use Exception;
use Illuminate\Support\Facades\Mail;

class PermisoController extends Controller
{

    public function exportarPDF($idPermiso){

        $permiso = Permiso::where('perm_id', $idPermiso)->orderBy('perm_id', 'desc')->get();
              $permisoDetalle =  DB::table('permisos')
                               ->join('permiso_detalle','permisos.perm_id','=','permiso_detalle.perm_id')
                               ->where('permiso_detalle.perm_id', $idPermiso)
                              ->get();
              $preguntaString = Pregunta::all();

            if(Permiso::where('perm_id', $idPermiso)->andWhere('form_id', 2)){
                $pdf = PDF::loadView('pdf.soliPDF', compact('permisoDetalle', 'preguntaString'));
                return $pdf->download('Informe_Solicitante.pdf');
            } else{
                $pdf = PDF::loadView('pdf.soliComunidadPDF', compact('permisoDetalle', 'preguntaString'));
                return $pdf->download('Informe_Solicitante.pdf');
            }
    }

    public function verPermiso($idPermiso)
    {
        $permiso = Permiso::where('perm_id', $idPermiso)->orderBy('perm_id', 'desc')->get();
        $permisoDetalle =  DB::table('permisos')
                               ->join('permiso_detalle','permisos.perm_id','=','permiso_detalle.perm_id')
                               ->where('permiso_detalle.perm_id', $idPermiso)
                              ->get();

              $preguntaString = Pregunta::all();
              return view('pdf.soliPDF', compact('permiso','permisoDetalle','preguntaString'));
    }

    public function verPermisoComunidad($idPermiso)
    {
        $permiso = Permiso::where('perm_id', $idPermiso)->orderBy('perm_id', 'desc')->get();
        $permisoDetalle =  DB::table('permisos')
                               ->join('permiso_detalle','permisos.perm_id','=','permiso_detalle.perm_id')
                               ->where('permiso_detalle.perm_id', $idPermiso)
                              ->get();

              $preguntaString = Pregunta::all();
              return view('pdf.soliComunidadPDF', compact('permiso','permisoDetalle','preguntaString'));
    }

    //Boton Form Anda (NO UTILIZADO)
    public function SavePreguntas(Request $request, $idForm){


            $Info = new PermisoFormulario();
            $Info->perm_id = Session::get('nuevo-permiso');
            $Info->form_id = $idForm;
            $Info->perf_fecha = date('Y-m-d H:i:s');
            $Info->usua_id = Auth::id();
            $Info->save();

            foreach($request->campoPregunta as $seccion => $preguntas){
                foreach($preguntas as $preg_id => $valor){
                    DB::insert('insert into permiso_detalle (preg_id, secc_id, perd_respuesta, perf_id) values(?,?,?,?)', [$preg_id, $seccion, $valor, $Info->perf_id]);
                }
            }

            if(isset($request->campoPreguntaFile)){
                foreach($request->campoPreguntaFile as $seccion => $preguntas ){
                    foreach($preguntas as $id => $valor){
                        $ext = $preguntas[$id]->getClientOriginalExtension();
                        $identificador = Session::get('nuevo-permiso').'_'.$id.'.'.$ext;
                        $valor = $identificador;
                        $preguntas[$id]->move(storage_path('uploads'), $identificador);
                        Log::info($valor);
                        DB::insert('insert into permiso_detalle (preg_id, secc_id, perd_respuesta, perf_id) values(?,?,?,?)', [$id, $seccion, $valor, $Info->perf_id]);
                    }
                }
            }
    }

    public function index(){


        $Permisos = Permiso::join('formularios as f', 'f.form_id', 'permisos.form_id')
        ->leftjoin('estado_control as e', function ($query){
            $query->on('e.esta_id', 'permisos.esta_id');
            $query->whereIn('estc_tipo', [1, 2]);
            $query->where('estc_cambio', 1);
            $query->on('e.form_id', 'permisos.form_id');
        } )
        ->join('estados as es', 'es.esta_id', 'permisos.esta_id')
        ->join('estado_general as eg', 'eg.estg_id', 'es.estg_id')
        ->join('users as u', 'id', 'usua_id')
        ->where('permisos.esta_id', '>=', 0)
        ->where('usua_id', Auth::id())
        ->select('perm_codigo', 'name', 'form_nombre', 'perm_fecha', 'f.form_id', 'permisos.form_id', 'permisos.perm_id', 'form_new', 'estc_tipo', 'esta_nombre','estg_nombre'
        , DB::raw('(select esta_nombre from estados where esta_id = estc_id) as Nombre'))
        ->orderBy('perm_id', 'desc')->paginate(10)->withPath(route('buscar-permisos'));

        return view('permiso.index', compact('Permisos'));

    }

    public function indexAll(){
        $Permisos = Permiso::join('formularios as f', 'f.form_id', 'permisos.form_id')
        ->leftjoin('estado_control as e', function ($query){
            $query->on('e.esta_id', 'permisos.esta_id');
            $query->whereIn('estc_tipo', [1, 2]);
            $query->where('estc_cambio', 1);
            $query->on('e.form_id', 'permisos.form_id');
        } )
        ->join('estados as es', 'es.esta_id', 'permisos.esta_id')
        ->join('estado_general as eg', 'eg.estg_id', 'es.estg_id')
        ->join('users as u', 'id', 'usua_id')
        ->where('permisos.esta_id', '>=', 0)
        ->select('perm_codigo', 'name', 'form_nombre', 'perm_fecha', 'f.form_id', 'permisos.form_id', 'permisos.perm_id', 'form_new', 'estc_tipo', 'esta_nombre','estg_nombre'
        , DB::raw('(select esta_nombre from estados where esta_id = estc_id) as Nombre'))
        ->orderBy('perm_id', 'desc')->paginate(10)->withPath(route('buscar-permisos'));
        // ->paginate(3)->withPath(route('buscar-permisos'))

        Log::info($Permisos);



        return view('permiso.index', ['Permisos' => $Permisos]);
    }

    public function FormPermiso($id, $permisoId = 0){

        $Info = Sucursal::find(Config::get('constants.id_sucursal'));
        $Forms = Formulario::where('sucu_id', Config::get('constants.id_sucursal'))->get();
        $Permiso = Permiso::where('usua_id', Auth::id())->where('esta_id', -1)->first();
        $pago = 1;

        if($id == 7){

            if(!isset($Permiso->perm_id)){
                $Permiso = new Permiso();
                $Permiso->esta_id = -1;
                $Permiso->usua_id = Auth::id();
                $Permiso->form_id = $id;
                $Permiso->perm_fecha = date('Y-m-d');
                $Permiso->save();
            }

        }else{
            try{
                $pago = Permiso::where('perm_id', $permisoId)->join('estado_control as ec', 'ec.esta_id', 'permisos.esta_id')
                ->whereIn('estc_tipo', [1, 2])->first()->estc_pago;
            }catch(Exception $ex){ }

            $Permiso = new Permiso();
            $Permiso->perm_id = $permisoId;
            $Permiso->form_id = $id;
        }

        Session::put('nuevo-permiso', $Permiso->perm_id);

        return view('permiso.form_proyecto', compact('Info', 'Forms', 'Permiso', 'id', 'pago'));
    }


    public function SavePregunta(Request $request){
        $request->nombre = str_replace(' ','', $request->nombre);

        $PregHija = DetallePermiso::where('secc_id', $request->seccion)->where('preg_id', $request->nombre)->first();

        if(!isset($PregHija->perd_id)){
            $PregHija = new DetallePermiso();
            $PregHija->preg_id = $request->nombre;
            $PregHija->secc_id = $request->seccion;

            $PregHija->perm_id = Session::get('nuevo-permiso');
        }

        $PregHija->perd_respuesta = $request->valor;
        $PregHija->save();
    }


    //Boton Guardar Formulario
    public function GuardarPermiso(Request $request){
        return DB::transaction(function() use ($request){

            $idForm = $request->SelectForm;
            if ($request->SelectForm != 7) {
                $this->SavePreguntas($request, $idForm);
                $estaForm = Formulario::find($request->SelectForm)->esta_id;
                DB::update('update permisos set esta_id = (select estc_id from estado_control where estc_tipo = 0 and estc_cambio = 1 and form_id = permisos.form_id and esta_id = permisos.esta_id) where perm_id = ?', [Session::get('nuevo-permiso')]);
                return $this->index();

            }else{

                $this->SavePreguntas($request, $idForm);
                $estaForm = Formulario::find($request->SelectForm)->esta_id;
                DB::update('update permisos set esta_id = ?, form_id = ?, perm_fecha = ?, perm_codigo = concat(perm_id, "/", year(now())) where esta_id = -1', [$estaForm ,$request->SelectForm, date('Y-m-d H:i:s')]);
                return $this->index();
            }

        });
    }

    public function Buscar($palabra = ''){
       // $Permisos = Permiso::orderBy('perm_fecha', 'asc')->where('perm_id', 'like', "%$palabra%")->paginate(50)->withPath(route('buscar-permisos'));

        $Permisos = Permiso::join('formularios as f', 'f.form_id', 'permisos.form_id')
        ->leftjoin('users as us', 'us.id', 'permisos.usua_id')
        ->leftjoin('estado_control as e', function ($query){
            $query->on('e.esta_id', 'permisos.esta_id');
            $query->whereIn('estc_tipo', [1, 2]);
            $query->where('estc_cambio', 1);
            $query->on('ec.form_id', 'permisos.form_id');
        } )
        ->join('estados as es', 'es.esta_id', 'permisos.esta_id')
        ->join('estado_general as eg', 'eg.estg_id', 'es.estg_id')
        ->select('perm_codigo', 'name', 'form_nombre', 'perm_fecha', 'f.form_id', 'permisos.form_id', 'permisos.perm_id', 'form_new', 'estc_tipo', 'esta_nombre','estg_nombre'
        , DB::raw('(select esta_nombre from estados where esta_id = estc_id) as Nombre'))
        ->orwhere('perm_id', 'like', "%$palabra%")
        ->orwhere('perm_codigo', 'like', "%$palabra%")
        ->orwhere('name', 'like', "%$palabra%")
        ->orwhere('perm_fecha', 'like', "%$palabra%")
        ->orderBy('perm_id', 'desc')
        ->paginate(10)->withPath(route('buscar-permisos'));
        // ->paginate(3)->withPath(route('buscar-permisos'));

        return view('permiso.data-permisos', ['Permisos' => $Permisos]);
    }

    public function Ordenar($palabra){

        $ordens = ['asc' => 'desc', 'desc' => 'asc'];
        Session::put('order', $ordens[Session::get('order', 'asc')]);
        $orden = Session::get('order');

        $asi = array(0=>'perm_codigo', 1=>'name', 2=>'form_nombre', 3=>'perm_fecha');

        $Permisos = Permiso::join('formularios as f', 'f.form_id', 'permisos.form_id')
                ->leftjoin('users as us', 'us.id', 'permisos.usua_id')
                ->leftjoin('estado_control as e', function ($query){
                    $query->on('e.esta_id', 'permisos.esta_id');
                    $query->whereIn('estc_tipo', [1, 2]);
                    $query->where('estc_cambio', 1);
                    $query->on('ec.form_id', 'permisos.form_id');
                } )
                ->join('estados as es', 'es.esta_id', 'permisos.esta_id')
                ->join('estado_general as eg', 'eg.estg_id', 'es.estg_id')
                ->select('perm_codigo', 'name', 'form_nombre', 'perm_fecha', 'f.form_id', 'permisos.form_id', 'permisos.perm_id', 'form_new', 'estc_tipo', 'esta_nombre','estg_nombre'
                , DB::raw('(select esta_nombre from estados where esta_id = estc_id) as Nombre'))
                ->orderBy($asi[$palabra], $orden )
                ->paginate(10)
                ->withPath(route('buscar-permisos'));

                return view('permiso.data-permisos', ['Permisos' => $Permisos]);
        }


    public function aprobarPermiso($id)
    {
        $idPermiso = Permiso::findOrFail($id);
        // 1 = Aprobado   3 = Rechazado   0 = Pendiente
        $idPermiso->esta_id = 1;
        $idPermiso->save();

       return redirect('/');
    }

    public function rechazarPermiso($id)
    {
        $idPermiso = Permiso::findOrFail($id);
        // 1 = Aprobado   3 = Rechazado   0 = Pendiente
        $idPermiso->esta_id = 3;
        $idPermiso->save();
        Mail::send();

        return redirect('/');
    }

    public function detallePreguntas( $IdPermiso, $idForm, $perf_id){
        $Titulo = 'DETALLE PREGUNTAS';

        $Permiso = Permiso::join('formularios','permisos.form_id','formularios.form_id')
        ->where('perm_id',$IdPermiso)->first();

        $Preguntas = DB::select('select preg_nombre, if(TIPE_ID = 2 OR TIPE_ID = 14, (select opci_nombre from opciones where opci_id = pd.perd_respuesta), perd_respuesta) as RESULTADO from preguntas p inner join pregunta_seccion ps on  ps.preg_id = p.preg_id inner join seccion_formulario
        sf on sf.secc_id = ps.secc_id inner join permiso_detalle pd on pd.preg_id = p.preg_id where sf.form_id = ?
        and pd.perf_id = ?', [$idForm, $perf_id]);

        $tipoPregunta = Pregunta::all();

        return view('permiso.detalle_preguntas',
        compact('Permiso','Preguntas', 'Titulo', 'tipoPregunta','IdPermiso','idForm','perf_id'));
    }

    public function GenerarActa($id, $tipo, $perfID){

        $vistas = ['OT', 'RRP', 'CDC', 'CDH', 'SHO', 'SRFO', 'CDF'];
        $vista = 'ActaProyecto';

        $preguntas = $this->GetArrayPreguntas($id);
        $permiso = Permiso::find($id);

        $fecha_creacion = PermisoFormulario::where('perf_id', $perfID)->first()->perf_fecha;
        $perm_codigo = Permiso::where('perm_id',$id)->first()->perm_codigo;


        $pdf = PDF::loadView('permiso.actas.'. $tipo, compact('preguntas', 'permiso', 'fecha_creacion','perm_codigo'));
        return $pdf->stream('acta.pdf');

    }

    public function GetArrayPreguntas($id){

        $preguntas = PermisoFormulario::where('perm_id', $id)->join('permiso_detalle as pd', 'pd.perf_id', 'permisos_formulario.perf_id')
        ->join('preguntas as p', 'p.preg_id', 'pd.preg_id')->get(['p.preg_id', 'tipe_id', DB::raw('if(TIPE_ID = 2 OR TIPE_ID = 14, (select opci_nombre from opciones where opci_id = pd.perd_respuesta), perd_respuesta) as respuesta')]);

        $Datos = [];
        foreach($preguntas as $item){
            $Datos[$item->preg_id] = $item->respuesta;
        }
        return $Datos;
    }

    public function guardarActualizacion(Request $request){

        foreach($request->campoPregunta as $seccion => $preguntas){
            foreach($preguntas as $id => $valor){
               DB::update('update permiso_detalle set perd_respuesta = ? where perf_id = ? and secc_id = ? and preg_id = ?', [$valor, $request->PFormulario, $seccion, $id]);
            }
        }

        if(isset($request->campoPreguntaFile)){
            foreach($request->campoPreguntaFile as $seccion => $preguntas ){

                foreach($preguntas as $id => $valor){
                    $ext = $preguntas[$id]->getClientOriginalExtension();
                    $identificador = Session::get('nuevo-permiso').'_'.$id.'.'.$ext;
                    $valor = $identificador;
                    $preguntas[$id]->move(storage_path('uploads'), $identificador);

                    DetallePermiso::updateOrCreate(['perd_respuesta' => $valor, 'perf_id' => $request->PFormulario, 'secc_id' => $seccion, 'preg_id' => $id])
                    ->where([ 'perf_id' => $request->PFormulario, 'secc_id' => $seccion, 'preg_id' => $id]);
                }
            }
        }

        DB::update('update permisos set esta_id = (select estc_id from estado_control where estc_tipo = 0
        and estc_cambio = 1 and form_id = permisos.form_id and esta_id = permisos.esta_id) where perm_id = ?', [Session::get('nuevo-permiso')]);
        return $this->index();
    }




}
