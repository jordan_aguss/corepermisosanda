<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\ClasesExternas\MetodosGenerales;

class TipoObservacion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    { 
        Schema::create(MetodosGenerales::$Esquema . 'tipo_observacion', function (Blueprint $table) {
            $table->id('TIPO_ID')->comment('ID DE LA TABLA');
            $table->string('TIPO_OBSERVACION')->comment('NOMBRE DE LA OBSERVACION');
            $table->unsignedInteger('TIPO_ACTIVO')->comment('ESTADO');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(MetodosGenerales::$Esquema . 'tipo_observacion');
    }
}
