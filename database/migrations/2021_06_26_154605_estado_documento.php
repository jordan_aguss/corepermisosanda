<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\ClasesExternas\MetodosGenerales;

class EstadoDocumento extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    { 
        Schema::create(MetodosGenerales::$Esquema . 'estado_documento', function (Blueprint $table) {
            $table->id('ESTD_ID')->comment('ID DE LA TABLA');
            $table->string('ESTD_NOMBRE', 50)->comment('NOMBRE DEL ESTADO');
            $table->string('ESTD_AYUDA')->comment('MENSAJE DE AYDUA');
            $table->unsignedInteger('ESTD_ACTIVO')->comment('ESTADO');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(MetodosGenerales::$Esquema . 'estado_documento');
    }
}
