<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class PreguntaOpcion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    { 
        Schema::create('pregunta_opcion', function (Blueprint $table) {
            $table->id('PREO_ID')->comment('id de la tabla');
            $table->unsignedInteger('PREG_ID')->comment('Id de la pregunta');
            $table->string('OPCI_NOMBRE',100)->comment('Nombre de la opcion');
            $table->unsignedInteger('PREO_ACTIVO')->comment('Estado de las opciones de la pregunta');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('pregunta_opcion');
    }
}
