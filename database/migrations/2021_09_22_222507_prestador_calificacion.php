<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class PrestadorCalificacion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    { 
        Schema::create('prestador_calificacion', function (Blueprint $table) {
            $table->unsignedBigInteger('PROY_ID')->comment('Id del proyecto');
            $table->unsignedBigInteger('PRES_ID')->comment('Id del prestador evaluado');
            $table->unsignedInteger('PRES_CALIFICACION')->comment('Calificación asignada por el sistema interno y externo');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('prestador_calificacion');
    }
}
