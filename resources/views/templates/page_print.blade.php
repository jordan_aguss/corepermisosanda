<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>Plataforma Ambiental</title>
        <link rel="stylesheet" type="text/css" href="{{asset('css/bootstrap.min.css')}}">
        <link href="{{asset('css/admin.min.css')}}" rel="stylesheet">
        
        <script src="https://kit.fontawesome.com/a076d05399.js"></script>
        <style>

            @media print{
        
                .divPrint{
                    flex: 0 0 100%!important;
                    max-width: 100%!important;
                    margin: 0%!important;
                    padding: 0%!important;
                    box-shadow: 0!important;
                }
                    
            }

       
            .fabb {
                width: 70px;
                height: 70px;
                background-color:rgb(41, 54, 67);
                border-radius: 50%;
                z-index: 5;
                position: fixed;
                right: 30px;
                bottom: 30px;
            }

        </style>
    </header>
    <body>

        <button class="fabb d-print-none shadow d-flex justify-content-center pt-2">
            <i class="fas fa-cloud-download-alt black mt-1 text-white" onclick="Imprimir()" style="font-size: 40px;"></i>
        </button>

        <div class="col-12 m-0 p-0 mt-5 whiteSmoke divPrint">
            <div class="shadow col-xl-8 col-md-10 col-sm-12 offset-xl-2 offset-md-1 divPrint">
                <div class="card-body">
                    @include('forms.ProyectoCpdf')
                </div>                
            </div>
        </div>  
        
        <div class="col-12 d-print-none mb-5 mt-5"></div>

    </body>
</html>
