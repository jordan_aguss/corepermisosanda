<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\ClasesExternas\MetodosGenerales;

class DiaNoLaboral extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    { 
        Schema::create(MetodosGenerales::$Esquema . 'dia_no_laboral', function (Blueprint $table) {
            $table->id('DINL_ID')->comment('ID DE LA TABLA');
            $table->string('DINL_ANIO',4)->comment('año');
            $table->datetime('DINL_FECHA')->comment('Fecha del dia no laboral');
            $table->string('DINL_DESCRIPCION')->comment('Descripcion del dia no laboral');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(MetodosGenerales::$Esquema . 'dia_no_laboral');
    }
}
