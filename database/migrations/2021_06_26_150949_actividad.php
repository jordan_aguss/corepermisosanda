<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\ClasesExternas\MetodosGenerales;

class Actividad extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    { 
        Schema::create(MetodosGenerales::$Esquema . 'actividad', function (Blueprint $table) {
            $table->id('ACTI_ID')->comment('ID DE LA TABLA');
            $table->string('ACTI_NOMBRE')->comment('NOMBRE DE LA ACTIVIDAD');
            $table->string('ACTI_TIPO', 10)->comment('TIPO');
            $table->unsignedInteger('ACTI_ACTIVO')->comment('ESTADO');
            $table->unsignedInteger('ESTG_ID')->comment('ESTADO');
            $table->string('TAB_PARAMETRO', 10)->comment('');
            $table->string('TABLA_REFERENCIA', 25)->comment('');
            $table->string('ACTI_IMG_AYUDA')->comment('');
            $table->string('COD')->comment('');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(MetodosGenerales::$Esquema . 'actividad');
    }
}
