<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\ClasesExternas\MetodosGenerales;

class TitularEstado extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
     { 
         Schema::create(MetodosGenerales::$Esquema . 'titular_estado', function (Blueprint $table) {
             $table->id('TITE_ID');
             $table->string('TITE_NOMBRE',100);
         });
     }
 
     /**
      * Reverse the migrations.
      *
      * @return void
      */
     public function down()
     {
         Schema::drop(MetodosGenerales::$Esquema . 'titular_estado');
     }
}
