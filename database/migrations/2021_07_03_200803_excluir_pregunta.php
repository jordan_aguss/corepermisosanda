<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ExcluirPregunta extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    { 
        Schema::create('excluir_pregunta', function (Blueprint $table) {
            $table->id('EXCP_ID')->comment('Id de la tabla');
            $table->unsignedInteger('PREG_ID')->nullable()->comment('id de la pregunta a excluir');
            $table->unsignedInteger('SECT_ID')->nullable()->comment('id del sector vinculado');
            $table->unsignedInteger('EXCP_ACTIVO')->nullable()->comment('Estado de la tabla');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('excluir_pregunta');
    }
}
