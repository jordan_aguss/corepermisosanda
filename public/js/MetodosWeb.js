var token = $('meta[name=_token]').attr('content');
var urlBack = '';


function LoadPage(url){
    urlBack = url;
    MakeRequestData(url, '#bodyContent', true);
}

$("body").on("click", ".page-link", function(event){
    event.preventDefault();
    ReplaceData($(this).attr('href'), '#TablePageDivs', true);
});

function readURL(input, Name, inputId) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $(Name).html($(inputId).val());
        };
        reader.readAsDataURL(input.files[0]);
    }
}

function AlertConfirmacion(TextAlert){
    return new Promise((resolve, reject) => {
        let Titulo = $(TextAlert).text();

        Swal.fire({
            text: Titulo,
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: 'Cancelar',
            allowOutsideClick: false,
            confirmButtonText: 'Aceptar',
        }).then((result) =>{
            resolve(result.isConfirmed);
        });
    });
}

function getDataRequest(IdForm, ...Datos){
    if(IdForm != ''){
        var Form = $(IdForm)[0];
        return new FormData(Form);
    }
    let DataSend = { '_token': token };

    for( var i = 0; i < Datos.length; i++){
        let clave = Datos[i].split('/');
        DataSend[clave[0]] = clave[1];
    }
    return DataSend;
}

function MakeRequestData(Url, ItemReplace, HideLoad = false, ModalName = '', type = 'GET',
    accion = 2, IdForm = '', ShowResult = false, closeModal= false, ...params){
    let DataSend = getDataRequest(IdForm, ...params);
    if(HideLoad) LoadingAlert();

    let confi = {
        type: type,
        url: Url,
        cache: false,
        data: (type == 'POST' ? DataSend : null),
        timeout: 800000,
        async: true,
        success: function(response) {
            if(ModalName != '') $(ModalName).modal('show');
             console.log('Metodo replace');
            SetDataResult(ItemReplace, accion, response);
            Swal.close();

            $('.selectpicker').selectpicker('refresh');
            if(ShowResult) ShowAlert('Exito', 'El proceso se ha realizado correctamente','success', false);

            if($(ModalName).is(':visible') && closeModal) $(ModalName).modal('hide');
        },
        error: function(error){ ErrorRequest(error); }
    }

    if(IdForm != '') {
        confi['processData'] = false;
        confi['contentType'] = false;
    }
    $.ajax(confi);
}

function ReplaceData(Url, ItemReplace, HideLoad = false, ModalName = '', type = 'GET',
            accion = 2, IdForm = '', ShowResult = false, closeModal= false, ...params){
    if(params.length > 0 && params[0].includes('NeedAsk/#')){
        let info = params[0].split('/');
        params.shift();
        AlertConfirmacion(info[1])
        .then(response => {
            if(response){
                MakeRequestData(Url, ItemReplace, HideLoad, ModalName, type,
                accion = 2, IdForm, ShowResult, closeModal, ...params);
            }
        });
    }else{
        MakeRequestData(Url, ItemReplace, HideLoad, ModalName, type,
        accion = 2, IdForm, ShowResult, closeModal, ...params);
    }
}

function SetDataResult(ItemReplace, accion, response){
    switch(accion){
        case 0:
            $(ItemReplace).append(response);
            break;
        case 1:
            location.reload();
            break;
        case 2:
            $(ItemReplace).html(response);
            break;
        case 3:
            $(ItemReplace).replaceWith(response);
            break;
        case 4:
            location.replace(response);
            break;
    }
}

function ErrorRequest(response){

    Swal.close();
    let r = jQuery.parseJSON(response.responseText);

    if(r.includes('^')){
        ShowAlert('Error', r.replace('^',''),'error');
    }else if (r.includes('/Sesion')){
        location.reload();
    }
    else{
        ShowAlert('Error', 'Ocurrio un error al procesar la solicitud.','error');
    }
}

function LoadingAlert(){
    Swal.fire({
        type: 'info',
        html: '<div class="spinner-border text-warning col-12" role="status"></div><span class="col-12 text-center">Cargando</span>',
        allowOutsideClick: false,
        showConfirmButton: false,
        width: 160,
        padding: '0'
    });
}

function ShowAlert(Titulo, Mensaje, Icono, showButton = true){
    Swal.fire({
        title: Titulo,
        text: Mensaje,
        icon: Icono,
        confirmButtonColor: '#293643',
        showConfirmButton: showButton,
        allowOutsideClick: false,
        toast: true,
        confirmButtonText:'Aceptar'
    });
}
