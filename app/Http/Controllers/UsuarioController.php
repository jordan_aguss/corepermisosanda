<?php

namespace App\Http\Controllers;

use App\Models\PreguntaSeccion;
use App\Models\User;
use App\Models\Usuario;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class UsuarioController extends Controller
{
    public function index(){

        $Preguntas = PreguntaSeccion::join('preguntas as p', 'p.preg_id', 'pregunta_seccion.preg_id')
        ->join('secciones as s', 's.secc_id', 'pregunta_seccion.secc_id')->
        orderBy('s.secc_id')->join('seccion_formulario as sf', 'sf.secc_id', 's.secc_id')
        ->where('preg_padre', 1)->where('form_id', 10)->get(['p.*', 's.secc_id', 's.secc_nombre']);
        return view('usuarios.index', compact('Preguntas'));
    }

    public function SaveUser(Request $request){

        try{

            $Info = Usuario::where('usua_mail', $request->correo)->first();

            if(isset($Info->usua_id)){
                throw new Exception('^Ya existe un usuario con ese correo');
            }

            return DB::transaction(function() use ($request){

                $user = new Usuario();
                $user->usua_mail = $request->correo;
                $user->usua_pass = password_hash($request->pass, PASSWORD_DEFAULT);
                $user->usua_nombre = $request->nombre;
                $user->save();

                foreach($request->campoPregunta as $id => $valor){
                    DB::insert('insert into usuario_detalle (preg_id, usua_id, usud_valor) values (?,?,?)', [$id, $user->usua_id, $valor]);
                }
                return route('login');
            });
        }catch(Exception $ex){
            return response()->json($ex->getMessage(), 500);
        }

    }
}
