<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PreguntaSeccion extends Model
{
    use HasFactory;
    protected $table = "pregunta_seccion";
    protected $primaryKey = 'secc_id';
    public $incrementing = true;
}
