<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use App\ClasesExternas\MetodosGenerales;
use App\Models\Formulario;
use App\Models\Permiso;
use App\Models\Pregunta;
use App\Models\Registro;
use App\Models\Sector;
use App\Models\Sucursal;
use Exception;
use Illuminate\Support\Facades\Config;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class ProyectoController extends Controller {
    
    private $OtrosSectores = [69,71,72,73,74,94,95,96,97,99,100,101,116,120,122];

    public function PermisosForm()
    {
        $Info = Sucursal::find(Config::get('constants.id_sucursal'));
        $Forms = Formulario::where('sucu_id', Config::get('constants.id_sucursal'))->get();
        Log::info(Config::get('constants.id_sucursal'));
        $Permiso = Permiso::where('usua_id', 1)->where('esta_id', -1)->first();
        return view('forms.proyecto_form', compact('Info', 'Forms', 'Permiso'));
    }

    public function Seguimiento()
    {
        $Info = Sucursal::find(Config::get('constants.id_sucursal'));
        $Forms = Formulario::where('sucu_id', Config::get('constants.id_sucursal'))->get();
        $Permiso = Permiso::where('usua_id', 1)->where('esta_id', -1)->first();

        return view('workflow.index', compact('Info', 'Forms', 'Permiso'));
    }

    public function FormProyecto(){
        $Sectores = Sector::where('SECT_VISIBLE', 1)->orwhereIn('SECT_ID',[69,71,72,73,74,93,94,95,96,97,98,99,100,101,116,117,120,121,122 ])
        ->select('SECT_ID', 'SECT_NOMBRE','SECT_NIVEL', 'SECT_ATOMICO', DB::raw('rpad(SECT_CODIGO, 8, "0") AS CODIGO'))->orderBy('SECT_CODIGO')->get();
        $form = 'proyecto.form_proyecto';
        return view('HomeView', compact('Sectores'));
    }
    
    public function GetSubPreguntas($Pregunta, $Opcion){
        $BaseQuery = 'select distinct p.PREG_ID, p.PREG_LONGITUD, if(isnull(pa.PREG_ID), 0, 1) AS DEPENDE, p.PREG_NOMBRE, p.SECT_ID, s.SECT_CODIGO, '
                . 'p.TIPE_ID, 0 as SECF_ID, p.PREG_ORDEN, p.PREG_AYUDA from pregunta p inner join sector s on s.SECT_ID = p.SECT_ID'
                . ' left join pregunta_accion pa on pa.PREG_ID = p.PREG_ID where p.PREG_ACTIVO = 1 and p.PREG_ID in '
                . '(select PREG_ID_DEPENDE from pregunta_accion where PREG_ID = ? and PREO_ID = ?) Order by PREG_ORDEN';
        
        $Preguntas = DB::select($BaseQuery, [$Pregunta, $Opcion]);       
        return view('forms.preguntas_form', ['Preguntas' => $Preguntas, 'ClaseMetodos' => new MetodosGenerales()]);
    }
    
    public function GetPreguntasSector($IdSector){
        
        $info = explode('-', $IdSector);
        $IdSector = $info[0];

        if(in_array($info[1], $this->OtrosSectores)){

            $Sector = Sector::where('SECT_ID', $info[1])->select(DB::raw('rpad(SECT_CODIGO, 8, "0") AS CODIGO'),'SECT_NOMBRE', 'SECT_NIVEL','SECT_ATOMICO','SECT_URLDOC')->first();

            if(!in_array($info[1], [94,95,96,97,99,100,101])){
                $Nota = "Para este tipo de Actividad, obra o proyecto a realizar usted necesita descargar los siguientes documentos que deben ser completados, firmados y escaneados. <strong> Dichos documentos y las factibilidades de abastecimiento de agua, energia, aguas residuales y desechos solidos</strong> forman parte de la carpeta tecnica solicitada del proyecto:";
            }else{
                $Nota = "Para éste tipo de solicitud deberá descargar los documentos a continuación, completarlos y posterior escanearlos en formato PDF y adjuntar en el apartado de carpeta técnica. Exceptuando escanear los que poseen asterisco (*)";
            }
            
            $Almacenamiento = in_array($info[1], [96,100]);
            $Transporte = in_array($info[1], [99,95]);
            $Preguntas = Pregunta::whereIn('pregunta.PREG_ID',[1246])->join('seccion_formulario','seccion_formulario.SECF_ID', 'pregunta.SECF_ID')
            ->select('pregunta.PREG_ID','PREG_LONGITUD','pregunta.TIPE_ID','PRED_ID','SECF_NOMBRE','pregunta.SECF_ID','PREG_AYUDA','PREG_NOMBRE')->get();

            return view('forms.preg_otros_sectores', compact('Nota','Sector','Almacenamiento','Transporte', 'Preguntas'));
        }

        $result = $this->getSectorPregunta($IdSector);
        
        if(count($result) > 0){
            $SectorVinculo = $result[0]->VINCULO;
        }else{
            $SectorVinculo = ' ';
        }
       
        $baseQuery = 'SELECT DISTINCT p.PREG_ID, p.PREG_LONGITUD, IF(ISNULL(pa.PREA_ID), 0, 1) AS DEPENDE, p.PREG_NOMBRE, s.SECT_ID, s.SECT_CODIGO, 
            p.TIPE_ID, p.PRED_ID, sf.SECF_NOMBRE, p.SECF_ID, sf.SECF_ORDEN AS ORDEN_SECCION, p.PREG_ORDEN, p.PREG_AYUDA FROM pregunta p INNER JOIN  seccion_formulario sf
            ON p.SECF_ID = sf.SECF_ID INNER JOIN sector s ON p.SECT_ID = s.SECT_ID LEFT JOIN pregunta_accion pa ON pa.PREG_ID = p.PREG_ID WHERE p.PREG_ACTIVO = 1 AND p.PRED_ID = 0 AND ';
        
        $baseWhere = "s.SECT_CODIGO = '00' OR rpad(s.SECT_CODIGO, 8, 0) = ? OR ( rpad(s.SECT_CODIGO, 6, 0) = ? AND s.SECT_NIVEL = 3 ) "
                . "OR ( rpad(s.SECT_CODIGO, 4,0) = ? AND s.SECT_NIVEL = 2 ) OR ( rpad(s.SECT_CODIGO, 2, 0) = ? AND s.SECT_NIVEL = 1 )";
        
        $Nivel3 = substr($IdSector, 0, 6);
        $Nivel2 = substr($IdSector, 0, 4);
        $Nivel1 = substr($IdSector, 0, 2);
        
        $Preguntas = DB::select("select * from (($baseQuery s.SECT_CODIGO = '$SectorVinculo') union ($baseQuery ($baseWhere))) as Preguntas"
                . " where not PREG_ID in (SELECT ep.PREG_ID FROM excluir_pregunta ep INNER JOIN sector s ON ep.SECT_ID = s.SECT_ID where $baseWhere )"
                . " order by ORDEN_SECCION, PREG_ORDEN", [$IdSector, $Nivel3, $Nivel2, $Nivel1, $IdSector, $Nivel3, $Nivel2, $Nivel1]);
        
        return view('forms.preguntas_form', ['Preguntas' => $Preguntas, 'ClaseMetodos' => new MetodosGenerales()]);
    }
    
    private function getSectorPregunta($IdSector){
        return DB::select('SELECT S1.SECT_CODIGO AS VINCULO FROM sector s INNER JOIN vincular_sector vs ON s.SECT_ID = vs.SECT_PRINCIPAL
            INNER JOIN sector S1 ON vs.SECT_VINCULADO = S1.SECT_ID where rpad(s.SECT_CODIGO , 8, "0") = ?',[$IdSector]);
    }
}