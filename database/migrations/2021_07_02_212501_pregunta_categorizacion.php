<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class PreguntaCategorizacion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    { 
        Schema::create('pregunta_categorizacion', function (Blueprint $table) {
            $table->id('PREC_ID')->comment('Id de la opcion');
            $table->unsignedInteger('PREG_ID')->comment('Id de la pregunta');
            $table->unsignedInteger('SECT_ID')->comment('Id de la seccion');
            $table->unsignedInteger('PREO_ID')->comment('Id de la pregunta opcion');
            $table->unsignedInteger('CATE_ID')->comment('Id categorizacion');
            $table->unsignedInteger('PREC_VI')->comment('');
            $table->unsignedInteger('PREC_VF')->comment('');
            $table->unsignedInteger('PREC_PONDERACION')->comment('');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('pregunta_categorizacion');
    }
}
