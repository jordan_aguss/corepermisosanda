<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\ClasesExternas\MetodosGenerales;

class DetalleObservacion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    { 
        Schema::create(MetodosGenerales::$Esquema . 'detalle_observacion', function (Blueprint $table) {
            $table->id('DETO_ID')->comment('ID DE LA TABLA');
            $table->string('ACTI_NOMBRE')->comment('NOMBRE DE LA ACTIVIDAD');
            $table->string('DETO_OBSERVACION')->comment('TIPO');
            $table->string('DETO_TIPO', 50)->comment('TIPO');
            $table->string('DETO_ARCHIVO')->comment('TIPO');
            $table->datetime('DETO_CREACION')->comment('TIPO');
            $table->unsignedInteger('DETO_ACTIVO')->comment('ESTADO');
            $table->unsignedInteger('OBSE_ID')->comment('ESTADO');
            $table->unsignedInteger('ESTU_ID')->comment('ESTADO');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(MetodosGenerales::$Esquema . 'detalle_observacion');
    }
}
