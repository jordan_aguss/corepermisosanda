<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\ClasesExternas\MetodosGenerales;

class TipoModificacion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    { 
        Schema::create(MetodosGenerales::$Esquema . 'tipo_modificacion', function (Blueprint $table) {
            $table->id('TIPM_ID')->comment('ID DE LA TABLA');
            $table->string('TIPM_TIPO',200)->comment('');
            $table->string('TIPM_ALCANCE',50)->comment('');
            $table->unsignedInteger('TIPM_ACTIVO')->comment('');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(MetodosGenerales::$Esquema . 'tipo_modificacion');
    }
}
