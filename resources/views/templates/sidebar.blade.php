
<div id="layoutSidenav_nav">
    <nav class="sidenav shadow-right sidenav-dark">
        <div class="sidenav-menu ">
            <div class="nav accordion" id="accordionSidenav">
                <div class="mt-3">
                    <img class="img-fluid "
                        src="{{$Info->sucu_logo}}" alt="" draggable="false">
                </div>

                <!-- Sidenav Menu Heading (Core)-->

                <div class="sidenav-menu-heading">MENU PRINCIPAL</div>
                <a class="nav-link " href="{{route('home-home')}}">Inicio</a>


                @if (App\Models\User::find(Auth::id())->name != 'AGENCIA')


                <a class="nav-link collapsed active" href="javascript:void(0);" data-bs-toggle="collapse"
                    data-bs-target="#collapseDashboards" aria-expanded="false"
                    aria-controls="collapseDashboards">
                    <div class="nav-link-icon "><i data-feather="activity"></i></div>
                    <span class=""> Solicitante</span>

                    <div class="sidenav-collapse-arrow text-light"><i class="fas fa-angle-down"></i></div>
                </a>
                    <div class="collapse show" id="collapseDashboards" data-bs-parent="#accordionSidenav">
                        <nav class="sidenav-menu-nested nav accordion" id="accordionSidenavPages">

                            <a class="nav-link" href="#" onclick="LoadPage('{{route('Proyecto.Form', [7])}}')">Solicitudes de Factibilidad</a>
                           <a class="nav-link " href="#" onclick="LoadPage('{{route('permisos.index')}}')">Permisos</a>
                           <a class="nav-link " href="#" onclick="LoadPage('{{route('notificaciones')}}')">Notificaciones
                                @if ($Noty > 0)
                                &nbsp; <span class="badge bg-danger me-2">{{$Noty}}</span>
                                @endif
                            </a>
                        </nav>
                    </div>

                    @endif

                @can('agencia')
                    <a class="nav-link collapsed active" href="javascript:void(0);" data-bs-toggle="collapse"
                    data-bs-target="#SegundoMenu" aria-expanded="false"
                    aria-controls="SegundoMenu">
                    <div class="nav-link-icon "><i data-feather="activity"></i></div>
                    <span class=""> Agencia</span>

                    <div class="sidenav-collapse-arrow text-light"><i class="fas fa-angle-down"></i></div>
                </a>
                    <div class="collapse show" id="SegundoMenu" data-bs-parent="#accordionSidenav">
                        <nav class="sidenav-menu-nested nav accordion" id="accordionSidenavPages">
                            <a class="nav-link" href="#" onclick="LoadPage('{{route('Proyecto.Form', [7])}}')">Solicitudes de Factibilidad</a>
                            <a class="nav-link " href="#" onclick="LoadPage('{{route('permisosAll.index')}}')">Permisos</a>
                            <a class="nav-link " href="#" onclick="LoadPage('{{route('notificaciones')}}')">Notificaciones
                                @if ($Noty > 0)
                                &nbsp; <span class="badge bg-danger me-2">{{$Noty}}</span>
                                @endif
                            </a>
                        </nav>
                    </div>
                @endcan

                    @can('menuFRegional')
                    <a class="nav-link collapsed active" href="javascript:void(0);" data-bs-toggle="collapse"
                    data-bs-target="#TercerMenu" aria-expanded="false"
                    aria-controls="TercerMenu">
                    <div class="nav-link-icon "><i data-feather="activity"></i></div>
                    <span class=""> Factibilidad Regional</span>

                    <div class="sidenav-collapse-arrow text-light"><i class="fas fa-angle-down"></i></div>
                </a>
                    <div class="collapse show" id="TercerMenu" data-bs-parent="#accordionSidenav">
                        <nav class="sidenav-menu-nested nav accordion" id="accordionSidenavPages">
                            <a class="nav-link " href="#" onclick="LoadPage('{{route('permisosAll.index')}}')">Permisos</a>
                            <a class="nav-link " href="#" >Informes de Campo</a>
                        </nav>
                    </div>

                    @endcan

                    @can('menuTCampo')

                    <a class="nav-link collapsed active" href="javascript:void(0);" data-bs-toggle="collapse"
                    data-bs-target="#CuartoMenu" aria-expanded="false"
                    aria-controls="CuartoMenu">
                    <div class="nav-link-icon "><i data-feather="activity"></i></div>
                    <span class=""> Tecnico de Campo</span>

                    <div class="sidenav-collapse-arrow text-light"><i class="fas fa-angle-down"></i></div>
                </a>
                    <div class="collapse show" id="CuartoMenu" data-bs-parent="#accordionSidenav">
                        <nav class="sidenav-menu-nested nav accordion" id="accordionSidenavPages">
                            <a class="nav-link " href="#" >Informes de campo</a>
                        </nav>
                    </div>

                    @endcan

                    @can('menuFCentral')

                    <a class="nav-link collapsed active" href="javascript:void(0);" data-bs-toggle="collapse"
                    data-bs-target="#QuintoMenu" aria-expanded="false"
                    aria-controls="QuintoMenu">
                    <div class="nav-link-icon "><i data-feather="activity"></i></div>
                    <span class=""> Factibilidad Central</span>

                    <div class="sidenav-collapse-arrow text-light"><i class="fas fa-angle-down"></i></div>
                </a>
                    <div class="collapse show" id="QuintoMenu" data-bs-parent="#accordionSidenav">
                        <nav class="sidenav-menu-nested nav accordion" id="accordionSidenavPages">
                            <a class="nav-link " href="#" >Actas de Factibilidad Inicial</a>
                        </nav>
                    </div>

                    @endcan


                    @can('menuComite')

                    <a class="nav-link collapsed active" href="javascript:void(0);" data-bs-toggle="collapse"
                    data-bs-target="#SextoMenu" aria-expanded="false"
                    aria-controls="SextoMenu">
                    <div class="nav-link-icon "><i data-feather="activity"></i></div>
                    <span class="">Comite Factibilidad</span>

                    <div class="sidenav-collapse-arrow text-light"><i class="fas fa-angle-down"></i></div>
                </a>
                    <div class="collapse show" id="SextoMenu" data-bs-parent="#accordionSidenav">
                        <nav class="sidenav-menu-nested nav accordion" id="accordionSidenavPages">
                            <a class="nav-link " href="#" >Actas de Factibilidad Inicial</a>
                        </nav>
                    </div>

                    @endcan

                    @can('menuGerencia')

                    <a class="nav-link collapsed active" href="javascript:void(0);" data-bs-toggle="collapse"
                    data-bs-target="#SeptimoMenu" aria-expanded="false"
                    aria-controls="SeptimoMenu">
                    <div class="nav-link-icon "><i data-feather="activity"></i></div>
                    <span class="">Gerencia Regional</span>

                    <div class="sidenav-collapse-arrow text-light"><i class="fas fa-angle-down"></i></div>
                </a>
                    <div class="collapse show" id="SeptimoMenu" data-bs-parent="#accordionSidenav">
                        <nav class="sidenav-menu-nested nav accordion" id="accordionSidenavPages">
                            <a class="nav-link " href="#" >Actas de Factibilidad Inicial</a>
                        </nav>
                    </div>
                    @endcan

            </div>
        </div>
    </nav>
</div>


