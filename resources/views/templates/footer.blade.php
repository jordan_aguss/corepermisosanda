<footer class="sticky-footer bg-white mt-3 p-3">
    
        <div class='row d-flex align-items-center ps-3 pe-3'>

            
            <div class="col-xl-4 col-md-6 col-sm-12 d-flex justify-content-center mb-2">            
                <img src="{{$Info->sucu_logo}}" alt="" height="90" draggable="false">
            </div>
            
            <div class=" row col-xl-4 col-md-6 col-sm-12">

                <h5 class="col-12 text-left mb-2">Redes sociales</h5>

                <div class="col-xl-6 col-md-6 col-sm-12 text-left">
                    <i class="fab fa-facebook-square colorSeaText small">&nbsp&nbsp&nbsp /marn.gob.sv</i>
                </div>
                
                <div class="col-xl-6 col-md-6 col-sm-12 text-left">
                    <i class="fab fa-twitter-square colorSeaText small">&nbsp&nbsp&nbsp @MARN_SV</i>
                </div>
                <div class="col-xl-6 col-md-6 col-sm-12 text-left">
                    <i class="fab fa-youtube colorSeaText small">&nbsp&nbsp&nbsp /MARNsv</i>
                </div>
                <div class="col-xl-6 col-md-6 col-sm-12 text-left mb-3">
                    <i class="fa fa-envelope colorSeaText small">&nbsp&nbsp&nbsp sea@marn.gob.sv</i>
                </div>
            </div>
            
            <div class=" col-xl-4 col-md-12 col-sm-12">
                
                <h5 class="col-12 text-left mb-2">Dirección</h5>
                <h6 class='text-left colorSeaText small col-12'> {{$Info->sucu_direccion}} </h6>
                <h6 class='text-left col-12 colorSeaText small'>Telefono: {{$Info->sucu_telefono}} </h6>
            </div>


        </div>

</footer>