<!DOCTYPE html>
<html>

<head>

  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css" integrity="sha384-zCbKRCUGaJDkqS1kPbPd7TveP5iyJE0EjAuZQTgFLD2ylzuqKfdKlfG/eSrtxUkn" crossorigin="anonymous">
</head>

<script>function printHTML() {
  if (window.print) {
    window.print();
  }
}</script>



<body>

@php

$inicio= '@foreach($permisoDetalle as $item) @if($item->preg_id ==';
$fin = ') {{$item->perd_respuesta}} @break @endif @endforeach';
    
@endphp




<div class="container"> 
  <div class="card">
    <div class="card-header font-weight-bold"> <br>

    <center>   <h1>COMUNIDADES</h1></center>

      <p class="text-right"> @foreach($permisoDetalle as $item) @if($item->preg_id ==36) {{$item->perd_respuesta}} @break @endif @endforeach</p> <br>

      <p class="font-weight-bold">Señores, <br>
      UNIDAD DE FACTIBILIDADES <br>
      ANDA <br>
      PRESENTE
      </p>
      <a onclick="printHTML()" class="btn"><img src="{{asset('images/printer.png')}}" style="height: 25px;" /></a>
    </div>

    <div class="card-body">

      Estimados Sres.: <br>

      Atentamente solicito a usted el Certificado de Factibilidad de servicios de agua potable y aguas negras la comunidad de
    <strong> @foreach($permisoDetalle as $item) @if($item->preg_id ==90) {{$item->perd_respuesta}} @break @endif @endforeach </strong>, ubicado
    en  <strong> @foreach($permisoDetalle as $item) @if($item->preg_id ==77) {{$item->perd_respuesta}}@break @endif @endforeach</strong>,  <strong> @foreach($permisoDetalle as $item) @if($item->preg_id ==76) {{$item->perd_respuesta}} @break @endif @endforeach .</strong>

   <br><br> N° DE SERVICIOS SOLICITADOS <strong> @foreach($permisoDetalle as $item) @if($item->preg_id ==93) {{$item->perd_respuesta}}@break @endif @endforeach</strong> DOMICILIAR

   <br><br>

   <strong> <p>N° DE FACTIBILIDAD </strong>  @foreach($permisoDetalle as $item) @if($item->preg_id ==78) {{$item->perd_respuesta}} @break @endif @endforeach</p>
   <strong>  <p>REFERENCIA </strong> @foreach($permisoDetalle as $item) @if($item->preg_id ==79) {{$item->perd_respuesta}} @break @endif @endforeach </p>
   <strong> <p>FECHA EMISION </strong> @foreach($permisoDetalle as $item) @if($item->preg_id ==38) {{$item->perd_respuesta}} @break @endif @endforeach</p> 
   
   <br><h5>DATOS CARACTERISTICOS DE LA COMUNIDAD</h5>

   <table class="table table-hover">
   
 
      <tbody>
        <tr>
          <th scope="row">Numero de Viviendas</th>
          <td>@foreach($permisoDetalle as $item) @if($item->preg_id ==81) {{$item->perd_respuesta}}@break @endif @endforeach</td>
        </tr>
        <tr>
          <th scope="row">Area Promedia de Vivienda</th>
          <td>@foreach($permisoDetalle as $item) @if($item->preg_id ==82) {{$item->perd_respuesta}}@break @endif @endforeach M</td>
        </tr>
        <tr>
          <th scope="row">Direccion P / Notificar</th>
          <td>@foreach($permisoDetalle as $item) @if($item->preg_id ==83) {{$item->perd_respuesta}}@break @endif @endforeach</td>
        </tr>
        <tr>
          <th scope="row">Telefono</th>
          <td>@foreach($permisoDetalle as $item) @if($item->preg_id ==84) {{$item->perd_respuesta}}@break @endif @endforeach / @foreach($permisoDetalle as $item) @if($item->preg_id ==85) {{$item->perd_respuesta}}@break @endif @endforeach</td>
        </tr>
        <tr>
          <th scope="row">Correo Electronico</th>
          <td>@foreach($permisoDetalle as $item) @if($item->preg_id ==91) {{$item->perd_respuesta}}@break @endif @endforeach</td>
        </tr>
      </tbody>
  </table>

  <p>
  
  <br><strong> <h5>NOMBRE Y FIRMA DEL REPRESENTANTE DE LA COMUNIDAD:</h5> </strong><br><br>

  __________________________________________ <br>
  <strong> {{Auth::user()->name}} </strong> <br>
  @foreach($permisoDetalle as $item) @if($item->preg_id ==41) {{$item->perd_respuesta}}@break @endif @endforeach
</p>
      

    </div>
    
  </div>
</div>




















<!--
<center><h3>Proyecto -  @foreach($permisoDetalle as $item) @if($item->preg_id ==41) {{$item->perd_respuesta}} @break @endif @endforeach</h3></center>
    <center><h2>Nombre del Solicitante: {{Auth::user()->name}} </h2></center>
    <table class="table table-hover">
        <thead>
          <tr>
            <th scope="col">ID Permiso</th>
            <th scope="col">Pregunta</th>
            <th scope="col">Respuesta</th> 
          </tr>
        </thead>
        @foreach ($permisoDetalle as $item)
        <tbody>
          <tr>
            <th>{{$item->perm_id}}</th>
          @foreach ($preguntaString as $key)
            @if($key->preg_id == $item->preg_id)
             <td>{{$key->preg_nombre}}</td>
            @endif
          @endforeach

            <td>{{$item->perd_respuesta}}</td>
          </tr>
        </tbody>
        @endforeach
      </table> -->

</body>

</html>