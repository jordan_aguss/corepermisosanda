@foreach ($Preguntas as $Pregunta)
    @if (!isset($hijas))
        @if ($FormActual != $Pregunta->secc_id)

        <script>
            $('#cardTab').append(
                '<a onclick="Quitar(this)" data-tab={{ $Tab }} class="nav-item nav-link {{ $FormActual == 0 ? 'active' : '' }} tabSection-{{ $Tab }}" id="tab-{{ $Pregunta->secc_id }}" href="#pagetabe-{{ $Pregunta->secc_id }}"' +
                    'data-bs-toggle="tab" role="tab" aria-selected="true">' +
                    '<div class="wizard-step-icon" id="cardid">{{ $Tab }}</div>' +
                    '<div class="wizard-step-text">' +
                    '<div class="wizard-step-text-name colortexto">{{ $Pregunta->secc_nombre }}</div>' +
                    '</div>' +
                '</a>'
            );
        </script>

            @if ($FormActual != 0)
    </div>

                    @if ($Fill)
                        <script>
                        $('.tabSection-' + '{{ $Tab }}').removeClass('disabled');
                        $('#FormTab-' + '{{ $Tab - 1}}').addClass('enviado');

                        </script>
                        @php
                            $Fill = false;
                        @endphp
                    @else
                        <div class="col-12 m-0 p-0 d-flex justify-content-end align-items-center mt-4 field idSiguiente">
                            <a type="button" class="next btn bg-primary p-2 rounded-2 text-center justify-content-end btnNext"
                                style="color: white" tabItem="{{ $Tab - 1 }}" id="tabboton">
                                Siguiente&nbsp;&nbsp; <i class="fas fa-arrow-right"></i>
                            </a>
                        </div>
                    @endif
                </div>
            @endif



    <div class="card-body form-outer tab-pane fade tabPage-{{ $Tab }} {{ $FormActual == 0 ? ' show active' : '' }}"
                id="pagetabe-{{ $Pregunta->secc_id }}" role="tabpanel">
                <div id="FormTab-{{ $Tab }}" class="formSecundario row" novalidate seccion="{{ $Pregunta->secc_id }}" enctype="multipart/form-data">
                    @csrf
                    <input type="hidden" name="seccion" value="{{ $Pregunta->secc_id  }}" class="d-none iseccion">
                    @php
                        $Tab++;
                    @endphp
        @endif
    @endif

    @php
        $DivDepende = '';
        $Dependiente = '';
        $ErrorMessage = '';

        if ($Pregunta->DEPENDE > 0) {
            $DivDepende = '<div class="col-12 ml-10" id="contenedor' . $Pregunta->preg_id . '"></div>';
            $Dependiente = 'dependiente';
        }
    @endphp
{{-- , 177, 181, 183, 188 --}}
    @php
    switch ($Pregunta->preg_id) {
        case '174':
          echo '<div class="m-1 p-1 d-flex justify-content-between align-items-center alert alert-dark" role="alert"> Datos Personales</div>';
        break;

        case '176':
        echo '<div class="m-1 p-1 mt-5 justify-content-between align-items-center alert alert-dark" role="alert"> Documentos Personales</div>';
        break;

        case '179':
        echo '<div class="m-1 p-1 mt-5 justify-content-between align-items-center alert alert-dark" role="alert">Residencia</div>';
        break;

        case '183':
        echo '<div class="m-1 p-1 mt-5 justify-content-between align-items-center alert alert-dark" role="alert">Contacto</div>';
        break;

        case '185':
        echo '<div class="m-1 p-1 mt-5 justify-content-between align-items-center alert alert-dark" role="alert">Datos Jurídicos</div>';
        break;

        case '419':
        echo '<div class="m-1 p-1 mt-5 justify-content-between align-items-center alert alert-dark" role="alert">Caracterísitcas del Proyecto</div>';
        break;

        case '206':
        echo '<div class="m-1 p-1 justify-content-between align-items-center alert alert-dark" role="alert">Áreas Puntuales</div>';
        break;

        case '209':
        echo '<div class="m-1 p-1 mt-5 justify-content-between align-items-center alert alert-dark" role="alert">Cantidades de Establecimientos</div>';
        break;

        case '215':
        echo '<div class="m-1 p-1 mt-5 justify-content-between align-items-center alert alert-dark" role="alert">Áreas Promedios</div>';
        break;

        case '220':
        echo '<div class="m-1 p-1 mt-5 justify-content-between align-items-center alert alert-dark" role="alert">Áreas Puntuales</div>';
        break;

        case '225':
        echo '<div class="m-1 p-1 mt-5 justify-content-between align-items-center alert alert-dark" role="alert">Cantidades</div>';
        break;

        case '156':
        echo '<div class="m-1 p-1 mt-5 justify-content-between align-items-center alert alert-dark" role="alert">Adjuntos de Proyectos</div>';
        break;

        case '240':
        echo '<div class="m-1 p-1 mt-5 justify-content-between align-items-center alert alert-dark" role="alert">Adjuntos de Comunidades</div>';
        break;

        case '246':
        echo '<div class="m-1 p-1 mt-5 justify-content-between align-items-center alert alert-dark" role="alert">Categoria</div>';
        break;
        
        default:
            # code...
        break;
    }

                    if (Auth::user()->name != 'AGENCIA' && in_array($Pregunta->preg_id, [166, 160])) {
                        $verPregFile = 'hidden';
                                                
                    }else{
                        $verPregFile = '';
                    }

    @endphp
    <div name='campoPregunta[{{$Pregunta->secc_id}}][{{$Pregunta->preg_id}}]' class="col-sm-12 col-xl-6 col-md-6 m-0 row mt-3 form-outer" {{$verPregFile}}>
        <div  class="col-12 m-0 p-0 d-flex justify-content-between align-items-center" id="hija">
            <span class="text-black black small mt-1 jutify-content-end">{{ $Pregunta->preg_nombre }}</span>
            <div type="button" class="btn bg-info p-2 rounded-0 text-center justify-content-end"
                data-bs-toggle="tooltip" data-bs-placement="top" title="{{ $Pregunta->PREG_AYUDA }}">
                <i class="fas fa-question black"></i>
            </div>
        </div>

        @switch($Pregunta->tipe_id)
            @case(2)
                @php
                    $ErrorMessage = 'Seleccione una opción';
                    $selecciones[] = $Pregunta->preg_id;
                    if(in_array($Pregunta->preg_id, [178])){
                        $fSel = 'validarTipoDocumento(this)';
                    }else{
                        $fSel = ''; 
                    }
                @endphp
            <select onchange="{{$fSel}}" class="form-control {{ $Dependiente }} seccion{{$Pregunta->secc_id}}"  id='campoPregunta{{ $Pregunta->preg_id }}'
                    name='campoPregunta[{{$Pregunta->secc_id}}][{{ $Pregunta->preg_id }}]' campoSeleccionado="{{$Pregunta->perd_respuesta}}">
                    <option value="{{ $Pregunta->opci_nombre}}" disabled {{$Pregunta->perd_respuesta == $Pregunta->opci_nombre ? 'selected' : ''}}>seleccion</option>
            </select>
           
            @break
            @case(3)

            @if ( in_array($Pregunta->preg_id, [71,91,182]))
            @php $ErrorMessage = 'Complete este campo'; @endphp
            <input onchange="isEmail(this)" type='email' class='form-control {{ $Dependiente }} seccion{{$Pregunta->secc_id}}' id='campoPregunta{{ $Pregunta->preg_id }}' required
                name='campoPregunta[{{$Pregunta->secc_id}}][{{ $Pregunta->preg_id }}]' min='0' max='10000000' value="{{Auth::user()->email }}">
            @else
                @php $ErrorMessage = 'Complete este campo'; @endphp
                <input type='text' class='form-control {{ $Dependiente }} {{ $Pregunta->vali_clase }} seccion{{$Pregunta->secc_id}}' id='campoPregunta{{ $Pregunta->preg_id }}' required
                    name='campoPregunta[{{$Pregunta->secc_id}}][{{ $Pregunta->preg_id }}]' min='0' max='10000000' value="{{ $Pregunta->perd_respuesta }}">
            @endif
            @break
            @case(4)
                @php $ErrorMessage = 'Ingrese un valor'; @endphp
                <input type='number' class='form-control{{ $Dependiente }} seccion{{$Pregunta->secc_id}}' required
                    id='campoPregunta{{ $Pregunta->preg_id }}' name='campoPregunta[{{$Pregunta->secc_id}}][{{ $Pregunta->preg_id }}]' min='0'
                    max='10000000' step='1' value="{{ $Pregunta->perd_respuesta }}">
            @break
            @case(5)
                @php $ErrorMessage = 'Ingrese un valor'; @endphp
                <input type='number' class='form-control {{ $Dependiente }} seccion{{$Pregunta->secc_id}}' required
                    id='campoPregunta{{ $Pregunta->preg_id }}' name='campoPregunta[{{$Pregunta->secc_id}}][{{ $Pregunta->preg_id }}]' min='0'
                    max='10000000' step=".01" value="{{ $Pregunta->perd_respuesta }}">
            @break
            @case(6)
            @if ( in_array($Pregunta->preg_id, [38, 80, 36]))
                @php $ErrorMessage = 'Seleccione una fecha valida';     @endphp
                <input type="date" class='form-control {{ $Dependiente }}' required
                id='campoPregunta{{ $Pregunta->preg_id }}' name='campoPregunta[{{$Pregunta->secc_id}}][{{ $Pregunta->preg_id }}]'
                value="{{ $hoy }}" readonly>
            @else
                @php $ErrorMessage = 'Seleccione una fecha valida'; $hoy = date("Y-m-d");  @endphp
                <input onchange="validarDate(this)" type="date" class='form-control {{ $Dependiente }}' required
                    id='campoPregunta{{ $Pregunta->preg_id}}' fecha_actual='{{$hoy}}' name='campoPregunta[{{$Pregunta->secc_id}}][{{ $Pregunta->preg_id }}]'
                    value="{{ $Pregunta->perd_respuesta }}">
            @endif


            @break
            @case(8)
                @php
                    $ErrorMessage = 'Ingrese un valor';
                    array_push($selecciones, $Pregunta->preg_id);
                @endphp
                @php $ErrorMessage = 'Complete este campo'; @endphp
                <textarea class='form-control {{ $Dependiente }} seccion{{$Pregunta->secc_id}}' id='campoPregunta{{ $Pregunta->preg_id }}'
                    name='campoPregunta[{{$Pregunta->secc_id}}][{{ $Pregunta->preg_id }}]' min='0' max='10000000' value=""> {{ $Pregunta->perd_respuesta }}</textarea>
            @break
            @case(9)
                @php $ErrorMessage = 'Adjunte un archivo';
                    if (Auth::user()->name != 'AGENCIA') {
                        $verCheckFile = 'hidden';
                    }else{
                        $verCheckFile = '';
                    }
    
                @endphp
                <div class="row align-items-start" {{$verPregFile}}>
                    <div id="divPreguntasFile" class="col custom-file p-0 mb-3">
                        <input type="file" class="form-control seccion{{$Pregunta->secc_id}}"  id='campoPregunta{{ $Pregunta->preg_id }}' required
                            name='campoPreguntaFile[{{$Pregunta->secc_id}}][{{ $Pregunta->preg_id }}]'
                            value="{{ $Pregunta->preg_id }}">
                    </div>
                    <div class="col-12 " {{$verCheckFile}}>
                        <label for="checkFile">Se entrego físicamente</label>
                        <input class="seccion{{$Pregunta->secc_id}}" id="campoPreguntaCheck{{ $Pregunta->preg_id }}" value="{{ $Pregunta->preg_id }}" onchange="checkFiles(this)" type="checkbox">
                    </div>
                </div>
            @break
            @case(10)
                @php
                    $ErrorMessage = 'Seleccione una opción';
                    array_push($selecciones, $Pregunta->preg_id);
                @endphp
                <select multiple class="selectpicker form-control {{ $Dependiente }}"
                    id='campoPregunta{{ $Pregunta->preg_id }}' name='campoPregunta[{{$Pregunta->secc_id}}][{{ $Pregunta->preg_id }}]' required>
                    <option disabled value="">Seleccionar</option>
                </select>
            @break

            @case(11)
            @php
                $ErrorMessage = 'Seleccione una opción';
                array_push($selecciones, $Pregunta->preg_id);
            @endphp
            @php $ErrorMessage = 'Complete este campo'; @endphp
            <input type="time" class='form-control {{ $Dependiente }}' id='campoPregunta{{ $Pregunta->preg_id }}'
                name='campoPregunta[{{$Pregunta->secc_id}}][{{ $Pregunta->preg_id }}]' min='0' max='10000000' value="">
        @break

        @case(12)
        @php
            $ErrorMessage = 'Seleccione una opción';
            array_push($selecciones, $Pregunta->preg_id);

            if (!isset($depaValor)) {
                $depaValor = '';
            }

            if (!isset($depaValorF)) {
                $depaValorF = '';
            }

            if (!isset($muniValorF)) {
                $muniValorF = '';
            }

        @endphp
        @if(in_array($Pregunta->preg_id, [181]))
            @php $ErrorMessage = 'Complete este campo'; @endphp
            <select class="form-control {{ $Dependiente }}" id='campoPregunta{{ $Pregunta->preg_id }}' name='campoPregunta[{{$Pregunta->secc_id}}][{{ $Pregunta->preg_id }}]' campoSeleccionado="{{$Pregunta->perd_respuesta}}" e>
               @foreach($departamento as $depa)
                    <option {{$depaValor == $depa->DEPA_NOMBRE ? 'selected' : ''}} value="{{$depa->DEPA_NOMBRE}}">{{$depa->DEPA_NOMBRE}}</option>
                @endforeach
            </select>
        @else

        @php $ErrorMessage = 'Complete este campo'; @endphp
        <select onchange="MakeRequestData('{{route('departamentos')}}'+'/'+ $(this).val(), '#getMuni', false, '', 'POST', 2, '')"  class="form-control {{ $Dependiente }}" id='campoPregunta{{ $Pregunta->preg_id }}' name='campoPregunta[{{$Pregunta->secc_id}}][{{ $Pregunta->preg_id }}]' campoSeleccionado="{{$Pregunta->perd_respuesta}}" e>
            @foreach($departamento as $depa)
                    <option {{$depaValorF == $depa->DEPA_NOMBRE ? 'selected' : ''}} value="{{$depa->DEPA_NOMBRE}}">{{$depa->DEPA_NOMBRE}}</option>
                @endforeach
        </select>

        @endif

        @break

            @case(13)
            <select id="getMuni" class="form-control {{ $Dependiente }}"  id='campoPregunta{{ $Pregunta->preg_id }}'
                name='campoPregunta[{{$Pregunta->secc_id}}][{{ $Pregunta->preg_id }}]'>
            @foreach($municipio as $depa)
                <option {{$muniValorF == $depa->muni_nombre ? 'selected' : ''}} value="{{$depa->muni_nombre}}">{{$depa->muni_nombre}}</option>
            @endforeach
            </select>
            @break

            @case(14)
                @php
                    $ErrorMessage = 'Seleccione una opción';
                    $selecciones[] = $Pregunta->preg_id;
                @endphp
            @if($Pregunta->preg_id == 247)
                <select onchange="disabledPregFile(this);" class="form-control {{ $Dependiente }}"  id='campoPregunta{{ $Pregunta->preg_id }}'
                    name='campoPregunta[{{$Pregunta->secc_id}}][{{ $Pregunta->preg_id }}]' campoSeleccionado="{{$Pregunta->perd_respuesta}}" >
                    <option value="{{ $Pregunta->opci_nombre}}" disabled {{$Pregunta->perd_respuesta == $Pregunta->opci_nombre ? 'selected' : ''}}>seleccion</option>
                </select>
            @else
                <select onchange="disabledPreg(this);" class="form-control {{ $Dependiente }}"  id='campoPregunta{{ $Pregunta->preg_id }}'
                    name='campoPregunta[{{$Pregunta->secc_id}}][{{ $Pregunta->preg_id }}]' campoSeleccionado="{{$Pregunta->perd_respuesta}}" {{$selectDisable == 'disabled' ? 'disabled' : ''}}>
                    <option value="{{ $Pregunta->opci_nombre}}" disabled {{$Pregunta->perd_respuesta == $Pregunta->opci_nombre ? 'selected' : ''}}>seleccion</option>
                </select>

            @endif
            @break

                
            @case(0)

            @php $ErrorMessage = 'Complete este campo'; @endphp
            <input type='email' class='form-control {{ $Dependiente }} {{ $Pregunta->vali_clase }} seccion{{$Pregunta->secc_id}}' id='campoPregunta{{ $Pregunta->preg_id }}' required
                name='campoPregunta[{{$Pregunta->secc_id}}][{{ $Pregunta->preg_id }}]'  value="{{ $Pregunta->perd_respuesta }}">
            @break

        @default
            @break
        @endswitch

        <div class="invalid-feedback small">
            {{ $ErrorMessage }}
        </div>

    </div>

    {!! $DivDepende !!}

    @if ($Pregunta->perd_respuesta && $Pregunta->tipe_id == 2)
        <script>GetHijas( '#campoPregunta{{ $Pregunta->preg_id }}', {{$Pregunta->perd_respuesta}}, 0)</script>
    @endif

    @php
        $FormActual = $Pregunta->secc_id;

        if(isset($Pregunta->perd_respuesta)){
            $Fill = true;
        }
    @endphp

@endforeach

<script>

    if(!$('.tabSection-' + '{{ $Tab - 1 }}').hasClass('disabled')){
        $('.tabSection-' + '{{ $Tab }}').removeClass('disabled');
        // $('#FormTab-' + '{{ $Tab - 2}}').addClass('enviado');
    }
</script>

<script>
  LoadOptions();


function LoadOptions(){

    let selected = 0;
    let opcioni  = 0;

    @foreach (OpcionesSelect($selecciones) as $Opcion)
        selected = $('#campoPregunta{{$Opcion->preg_id}}').attr('campoSeleccionado');
        opcioni = {{$Opcion->opci_id}};

        $("#campoPregunta{{ $Opcion->preg_id }}").append('<option '  +  ( selected == opcioni ? 'selected' : '' ) +  ' value="{{ $Opcion->opci_id }}">{{ $Opcion->opci_nombre }}</option>');
    @endforeach
}
</script>


<script>


var addValidate = function (elm) {

var classes = $(elm).attr('class');
var idItem = "#" + $(elm).attr('id');
// var placeholder = $(elm).attr('placeholder');

if (classes.search('dui') >= 0) {
    $(idItem).mask('00000000-0', { placeholder: '00000000-0' });
    bootstrapValidate(idItem, 'min:10: El formato es invalido');
}
if (classes.search('nit') >= 0) {
    $(idItem).mask('0000-000000-000-0', { placeholder: '0000-000000-000-0' });
    bootstrapValidate(idItem, 'min:17: El formato es invalido');
}
if (classes.search('nrc') >= 0) {
    $(idItem).mask('000000-0', { placeholder: '000000-0' });
    bootstrapValidate(idItem, 'min:8: El formato es invalido');
}
if (classes.search('telefono') >= 0) {
    $(idItem).mask('0000-0000', { placeholder: '0000-0000' });
    bootstrapValidate(idItem, 'min:9: El formato es invalido');
}
if (classes.search('celular') >= 0) {
    $(idItem).mask('0000-0000', { placeholder: '0000-0000' });
    bootstrapValidate(idItem, 'min:9: El formato es invalido');
}
if (classes.search('hour-minute') >= 0) {
    $(idItem).mask('00:00 ZZ', {
    placeholder: '00:00 AM',
    translation: {
        'Z': {
            pattern: /[AMamPMpm]/, optional: false
        }
    }
    });
}
}

// function isEmail(valor) {
//   if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3,4})+$/){
//     getElementById(valor.id)
//     alert("La dirección de email es correcta.");
//   } else {
//    alert("La dirección de email es incorrecta.");
//   }
// }


setTimeout(() => {
        var mainForm = document.getElementById('FormPermiso');
        var selectedForm = $(mainForm);
        selectedForm.find('input, select, textarea').each(function (index, elm) {
            if (elm.name.startsWith('campoPregunta')) {
                console.log(elm);
                addValidate(elm);
            }
    });
    }, 850);


    function disabledPregFile(valorFile){

//   if(valorFile.value == 1){
//       for (let index = 164; index <= 167; index++) {
//           $("#campoPregunta"+index).prop('disabled', true);
//       }
//       for (let index = 156; index <= 161; index++) {
//           $("#campoPregunta"+index).prop('disabled', true);
//       }
//         }else{
//       for (let index = 164; index <= 167; index++) {
//           $("#campoPregunta"+index).prop('disabled', false);
//       }
//       for (let index = 156; index <= 161; index++) {
//           $("#campoPregunta"+index).prop('disabled', false);
//       }
//   }
}


$(document).ready(function () {
   
   // $('#campoPregunta457').prop('disabled', true);
    
    $('#campoPregunta462').prop('disabled', true);
    $('#campoPregunta177').prop('disabled', true);

    $("#campoPreguntaCheck177").prop('disabled', true);
    $("#campoPreguntaCheck462").prop('disabled', true);
    $('#campoPregunta463').prop('disabled', true);
    $("#campoPreguntaCheck463").prop('disabled', true);
    $('#campoPregunta40').prop('disabled', true);
    $("#campoPreguntaCheck40").prop('disabled', true);


    $('.seccion' + 11).prop('disabled', true);
    $('.seccion' + 9).prop('disabled', true);
             for (let index = 156; index <= 160; index++) {
                $("#campoPregunta"+index).prop('disabled', true);
                $("#campoPreguntaCheck"+index).prop('disabled', true);

            }

            for (let index = 422; index <= 425; index++) {
                $("#campoPregunta"+index).prop('disabled', true);
                $("#campoPreguntaCheck"+index).prop('disabled', true);

            }

            for (let index = 164; index <= 167; index++) {
                $("#campoPregunta"+index).prop('disabled', true);
                $("#campoPreguntaCheck"+index).prop('disabled', true);

            }

            $("#campoPregunta240").prop('disabled', true);
            $("#campoPreguntaCheck240").prop('disabled', true);

            
        


valorSele = $("#campoPregunta246 option:selected" ).val();
switch (valorSele) {
            case '22':
            $('.seccion' + 11).prop('disabled', true);
            $('.seccion' + 9).prop('disabled', false);
            break;

            case '43':
            $('.seccion' + 11).prop('disabled', true);
            $('.seccion' + 9).prop('disabled', false);
            break;

            case '23':
            $('.seccion' + 9).prop('disabled', true);
            $('.seccion' + 11).prop('disabled', false);
            break;

            default:
            break;
        }

});


function validarTipoDocumento(val) { 
        if(val.value == 49){
            //$('#campoPregunta177').prop('disabled', false);
            //$('#campoPregunta457').prop('disabled', true);
        }else{
            //$('#campoPregunta457').prop('disabled', false);
            //$('#campoPregunta177').prop('disabled', true);

        }
 }

    function disabledPreg(valor){

        switch (valor.value) {
            //Urbanizacion
            case '22':
            $('.seccion' + 11).prop('disabled', true);
            $('.seccion' + 9).prop('disabled', false);

            for (let index = 156; index <= 160; index++) {
                $("#campoPregunta"+index).prop('disabled', false);
                $("#campoPreguntaCheck"+index).prop('disabled', false);
            }

            for (let index = 422; index <= 425; index++) {
                $("#campoPregunta"+index).prop('disabled', false);
                $("#campoPreguntaCheck"+index).prop('disabled', false);
            }

            for (let index = 164; index <= 167; index++) {
                $("#campoPregunta"+index).prop('disabled', true);
                $("#campoPreguntaCheck"+index).prop('disabled', true);

            }

            $("#campoPregunta240").prop('disabled', true);
            $("#campoPreguntaCheck240").prop('disabled', true);

            $('#campoPregunta462').prop('disabled', false);
            $("#campoPreguntaCheck462").prop('disabled', false);
            $('#campoPregunta177').prop('disabled', false);
            $("#campoPreguntaCheck177").prop('disabled', false);

            $('#campoPregunta424').prop('disabled', true);
            $("#campoPreguntaCheck424").prop('disabled', true);
            $('#campoPregunta463').prop('disabled', true);
            $("#campoPreguntaCheck463").prop('disabled', true);
            $('#campoPregunta40').prop('disabled', true);
            $("#campoPreguntaCheck40").prop('disabled', true);


            break;

            case '43':
            $('.seccion' + 11).prop('disabled', true);
            $('.seccion' + 9).prop('disabled', false);
            break;

            //Comunidad
            case '23':
            $('.seccion' + 9).prop('disabled', true);
            $('.seccion' + 11).prop('disabled', false);

            $('#campoPregunta240').prop('disabled', false);
            $("#campoPreguntaCheck240").prop('disabled', false);
            $('#campoPregunta429').prop('disabled', false);
            $("#campoPreguntaCheck429").prop('disabled', false);

            for (let index = 156; index <= 160; index++) {
                $("#campoPregunta"+index).prop('disabled', true);
                $("#campoPreguntaCheck"+index).prop('disabled', true);

            }

            for (let index = 422; index <= 425; index++) {
                $("#campoPregunta"+index).prop('disabled', true);
                $("#campoPreguntaCheck"+index).prop('disabled', true);

            }

            for (let index = 164; index <= 167; index++) {
                $("#campoPregunta"+index).prop('disabled', false);
                $("#campoPreguntaCheck"+index).prop('disabled', false);
            }

            $('#campoPregunta424').prop('disabled', false);
            $("#campoPreguntaCheck424").prop('disabled', false);
            $('#campoPregunta462').prop('disabled', true);
            $("#campoPreguntaCheck462").prop('disabled', true);
            $('#campoPregunta177').prop('disabled', true);
            $("#campoPreguntaCheck177").prop('disabled', true);
            $('#campoPregunta463').prop('disabled', false);
            $("#campoPreguntaCheck463").prop('disabled', false);
            $('#campoPregunta40').prop('disabled', false);
            $("#campoPreguntaCheck40").prop('disabled', false);
            

          


            break;

            default:
            break;
        }
    }


     function checkFiles(datoFile){
        var checkBox = document.getElementById(datoFile.id);
        if (checkBox.checked == true){
            document.getElementById('campoPregunta'+checkBox.value).disabled = true;
        }else{
            document.getElementById('campoPregunta'+checkBox.value).disabled =false;
        }
    }


    function validarDate(valor){

    fecha_actual = $(valor).attr('fecha_actual');
    fecha_suceso = valor.value;

            if(fecha_suceso < fecha_actual){
                // alert("La fecha tiene que ser mayor a la de hoy: "+ fecha_actual);
                // valor.value = fecha_actual;
            }
    }

    function isEmail(email) {
        var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        return regex.test(email);
    }

</script>
