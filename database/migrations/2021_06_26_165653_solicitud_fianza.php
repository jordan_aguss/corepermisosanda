<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\ClasesExternas\MetodosGenerales;

class SolicitudFianza extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    { 
        Schema::create(MetodosGenerales::$Esquema . 'solicitud_fianza', function (Blueprint $table) {
            $table->id('SOLF_ID')->comment('ID DE LA TABLA');
            $table->unsignedInteger('EVAL_ID')->comment('');
            $table->decimal('SOLF_MONTO')->comment('');
            $table->unsignedInteger('SOLF_PLAZO')->comment('');
            $table->unsignedInteger('CONV_ID')->comment('');
            $table->decimal('CONV_MONTO')->comment('');
            $table->unsignedInteger('CONV_PLAZO')->comment('');
            $table->string('SOLF_NOTAS')->comment('');
            $table->datetime('SOLF_FCREACION')->comment('');
            $table->unsignedInteger('SOLF_CAPROBADA')->comment('Correccion aprobada');
            $table->unsignedInteger('SOLF_APROBADA')->comment('');
            $table->datetime('SOLF_FAPROBADA')->comment('');
            $table->unsignedInteger('SOLF_CAPROBADO')->comment('Correccion aprobada');
            $table->datetime('SOLF_FCAPROBADO')->comment('fecha correccion aprobada');
            $table->unsignedInteger('SOLF_CELABORADO')->comment('Elaborado correcion');
            $table->datetime('SOLF_FCCREACION')->comment('fecha creacion correccion');
            $table->unsignedInteger('SOLF_ELABORADO')->comment('');
            $table->datetime('SOLF_FELABORADO')->comment('');
            $table->unsignedInteger('SOLF_SUPERADA')->comment('');
            $table->unsignedInteger('SOLF_SUPERACION')->comment('');
            $table->string('SOLF_ADJUNTO')->comment('');
            $table->unsignedInteger('ESTD_ID')->comment('');
            $table->datetime('SOLF_FESTADODOC')->comment('fecha estado documento');
            $table->datetime('CESTADODOC_ID')->comment('id estado documento correccion');
            $table->datetime('CESTADODOC_FECHA')->comment('fecha estado documento correccion');
            $table->unsignedInteger('SOLF_ACTIVO')->comment('');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(MetodosGenerales::$Esquema . 'solicitud_fianza');
    }
}
