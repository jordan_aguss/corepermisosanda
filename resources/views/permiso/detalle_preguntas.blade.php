@extends('templates.html_page')
@section('pretittle', 'Vista general')
@section('tittle', $Titulo)
@section("scripts")
<script src="{{ asset("js/index.js") }}" type="text/javascript"></script>
@endsection




<div class="container">

    <hr>

      <div>
        {{-- <a  type="button" target="_blank" href="{{route('permisos.acta', [$permiso->perm_id, $estado->estc_documento])}}" class="btn btn-success">{{$estado->esta_nombre}}</a> --}}

        @switch($idForm)
            @case(7)
                <a  type="button" target="_blank" href="{{route('permisos.acta', [$IdPermiso, 'SDF',$perf_id])}}" class="btn btn-success">Descargar Solicitud de Factibilidad</a>
            @break
            @case(8)
                <a  type="button" target="_blank" href="{{route('permisos.acta', [$IdPermiso, 'SRAP',$perf_id])}}" class="btn btn-success">Descargar Solicitud de Revisión y Aprobación de Planos</a>
            @break
            @case(9)
                <a  type="button" target="_blank" href="{{route('permisos.acta', [$IdPermiso, 'NIO',$perf_id])}}" class="btn btn-success">Descargar Notificación de Inicio de Obra</a>
            @break
            @case(10)
                <a  type="button" target="_blank" href="{{route('permisos.acta', [$IdPermiso, 'SRFC',$perf_id])}}" class="btn btn-success">Descargar Solicitud de Recepción Final de Campo</a>
            @break
            @case(11)
                <a  type="button" target="_blank" href="{{route('permisos.acta', [$IdPermiso, 'SHO',$perf_id ])}}" class="btn btn-success">Descargar Solicitud de Habilitación de Obra</a>
            @break
            @default
                
        @endswitch
      
      
    </div>

    <hr>

<div class="col-12 m-0 p-0" id="tabdoc">
    <div class="card shadow mb-3" >
        <div class="card-body">
            <div class="table-responsive">
                <table class="table card-table table-vcenter text-nowrap" id="data-table">
                    <thead>
                        <tr>
                            <th>PREGUNTA</th>
                            <th>RESPUESTA</th>
                        </tr>
                    </thead>
                    <tbody id="TBodyProyectos">
                        @if (count($Preguntas) == 0)
                            <tr><td colspan="6"><h4 class="text-muted text-center mt-3">No se encontraron registros</h4></td></tr>
                        @else
                            @foreach ($Preguntas as $item)
                            <tr>
                                    @foreach ($tipoPregunta as $key)
                                        @if($item->preg_nombre == $key->preg_nombre && $key->tipe_id == 9)
                                         @php($hidden ='none')
                                        <td>{{$key->preg_nombre}}</td>
                                        <td>
                                            <a href="{{route('descargarAdjunto', $item->RESULTADO)}}">{{$item->RESULTADO}}</a>
                                        </td> @break
                                        @else 
                                         @php($hidden = '')
                                        @endif
                                    @endforeach
                                <td style="display:{{$hidden}}">{{$item->preg_nombre}}</td>
                                <td style="display:{{$hidden}}">{{$item->RESULTADO}}</td> 
                            </tr>
                            @endforeach
                        @endif
                    </tbody>
                </table>
                <br>
            </div>
        </div>
    </div>
</div>

</div>


  
