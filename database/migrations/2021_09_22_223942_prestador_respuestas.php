<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class PrestadorRespuestas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    { 
        Schema::create('prestador_respuestas', function (Blueprint $table) {
            $table->id('PRER_ID')->comment('Id de la tabla');
            $table->unsignedBigInteger('PRES_ID')->comment('Id del prestador evaluado');
            $table->unsignedBigInteger('PROY_ID')->comment('Id del proyecto evaluado');
            $table->unsignedBigInteger('PREG_ID')->comment('Id de la pregunta');
            $table->string('TIPO_EVALUACION', 1)->comment('I = interno, P = publico');
            $table->unsignedBigInteger('OPCI_ID')->comment('Id de la opcion seleccionada');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('prestador_respuestas');
    }
}
