<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ConfirmaDocumento extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    { 
        Schema::create('confirma_documento', function (Blueprint $table) {
            $table->id('COND_ID')->comment('ID DE LA TABLA');
            $table->string('COND_NOMBRE')->comment('');
            $table->unsignedInteger('DOCU_ID')->comment('');
            $table->string('COND_TIPO')->comment('');
            $table->string('COND_DOCUMENTO')->comment('');
            $table->string('COND_IPADDRESS')->comment('');
            $table->string('COND_URLDOC')->comment('');
            $table->unsignedInteger('COND_COMFIRMLECTURA')->comment('Confirmacion de lectura');
            $table->string('COND_COORDENADAS')->comment('Coordenadas');
            $table->string('COND_CORREOENVIADO')->comment('Nombre de quien envio el correo');
            $table->datetime('COND_FLECTURA')->comment('fecha de lectura del documento');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('confirma_documento');
    }
}
