


<!DOCTYPE html>
<html lang="en">


<html>
    <head>
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css" integrity="sha384-zCbKRCUGaJDkqS1kPbPd7TveP5iyJE0EjAuZQTgFLD2ylzuqKfdKlfG/eSrtxUkn" crossorigin="anonymous">
    </head>
    
    
         <div class="card-body">

           <center><h5 > <p class="font-weight-bold"> SOLICITUD DE REVISIÓN Y APROBACIÓN DE PLANOS </p></h5 ></center>
            <center><img src="{{ storage_path() . '/app/public/images/logo_color.png' }}" width="45%" ></center>  <br> 
      

            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>
                            Fecha de Ingreso
                        </th>
                        <th>
                            Expendiente N.°
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td> <strong><p> {{$fecha_creacion}} </p></strong></td>
                        <td> <strong><p>  {{$perm_codigo}}</p></strong></td>
                    </tr>
                </tbody>
            </table>
    
     
           <center> <strong> <label for="">DATOS DEL INFORMANTE</label></strong></center>
 
            <table class="table table-bordered">
               <tbody>
                   <tr>
                       <td colspan="2">Nombres: {{isset($preguntas[459] )  ? $preguntas[459] : '' }}</td>
                       <td colspan="2" >Apellidos: {{isset($preguntas[248] )  ? $preguntas[248] : '' }}</td>
                   
                   </tr>
                   <tr>
                       <td colspan="4"  >Tipo de Persona:  {{isset($preguntas[249] )  ? $preguntas[249] : '' }}  </td>
                   </tr>
                   <tr>
                       <td colspan="2">NIT: {{isset($preguntas[252] )  ? $preguntas[252] : '' }}</td>
                       <td colspan="1" >Tipo de Documento:{{isset($preguntas[251] )  ? $preguntas[251] : '' }}</td>
                       <td colspan="1">DUI:  {{isset($preguntas[250] )  ? $preguntas[250] : '' }}</td>
                       

                   </tr>
                   <tr>
                       <td colspan="2">Nacionalidad:  {{isset($preguntas[253] )  ? $preguntas[253] : '' }}</td>
                       <td colspan="1">Domicilio:  {{isset($preguntas[254] )  ? $preguntas[254] : '' }}</td>
                       <td colspan="1">Departamento: {{isset($preguntas[255] )  ? $preguntas[255] : '' }}</td>
                   </tr>

                   <tr>
                       <td colspan="2">Correo Eléctronico: {{isset($preguntas[256] )  ? $preguntas[256] : '' }}</td>
                       <td colspan="2">Télefono: {{isset($preguntas[257] )  ? $preguntas[257] : '' }}</td>
                   </tr>

                   <tr>
                       <td colspan="4">Razón Social (Si Aplica):  {{isset($preguntas[397] )  ? $preguntas[397] : '' }}</td>
                   </tr>

                   <tr>
                       <td colspan="1">Inscrito al N° CNR:  {{isset($preguntas[258] )  ? $preguntas[258] : '' }}</td>
                       <td colspan="1">Libro:{{isset($preguntas[259] )  ? $preguntas[259] : '' }}</td>
                       <td colspan="1">Fecha: {{isset($preguntas[260] )  ? $preguntas[260] : '' }}</td>
                       <td colspan="1">NIT:{{isset($preguntas[261]) ? $preguntas[261] : '' }} </td>
                   </tr>

                   <tr>
                       <td colspan="2">Nombre del Proyecto:{{isset($preguntas[262] )  ? $preguntas[262] : '' }} </td>
                       <td colspan="2">Tipo de Proyecto:  {{isset($preguntas[263] )  ? $preguntas[263] : '' }}</td>
                   </tr>
                   <tr>
                       <td colspan="1">Propietario:{{isset($preguntas[264] )  ? $preguntas[264] : '' }}</td>
                       <td colspan="1">Ubicación: {{isset($preguntas[265] )  ? $preguntas[265] : '' }} </td>
                       <td colspan="1">Municipio: {{isset($preguntas[267] )  ? $preguntas[267] : '' }}</td>
                       <td colspan="1">Departamento: {{isset($preguntas[266] )  ? $preguntas[266] : '' }}</td>
                   </tr>
               </tbody>
            </table>

            <hr>


            <center> <strong> <label for="">DATOS COMPLEMENTARIOS (Sí aplica)</label></strong></center>

            <table class="table table-bordered"> 
                <tbody>
                    <tr>
                        <td colspan="4">Factibilidad Vigente: {{isset($preguntas[268] )  ? $preguntas[268] : '' }}</td>
                    </tr>
                    <tr>
                        <td>Nº {{isset($preguntas[269] )  ? $preguntas[269] : '' }}</td>
                        <td>Ref. Urb. {{isset($preguntas[271] )  ? $preguntas[271] : '' }}</td>
                        <td>Ref. Com. {{isset($preguntas[272] )  ? $preguntas[272] : '' }}</td>
                        <td>Fecha: {{isset($preguntas[270] )  ? $preguntas[270] : '' }}</td>
                    </tr>
                </tbody>
            </table>

            <hr>
            <center> <strong> <label for=""> DECLARACIÓN JURADA</label></strong></center>
            <p>
                El suscrito en calidad de titular, propietario o representante legal de la socieda dueña del 
                proyecto doy fe de la veracida de la información detallada en el presente documente y anexos,
                cumpliendo con los quisitos de ley exigidos, razón por la cual asumo la responsabilidad consencuente 
                derivada de esta declaración que tien calida de declaración jurada.
            </p>


            <hr>

            <center> <strong> <label for="">FIRMA DEL SOLICITANTE</label></strong></center>

            <table>
                <tbody>
                    <tr>
                        <td> <br>
                            __________________________________________________________________ <br>
                            Nombre y firma del solicitante (Propietario, Representante Legal o Apoderado, Profesional responsalbe)
                        </td>
                    </tr>
                </tbody>
            </table>
         
     </div>
 </html>
 
 