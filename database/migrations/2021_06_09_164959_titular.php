<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\ClasesExternas\MetodosGenerales;

class Titular extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    { 
        Schema::create(MetodosGenerales::$Esquema . 'titular', function (Blueprint $table) {
            $table->id('TITU_ID');
            $table->unsignedInteger('TIPE_ID');
            $table->string('TITU_NOMBRE',100);
            $table->string('TITU_NOMBRE_CORTO', 100);
            $table->string('TITU_NIT', 100)->nullable();
            $table->string('TITU_NITADJUNTO', 100)->nullable();
            $table->string('TITU_DUI', 15)->nullable();
            $table->string('TITU_ADJUNTO_DUI', 200)->nullable();
            $table->unsignedInteger('MUNI_ID')->nullable();
            $table->string('TITU_DIRECCION', 200)->nullable();
            $table->unsignedInteger('RELT_ID')->nullable();
            $table->unsignedInteger('TITE_ID')->nullable();
            $table->timestamp('FECHA_CREACION')->nullable();
            $table->bigInteger('USUA_CREACION')->comment('Id del usuario que creo el titular')->nullable();
            $table->timestamp('TITU_FECHA_MODIFICO')->nullable();
            $table->bigInteger('USUA_MODIFICO')->comment('Id del usuario que modifico el titular')->nullable();
            $table->string('TITU_RECHAZO')->comment('Motivo del rechazo del titular')->nullable();
            $table->timestamp('FECHA_ASIGNACION')->comment('Fecha de asignacion')->nullable();
            $table->bigInteger('USUA_ASIGNACION')->comment('Id del usuario que fue asignado para su revision')->nullable();
            $table->timestamp('FECHA_RESPUESTA')->nullable();
            $table->string('USUA_EXTRANJERA')->nullable();
            $table->string('TITU_PASAPORTE', 20)->nullable();
            $table->string('TITU_ADJUNTO_PASAPORTE', 200)->nullable();
            $table->string('TITU_ADJUNTO_ESCRITURA', 200)->nullable();
            $table->integer('TITU_MODIFICACION')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('titular');
    }
}
