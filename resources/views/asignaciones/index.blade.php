

<div class="col-11-m-auto card">
    <div class="card-header">
        <h5 class="card-tittle">Asignación de información de formularios</h5>
    </div>

    <div class="card-body">
        <div class="row">
            <div class="col-xl-4 col-md-6 col-sm-12">
                <div class="form-group">
                    <label><strong>Tipo de asignación</strong></label>
                    <select name="" class="form-control">
                        <option value="" selected>Preguntas a forms</option>
                        <option value="">Preguntas a secciones</option>
                        <option value="">Preguntas a preguntas</option>
                    </select>
                </div>
            </div>

            <div class="col-12"></div>

            <div class="col-xl-5 col-md-4 col-sm-12 p-2">

                <div class="card shadow-none">
                    <div class="card-header">
                        <h5 class="card-tittle">Preguntas disponibles</h5>
                    </div>

                    <div class="card-body bg-light">
                        <div id="InfoFree">
                            @foreach ($Forms as $item)
                                <div class="card shadow-none p-2 mb-2 mt-2">
                                    <div class=" d-flex justify-content-between align-items-center"
                                    idPregunta = '{{$item->preg_id}}'>

                                        <span>{{$item->preg_nombre}}</span>
                                        <i class="fas fa-chevron-right"></i>
                                    </div>
                                </div>
                            @endforeach
                            
                        </div>
                    </div>

                </div>

            </div>

            <div class="col-xl-2 col-md-4 col-sm-11 m-auto">

            </div>

            <div class="col-xl-5 col-md-4 col-sm-12 p-2">

                <div class="card shadow-none">
                    <div class="card-header">
                        <h5 class="card-tittle">Preguntas agregadas</h5>
                    </div>

                    <div class="card-body bg-light" style="height: 300px">
                        <div id="InfoAdd">
                            <div class="card shadow-none p-2 mb-2 mt-2">
                                <div class=" d-flex justify-content-between align-items-center">

                                    <span>Prueba</span>
                                    <i class="fas fa-chevron-right"></i>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

            </div>

        </div>
        
    </div>

</div>