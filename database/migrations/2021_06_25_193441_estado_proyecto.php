<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\ClasesExternas\MetodosGenerales;

class EstadoProyecto extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    { 
        Schema::create(MetodosGenerales::$Esquema . 'estado_proyecto', function (Blueprint $table) {
            $table->id('ESTP_ID')->comment('Id del estado');
            $table->string('ESTP_NOMBRE')->comment('Nombre del estado');
            $table->unsignedInteger('ESTP_ACTIVO')->comment('Estado de la tabla');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(MetodosGenerales::$Esquema . 'estado_proyecto');
    }
}
