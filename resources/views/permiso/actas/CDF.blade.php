<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <div style="width:600px; margin:0 auto;">
        <div style="text-align: right;">
            <p >San Salvador,</p>
            <p style=" text-align: right;">Proyecto:<strong>{{isset($preguntas[419] )  ? $preguntas[419] : '' }}</strong><br />
                Certificado de factibilidad:
                {{ isset($preguntas[426] ) ? $preguntas[426] : '' }}<br />Referencia:
                
                {{isset($preguntas[427] )  ? $preguntas[427] : '' }} <br /><br />
            </p>
        </div>

        <p style="text-align: left;"><br /><strong>{{isset($preguntas[174] )  ? $preguntas[174] : '' }}  {{isset($preguntas[484] )  ? $preguntas[484] : '' }}<br /></strong>Presente.
        </p>
        <p style="text-align: left;">&nbsp;</p>
        <p style="text-align: Justify;">De acuerdo al Acta No:
            <strong>{{ isset($preguntas[428] )  ? $preguntas[428] : '' }}</strong>
            del comite de factibilidades de esta fecha, celebrado en las oficinas centrales, se acordo autorizar la
            factibilidad de conexión de servicio de aguas negras hacia la red de ANDA, para un terreno propiedad
            de el/la Sr/Sra<strong>{{isset($preguntas[193] )  ? $preguntas[193] : '' }}</strong>ubicado en<strong>{{isset($preguntas[194] )  ? $preguntas[194] : '' }} </strong>Municipio de<strong>{{isset($preguntas[195] )  ? $preguntas[195] : '' }}</strong>
            Departamento de<strong>{{isset($preguntas[196] )  ? $preguntas[196] : '' }}</strong>, en el cual se proyecta la construcción de
            <strong>{{isset($preguntas[419] )  ? $preguntas[419] : '' }}</strong>, de conformidad
            con el planteamiento siguiente:</p>
        <p style="text-align: left;">&nbsp;</p>
        <table style="border-collapse: collapse; width: 100%; height: 154px;" border="0">
            <tbody>
            <tr>
                        <td>Área Total En M2: {{isset($preguntas[206] )  ? $preguntas[206] : '' }}</td>
                    </tr>
                    <tr>
                        <td>Área útil M2: {{isset($preguntas[207] )  ? $preguntas[207] : '' }}</td>
                    </tr>
                    <tr>
                      <td>Número de lotes: {{isset($preguntas[209] )  ? $preguntas[209] : '' }}</td>
                    </tr>
                    <tr>
                      <td>Número de apartamentos: {{isset($preguntas[210] )  ? $preguntas[210] : '' }}</td>
                    </tr>
                    <tr>
                      <td>Número de Viviendas: {{isset($preguntas[209] )  ? $preguntas[209] : '' }}</td>
                    </tr>
                    <tr>
                      <td>Número de locales: {{isset($preguntas[212] )  ? $preguntas[212] : '' }}</td>
                    </tr>
                    <tr>
                        <td>Área Promedio de Lotes en M2: {{isset($preguntas[215] )  ? $preguntas[215] : '' }}</td>
                    </tr>
                    <tr>
                        <td>Área Promedio de Apartamentos En M2: {{isset($preguntas[216] )  ? $preguntas[216] : '' }}</td>
                    </tr>
                    <tr>
                        <td>Área Promedio de Viviendas En M2: {{isset($preguntas[218] )  ? $preguntas[218] : '' }}</td>
                    </tr>
                    <tr>
                        <td>Área Promedio de Locales En M2:{{isset($preguntas[217] )  ? $preguntas[217] : '' }}</td>
                    </tr>
                    <tr>
                        <td>Área de Construcción Total En M2:{{isset($preguntas[220] )  ? $preguntas[220] : '' }}</td>
                    </tr>
                    <tr>
                        <td>Área de Construcción en RestaurantesEn M2: {{isset($preguntas[221] )  ? $preguntas[221] : '' }}</td>
                    </tr>
                    <tr>
                        <td style="width: 22.8333%; height: 18px; text-align: right;">Área de Construcción en Centro Comerciales M2:  {{isset($preguntas[222] )  ? $preguntas[222] : '' }}</td>
                    </tr>
                    <tr>
                        <td>Área de Construcción en Mercados M2:  {{isset($preguntas[223] )  ? $preguntas[223] : '' }}</td>
                    </tr>
                    <tr>
                        <td>Área de Construcción en Edificios para Oficina: {{isset($preguntas[224] )  ? $preguntas[224] : '' }}</td>
                    </tr>
                    <tr>
                        <td>Número de Alumnos en Escuela: {{isset($preguntas[225] )  ? $preguntas[225] : '' }}</td>
                    </tr>
                  
                    <tr>
                        <td>Número de Turnos: {{isset($preguntas[227] )  ? $preguntas[227] : '' }}</td>
                    </tr>
                    <tr>
                        <td>Número de Bombas en Gasolinera: {{isset($preguntas[228] )  ? $preguntas[228] : '' }}</td>
                    </tr>
                    <tr>
                        <td>Número de Camas en Hospitales: {{isset($preguntas[229] )  ? $preguntas[229] : '' }}</td>
                    </tr>
                    <tr>
                        <td>Número de niveles en Edificios: {{isset($preguntas[230] )  ? $preguntas[230] : '' }}</td>
                    </tr>
                    <tr>
                        <td>Número de Sótanos para Estacionamiento: {{isset($preguntas[231] )  ? $preguntas[231] : '' }}</td>
                    </tr>
                    <tr>
                        <td>Cantidad de Edificios: {{isset($preguntas[232] )  ? $preguntas[232] : '' }}</td>
                    </tr>
                    <tr>
                        <td>Número de Clínicas Médicas: {{isset($preguntas[233] )  ? $preguntas[233] : '' }}</td>
                    </tr>
                    <tr>
                        <td>Número de Clínicas Dentales:{{isset($preguntas[234] )  ? $preguntas[234] : '' }}</td>
                    </tr>
                    <tr>
                        <td>Número de Habitaciones en Hoteles/Pensión:{{isset($preguntas[235] )  ? $preguntas[235] : '' }}</td>
                    </tr>
                    <tr>
                        <td>Área de Estacionamiento: {{isset($preguntas[236] )  ? $preguntas[236] : '' }}</td>
                    </tr>
                    <tr>
                        <td>Área de Jardines:  {{isset($preguntas[237] )  ? $preguntas[237] : '' }}</td>
                    </tr>
                    <tr>
                        <td>Capacidad Cines, Teatros (número de personas):{{isset($preguntas[238] )  ? $preguntas[238] : '' }}</td>
                    </tr>
                    <tr>
                        <td>Monto o Valor de Inversion del Proyecto:{{isset($preguntas[487] )  ? $preguntas[487] : '' }}
                    </tr>


            </tr>
            </tbody>
            </table>
        <p style="text-align: Justify;">Esta dirección, previo al analisis del estudio preparado por elárea
            de factibilidades y de acuerdo a la opinión emitida por la Gerencia Regional,
            resuelve:<br /><br />1. Acueducto:
            {{ isset($preguntas[458] )  ? $preguntas[458] : '' }}</p>
            <p style="text-align: Justify;">2. Alcantarillado:
            {{ isset($preguntas[429] )  ? $preguntas[429] : '' }}</p>
            <p style="text-align: Justify;">3. Pagos:
            {{ isset($preguntas[473] )  ? $preguntas[473] : '' }}</p>
            <p style="text-align: Justify;">3.1 Por tramite:
            {{ isset($preguntas[474] )  ? $preguntas[474] : '' }}</p>
            <p style="text-align: Justify;">3.2 Por Prueba Hidrostatica:
            {{ isset($preguntas[475] )  ? $preguntas[474] : '' }}</p>
            <p style="text-align: Justify;">3.3 Por Area de Construcción:
            {{ isset($preguntas[476] )  ? $preguntas[476] : '' }}</p>
            <p style="text-align: Justify;">3.4 Por Entronque:
            {{ isset($preguntas[477] )  ? $preguntas[477] : '' }}</p>
            <p style="text-align: Justify;">4 Habilitacion:
            {{ isset($preguntas[478] )  ? $preguntas[478] : '' }}</p>
            <p style="text-align: Justify;">5 Alcance:
            {{ isset($preguntas[729] )  ? $preguntas[729] : '' }}</p>
            <p style="text-align: Justify;">Nota:
            {{ isset($preguntas[482] )  ? $preguntas[482] : '' }}</p>
            <p style="text-align: Justify;">Importante:
            {{ isset($preguntas[481] )  ? $preguntas[481] : '' }}</p>
            

            <center> <strong> <label for=""> ______________________________________________</label></strong></center>
            <center> <strong> <label for=""> GERENTE TECNICO</label></strong></center>


    </div>
</body>

</html>