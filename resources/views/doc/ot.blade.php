@include('templates.html_page')


<html>
   <div class="container">
     <div class="card">

   
        <div class="card-header"> 
            <div class="row justify-content-start">
                <div class="col-4">
                    <img class="img-fluid" width="200px" height="200px" src="https://www.bitrefill.com/content/cn/b_rgb%3A0a1b3f%2Cc_pad%2Ch_600%2Cw_1200/v1636606497/anda-test.jpg" 
                    alt="" draggable="false"> 
                </div>
                <div class="col-8">
                     <p class="text-primary"><h1>DEPARTAMENTO DE OPERACIONES GERENCIA REGIÓN REGIONAL</h1></p>
                     <p><h5>OPINIÓN TÉCNICA DE LOS SISTEMAS DE ACUEDUCTO Y ALCANTARILLADO SANITARIO</h5></p>
                </div>
            </div>
            
        </div>

        <div class="card-body">

           <strong><p> Ref.</p></strong>
           <strong> <p>Fecha de ingreso</p></strong>
           <strong> <p>Fecha de inspección</p></strong>
           <strong><p>Fecha de Emisión de la Opinión Técnica</p></strong>
           <strong> <p>Fecha de Aprobación en la Región </p></strong> 
           
           <br>

            <p>Nombre </p>
            <p>Nombre del Solicitante</p>
            <p>Nombre del propietario</p>
            <p>Ubicación</p>

            <br>

            <p>Cantidad de servicios solicitados</p>

            <br>

            <b><p>ACUEDUCTO</p></b> 
            <p>El Inmueble solicitante se situán en la zona de influencia de la EB Atzumpa Concención de Ataco, 
                la cual ya NO posee la capacidad para abastecer nuevos servicios, el problable punto de entorque se ubica 
                a 25 Mts Aprox. de una Línea de Impelencia, conforme al Artículo 22 Letra E de la Normativa de Factibilidades
                de Acueductos y alcantarillados NO es posible otorgar dicha factibilidad ya que se generaria una pérdida
                de carga al gradiente Hidráulico.</p>

            <p>Una nueva solicitud podrá ser evaluada cuando ANDA realice mejoras en el sector de abastecimiento del 
                inmueble.
            </p>

            <p> Presión en el punto de entronque: </p>
            <p>Distancia de la red de distribución más cercana </p>
            <p>Presión en distintos puntos de la red:</p>

            <ul>
                <li>Punto 1</li>
                <li>Punto 2</li>
                <li>Punto n..</li>
            </ul>

            <p>Horarios de servicios en la zona:</p>
            <p>Identificar áreas critícas en la zona: </p>
            <p>Caudal de abastecimiento en la zona:</p>
            <p>Fuente de abastecimiento en la zona: </p>
            <p>Caudal medio solicitado:</p>
            <p>Caudal disponible para nuevos usuarios:</p>
            <p>Número de Usuarios Actuales (conexiones):</p>
            <p>Número de Usuarios futuros (conexiones):</p>
            <p>Tipo de servicio existente: </p>
            <p>Estado Actual del Sistema y antecedentes del sistema:</p>


            <p>NOTA: Los planos que se presentan a revisión y aprobación deberían de tener dimensiones máximas
                de 1.50 x 0.90 mts., en el extremo inferior derecho de todos los planos que se presentan a revisión,
                se dejará un espacio de 15 x 25 cms., para la colocación de los sellos de revisión y aprobación de esta
                Administración.
            </p>

            <p>
                IMPORTANTE: <br>
                - Esta factibilidad no constituye autorización para construir sistemas de acueductos y alcantarillados 
            </p>

            <p>La unidad de Factibilidades deberá velar porque se cumpla este requisito </p>

            <p><b>ESTE CERTIFICADO DE FACTIBILIDAD ESTABLECE UNICAMENTE LA CAPACIDAD DE ANDA PARA PRESTAR EL SERVICIO
                SOLICITADO Y NO CONSTITUYE AUTORIZACIÓN PARA CONSTRUIR OBRAS DE ACUEDUCTO NI ALCANTARILLADO SANITARIO
                EN EL TERRENO, DEBIENDO QUEDAR SIN EFECTO EN CASO QUE LA RESPECTIVAS AUTORIZACIONES DE OTRAS
                INSTITUCIONES COMPETENTES FUESEN DENEGADAS</b></p>

            <p>Es pertinente informar que la ANDA cumplirá con lo establecido en el Art. 31,32 y 33 de la Ley 
                de Acceso a la Información Pública, en el que se constituye el derecho a la protección de datos
                personales, vinculado con lo establecido con el Art. 24 literales b, c y d de la ley en referencia.
                La información circunscrita en el presente certificado únicamente podrá ser divulgada a terceros mediante
                el consentimiento expreso y libre del titular de la misma.
            </p>

            <p>Atentamente,</p>
            <br><br>



            <center><p><b>ING. JOEL THOMAS D. BOEKLE <br> DIRECTOR DE INGENIERIA Y PROYECTOS</b></p></center>
        </div>
    </div>
   </div>
</html>

