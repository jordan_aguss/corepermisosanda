@extends('templates.html_page')

@section('Data')

<form class="col-10 m-auto my-4 card" id="FormUser" novalidate>
    <div class="card-header">
        <h5 class="card-title">Nuevo usuario</h5>
    </div>

    <div class="card-body">
        @csrf
        <div class="row">
            <div class="form-group col-xl-6 col-md-6 col-sm-12 mb-3">
                <label for="" class="small text-black">Correo</label>
                <input type="email" name="correo" id="correo" class="form-control" required>
            </div>

            <div class="form-group col-xl-6 col-md-6 col-sm-12 mb-3">
                <label for="" class="small text-black">Contraseña</label>
                <input type="password" name="pass" id="pass" class="form-control" required>
            </div>

            <div class="form-group col-xl-6 col-md-6 col-sm-12 mb-3">
                <label for="" class="small text-black">Confirmar contraseña</label>
                <input type="password" name="rpass" id="rpass" class="form-control" required>
            </div>

            <div class="form-group col-xl-6 col-md-6 col-sm-12 mb-3">
                <label for="" class="small text-black">Nombre</label>
                <input type="text" name="nombre" id="nombre" class="form-control" required>
            </div>

            @include('preguntas.form-preguntas', ['hijas' => true])


        </div>
    </div>

    <div class="card-footer">
        <div class="text-end">
            <button class="btn bg-green text-white" type="button"
            onclick="MakeRequestData('{{route('nuevo-usuario')}}', '', true, '', 'POST', 4, '#FormUser', true)">
                <i class="fas fa-save text-white"></i> &nbsp; Guardar
            </button>
        </div>
    </div>

</form>

@endsection
