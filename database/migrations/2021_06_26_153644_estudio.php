<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\ClasesExternas\MetodosGenerales;

class Estudio extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    { 
        Schema::create(MetodosGenerales::$Esquema . 'estudio', function (Blueprint $table) {
            $table->id('ESTU_ID')->comment('ID DE LA TABLA');
            $table->string('ESTU_NOMBRE')->comment('NOMBRE DEL ESTUDIO');
            $table->unsignedInteger('ESTU_ACTIVO')->comment('ESTADO');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(MetodosGenerales::$Esquema . 'estudio');
    }
}
