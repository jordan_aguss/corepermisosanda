<div class="">
    <div class="card-body shadow-lg bg-white mt-3 ml-4 mr-4">
        <form class="col-xl-10 col-sm-12 col-md-12 col- offset-xl-1 offset-md-0 offset-sm-0" id="FormProyecto" novalidate>
            @csrf
            @include('permiso.form_proyecto')
            <div class="col-12 mb-4 mt-2">
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" id="invalidCheck" required value="">
                    <label class="form-check-label text-justify small" for="invalidCheck">
                        Doy fe de la veracidad de la información detallada en el presente formulario, cumpliendo con los requisitos de ley exigidos,razón por la cual asumo
                        la responsabilidad administrativa, civil o penal, incluida la posibilidad multa o pena privativa de libertad, que se deriven de la falsedad de este
                        instrumento, el cual tiene calidad de declaración jurada. Además de aceptar recibir las notificaciones oficiales via correo electronico.
                    </label>
                </div>
            </div>
            <div class="col-12 mb-4">
                <button type="button" class="btn colorSea text-white col-12 p-2"
                onclick="VerificarProyecto('{{route('segimineto.index')}}','#FormProyecto')">
                Crear proyecto</button>
            </div>
        </form>
    </div>
</div>
<script>
    @foreach($Forms as $item)
        @if($item->form_id == $Permiso->form_id)
            $('#SelectForm').append('<option value="{{$item->form_id}}" selected>{{$item->form_nombre}}</option>');
            MakeRequestData('{{route('preguntas-form', [$item->form_id, 0])}}', '#divPreguntas' );
        @else
            $('#SelectForm').append('<option value="{{$item->form_id}}" >{{$item->form_nombre}}</option>');
        @endif
    @endforeach

    setUrls("", "{{route('get-preguntas-hijas')}}", "{{route('prem-save-preguntas')}}", "{{route('prem-save-pregunta')}}");
</script>
