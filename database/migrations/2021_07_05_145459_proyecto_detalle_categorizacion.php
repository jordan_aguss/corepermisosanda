<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ProyectoDetalleCategorizacion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    { 
        Schema::create('proyecto_detalle_categorizacion', function (Blueprint $table) {
            $table->id('PRDC_ID')->comment('Id de la tabla');
            $table->unsignedInteger('PROY_ID')->comment('id del proyecto');
            $table->unsignedInteger('PREG_ID')->comment('id de la pregunta');
            $table->unsignedInteger('SECT_ID')->comment('id del sector');
            $table->string('PRDC_VALOR')->comment('Respuesta de la pregunta');
            $table->unsignedInteger('CATE_ID')->comment('id de la categorizacion');
            $table->unsignedInteger('PRDC_PONDERACION')->comment('ponderacion de la pregunta');
            $table->unsignedInteger('PRDC_VALORINI')->nullable()->comment('valor inicial?');
            $table->unsignedInteger('PRDC_VALORFIN')->nullable()->comment('valor final?');
            $table->unsignedInteger('CATEGORIZO')->comment('');
            $table->unsignedInteger('PREG_CATEGORIZA')->comment('');
            $table->unsignedInteger('PROD_ID')->comment('Id del proyecto detalle');
            $table->unsignedInteger('RECA_ID')->comment('Id referencia capas aplicadas');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('proyecto_detalle_categorizacion');
    }
}
