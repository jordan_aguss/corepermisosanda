<!DOCTYPE html>
<html>

<head>

  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css" integrity="sha384-zCbKRCUGaJDkqS1kPbPd7TveP5iyJE0EjAuZQTgFLD2ylzuqKfdKlfG/eSrtxUkn" crossorigin="anonymous">
</head>

<body>
  <div>

  </div>
  
    <center><h1>Solicitud de Factibilidad - @foreach ($permisoDetalle as $name) {{$name->perm_codigo}} @php break; @endphp  @endforeach</h1></center>
    <center><h2>Nombre del Solicitante: {{Auth::user()->name}} </h2></center>
    <table class="table table-hover">
        <thead>
          <tr>
            <th scope="col">ID Permiso</th>
            <th scope="col">Pregunta</th>
            <th scope="col">Respuesta</th> 
          </tr>
        </thead> @php($cont = 0)
        @foreach ($permisoDetalle as $item)
        @if($item->form_id == 2)
        @if($cont <= 39)
          @php($cont++)
       @else
        @break
       @endif
    @else
        @if($cont <= 17)
        @php($cont++)
    @else
      @break
 @endif

    @endif
        <tbody>
          <tr>
            <th>{{$item->perm_id}}</th>
          @foreach ($preguntaString as $key)
            @if($key->preg_id == $item->preg_id)
             <td>{{$key->preg_nombre}}</td>
            @endif
          @endforeach

            @if(in_array($item->preg_id, [156, 157, 158, 159, 160, 161]) && $item->perd_respuesta)
                <td>
                  <a href="{{route('descargarAdjunto', $item->perd_respuesta)}}">Ver Adjunto</a>
                </td>
            @else
            <td>{{$item->perd_respuesta}} </td>
            @endif

            
          </tr>
        </tbody>
        @endforeach
      </table>

</body>

</html>