<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TablaAnalisisParaComparar extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    { 
        Schema::create('tabla_analisis_para_comparar', function (Blueprint $table) {
            $table->id('TABA_ID')->comment('Id de la tabla');
            $table->string('LAYER_TYPE', 100)->comment('Los datos de esta tabla se llenan de los obtenido en el vigea');
            $table->unsignedInteger('ID')->comment('');
            $table->string('QUESTION_TEXT')->comment('');
            $table->string('ANSWER', 5)->comment('');
            $table->string('LAYER_NAME')->comment('');
            $table->unsignedInteger('DIST_VAL')->comment('');
            $table->string('DIST_UNITS',5)->comment('');
            $table->string('ATT_FIELD',100)->comment('');
            $table->string('PHYSICAL_TYPE',10)->comment('');
            $table->string('MXD_LAYER_NAME')->comment('');
            $table->unsignedInteger('UNIQUE_ID')->comment('');
            $table->unsignedInteger('ID_TMP')->comment('');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tabla_analisis_para_comparar');
    }
}
