<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\ClasesExternas\MetodosGenerales;

class EstadoGeneral extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    { 
        Schema::create(MetodosGenerales::$Esquema . 'estado_general', function (Blueprint $table) {
            $table->id('ESTG_ID')->comment('ID DE LA TABLA');
            $table->string('ESTG_NOMBRE')->comment('NOMBRE DEL ESTADO');
            $table->string('ESTG_DESCRIPCION')->comment('DESCRIPCION DEL ESTADO');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(MetodosGenerales::$Esquema . 'estado_general');
    }
}
