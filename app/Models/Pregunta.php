<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pregunta extends Model
{
    use HasFactory;
    protected $table = "preguntas"; 
    protected $primaryKey = 'preg_id';
    public $timestamps = false;
}
