<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PermisoFormulario extends Model
{
    use HasFactory;
    protected $table = "permisos_formulario";
    protected $primaryKey = 'perf_id';
    public $timestamps = false;
}
