<?php

use App\Models\PermisoFormulario;
use App\Models\PreguntaOpcion;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Auth;

if(!function_exists('isAdmin')){
    function isAdmin($url){
        //return in_array($url, Session::get('urls'));
        return Session::get('perfil_usuario') == 'Administrador';
    }
}

if(!function_exists('Prueba')){
    function Prueba(){
        return 'clase';
    }
}

if(!function_exists('OpcionesSelect')){
    function OpcionesSelect($selecciones){

        if (count($selecciones) > 0) {
            return PreguntaOpcion::whereIn('preg_id', $selecciones)
                ->join('opciones as o', 'o.opci_id', 'pregunta_opcion.opci_id')->get();
        }

        return [];
    }
}
if(!function_exists('getForms')){
    function getForms($permiso){

        return PermisoFormulario::where('perm_id', $permiso)->where('usua_id', Auth::id())
        ->join('formularios as f', 'f.form_id', 'permisos_formulario.form_id')->get();
    }
}
