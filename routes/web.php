<?php

use App\Http\Controllers\HomeController;
use App\Http\Controllers\AndaController;
use App\Http\Controllers\PermisoController;
use App\Http\Controllers\PreguntaController;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\ProyectoController;
use App\Http\Controllers\UsuarioController;


use App\Models\User;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;



Route::get('/registro', [UsuarioController::class, 'index'])->name('users.index');
Route::post('/nuevo-usuario', [UsuarioController::class, 'SaveUser'])->name('nuevo-usuario');

Auth::routes();


Route::get('/descargarAdjuntoPublico/{ruta?}', [PreguntaController::class, 'descargarAdjuntoPublico'])->name('descargarAdjuntoPublico');

Route::group(['middleware' => ['auth']], function(){

   Route::get('/', [HomeController::class, 'Home'])->name('home-home');

    Route::post('/guardar-estado-preguntas', [PermisoController::class, 'SavePreguntas'])->name('prem-save-preguntas');
    Route::post('/guardar-estado-pregunta', [PermisoController::class, 'SavePregunta'])->name('prem-save-pregunta');

    Route::get('/LoadPage/{Vista}', [HomeController::class, 'GetVista'])->name('Home.LPage');

    Route::get('/get-preguntas-form/{idForm?}/{nuevo?}', [PreguntaController::class, 'GetPreguntasForm'])->name('preguntas-form');
    Route::get('/get-preguntas-hijas/{pregunta?}/{opcion?}/{seccion?}/{loading?}', [PreguntaController::class, 'GetPreguntasHijas'])->name('get-preguntas-hijas');
    Route::post('/guardarAjunto', [PreguntaController::class, 'guardarAjunto'])->name('guardarAdjunto');
    Route::get('/descargarAdjunto/{ruta?}', [PreguntaController::class, 'descargarAdjunto'])->name('descargarAdjunto');
    Route::post('/departamentos/{id?}', [PreguntaController::class, 'departamentos'])->name('departamentos');

    Route::get('detalle/preguntas/{IdPermiso}/{idForm?}/{perf_id?}',[PermisoController::class, 'detallePreguntas'])->name('permisos.detalle_preguntas');
    Route::get('generar-acta/{id}/{tipo?}/{perfID?}',[PermisoController::class, 'GenerarActa'])->name('permisos.acta');


    //Route::get('/vista-formulario', [PermisoController::class, 'FormPermiso'])->name('Proyecto.Form');
    Route::get('/vista-formulario/{id?}/{permisoId?}', [PermisoController::class, 'FormPermiso'])->name('Proyecto.Form');



    Route::get('/ProyectoView', [ProyectoController::class, 'ViewProyectos'])->name('Proyecto.View');
    Route::get('/GetPreguntas/{IdSector?}', [ProyectoController::class, 'GetPreguntasSector'])->name('Proyecto.GetPreguntas');
    Route::get('/Seguimiento', [ProyectoController::class, 'Seguimiento'])->name('segimineto.index');
    Route::post('/guardar-permiso', [PermisoController::class, 'GuardarPermiso'])->name('guardar-permiso');
    Route::get('/exportarPDF/{idPermiso?}', [PermisoController::class, 'exportarPDF'])->name('exportarPDF');
    Route::get('/verPermiso/{idPermiso?}', [PermisoController::class, 'verPermiso'])->name('verPermiso');
    Route::get('buscar/{palabra?}',[PermisoController::class, 'Buscar'])->name('buscar-permisos');
    Route::get('ordenar/{palabra?}',[PermisoController::class, 'Ordenar'])->name('ordenar-permisos');


    Route::get('/notifcaciones', [HomeController::class, 'Mensajes'])->name('notificaciones');
    Route::get('/confirmar-lectura/{mensaje}', [HomeController::class, 'ConfirmarLectura'])->name('confirmar-lectura');

    Route::get('aprobarPermiso/{id?}',[PermisoController::class, 'aprobarPermiso'])->name('aprobarPermiso');
    Route::get('rechazarPermiso/{id?}',[PermisoController::class, 'rechazarPermiso'])->name('rechazarPermiso');
    Route::get('/verPermisoComunidad/{idPermiso?}', [PermisoController::class, 'verPermisoComunidad'])->name('verPermisoComunidad');

    Route::get('/actualizarPermisos/{perm_id}/{form_id?}/{tipo?}', [PreguntaController::class, 'actualizarPermisos'])->name('actualizarPermisos');
    Route::post('/guardarActualizacion', [PermisoController::class, 'guardarActualizacion'])->name('guardarActualizacion');


    Route::get('/Permiso-form', [ProyectoController::class, 'PermisosForm'])->name('permisos.form');
    Route::get('/permisos', [PermisoController::class, 'index'])->name('permisos.index');
    Route::get('/permisosAll', [PermisoController::class, 'indexAll'])->name('permisosAll.index');

    Route::get('/form', [ProyectoController::class, 'FunctionName'])->name('form');
    Route::get('/estadisticas', [HomeController::class, 'Estadisticas']);

    Route::get('/anda', [AndaController::class, 'index'])->name('index-anda');
    Route::get('/anda-orden', [AndaController::class, 'UpdateOrden'])->name('anda-orden');
    Route::get('/get-preguntas-form-anda/{idForm?}/{codigo?}', [AndaController::class, 'GetPreguntasForm'])->name('preguntas-form-anda');
    Route::get('/get-preguntas-hijas-anda/{pregunta?}/{opcion?}/{seccion?}/{loading?}', [AndaController::class, 'GetPreguntasHijas'])->name('get-preguntas-hijas-anda');
    Route::post('/guardar-formulario-anda', [AndaController::class, 'SavePreguntas'])->name('guardar-form-anda');
});

Route::get('/crear', function ()
{
   // $role = Role::updateOrCreate(['name' => 'Usuario Externo']);

    //Permission::updateOrCreate(['name' => 'externo'])->assignRole($role);

    User::find(3)->assignRole('externo');


    //User::find(3)->assignRole('Admin');

});

Route::get('/logout', function (){
    Auth::logout();
    return view('auth.login');
})->name('logout');


Route::get('/vistaOT', function() {
    return view('doc.ot');
 });



