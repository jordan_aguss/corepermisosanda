


<!DOCTYPE html>
<html lang="en">


<html>
    <head>
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css" integrity="sha384-zCbKRCUGaJDkqS1kPbPd7TveP5iyJE0EjAuZQTgFLD2ylzuqKfdKlfG/eSrtxUkn" crossorigin="anonymous">
    </head>
    
    
         <div class="card-body">

           <center><h5 > <p class="font-weight-bold"> SOLICITUD DE HABILITACIÓN DE OBRA </p></h5 ></center>
            <center><img src="{{ storage_path() . '/app/public/images/logo_color.png' }}" width="45%" ></center>  <br> 
      

            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>
                            Fecha de Ingreso
                        </th>
                        <th>
                            Expendiente N.°
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td> <strong><p> {{$fecha_creacion}} </p></strong></td>
                        <td> <strong><p>  {{$perm_codigo}}</p></strong></td>
                    </tr>
                </tbody>
            </table>
    
     
           <center> <strong> <label for="">DATOS DEL INFORMANTE</label></strong></center>
 
            <table class="table table-bordered">
               <tbody>
                   <tr>
                       <td colspan="5" >Nombre:{{isset($preguntas[382] )  ? $preguntas[382] : '' }}</td>
                   
                   </tr>
                   <tr>
                       <td colspan="5"  >Tipo de Persona:  {{isset($preguntas[385] )  ? $preguntas[385] : '' }}  </td>
                   </tr>
                   <tr>
                       <td colspan="3">DUI:  {{isset($preguntas[387] )  ? $preguntas[387] : '' }}</td>
                       <td colspan="1" >Tipo de Documento: {{isset($preguntas[389] )  ? $preguntas[389] : '' }}</td>
                       <td colspan="1">NIT: {{isset($preguntas[390] )  ? $preguntas[390] : '' }}</td>
                   </tr>
                   <tr>
                       <td colspan="3">Nacionalidad:  {{isset($preguntas[391] )  ? $preguntas[391] : '' }}</td>
                       <td colspan="1">Domicilio:  {{isset($preguntas[393] )  ? $preguntas[393] : '' }}</td>
                       <td colspan="1">Departamento: {{isset($preguntas[394] )  ? $preguntas[394] : '' }}</td>
                   </tr>

                   <tr>
                       <td colspan="3">Correo Eléctronico: {{isset($preguntas[395] )  ? $preguntas[395] : '' }}</td>
                       <td colspan="2">Télefono:{{isset($preguntas[396] )  ? $preguntas[396] : '' }}</td>
                   </tr>

                   <tr>
                       <td colspan="5">Razón Social (Si Aplica):  {{isset($preguntas[397] )  ? $preguntas[397] : '' }}</td>
                   </tr>

                   <tr>
                       <td colspan="2">Inscrito al N° CNR:  {{isset($preguntas[398] )  ? $preguntas[398] : '' }}</td>
                       <td colspan="1">Libro:{{isset($preguntas[399] )  ? $preguntas[399] : '' }}</td>
                       <td colspan="1">Fecha: {{isset($preguntas[400] )  ? $preguntas[400] : '' }}</td>
                       <td colspan="1">NIT:{{isset($preguntas[401] )  ? $preguntas[401] : '' }} </td>
                   </tr>

                   <tr>
                       <td colspan="3">Nombre del Proyecto:{{isset($preguntas[419] )  ? $preguntas[419] : '' }} </td>
                       <td colspan="2">Tipo de Proyecto:  {{isset($preguntas[403] )  ? $preguntas[403] : '' }}</td>
                   </tr>
                   <tr>
                       <td colspan="2">Propietario: {{isset($preguntas[404] )  ? $preguntas[404] : '' }}</td>
                       <td colspan="1">Ubicación: {{isset($preguntas[405] )  ? $preguntas[405] : '' }} </td>
                       <td colspan="1">Municipio: {{isset($preguntas[406] )  ? $preguntas[406] : '' }}</td>
                       <td colspan="1">Departamento: {{isset($preguntas[407] )  ? $preguntas[407] : '' }}</td>
                   </tr>
               </tbody>
            </table>

            <hr>


            <center> <strong> <label for="">DATOS COMPLEMENTARIOS (Sí aplica)</label></strong></center>

            <table class="table table-bordered"> 
                <tbody>
                    <tr>
                        <td colspan="4">Fecha Aprobación ANDA: {{isset($preguntas[408] )  ? $preguntas[408] : '' }}</td>
                    </tr>
                    <tr>
                        <td>Nº {{isset($preguntas[409] )  ? $preguntas[409] : '' }}</td>
                        <td>Ref. Urb. {{isset($preguntas[410] )  ? $preguntas[410] : '' }}</td>
                        <td>Ref. Com. {{isset($preguntas[411] )  ? $preguntas[411] : '' }}</td>
                        <td>Fecha: </td>
                    </tr>
                    <tr>
                        <td  colspan="4">Número de servicios a Habilitar</td>
                    </tr>
                    <tr>
                        <td colspan="2"> Cantidad Acueducto {{isset($preguntas[413] )  ? $preguntas[413] : '' }}</td>
                        <td colspan="2"> Cantidad Alcantarillado {{isset($preguntas[414] )  ? $preguntas[414] : '' }}</td>
                    </tr>
                    <tr>
                        <td>Para proyectos: Cantidad de Conexiones solicitados para  </td>
                        <td>Cantidad Casetas: {{isset($preguntas[415] )  ? $preguntas[415] : '' }}</td>
                        <td>Áreas Verdes: {{isset($preguntas[416] )  ? $preguntas[416] : '' }}</td>
                        <td>Áreas Comunes: {{isset($preguntas[417] )  ? $preguntas[417] : '' }}</td>
                    </tr>
                  
                    <tr>
                        <td colspan="4">Nombre del receptor:{{isset($preguntas[418] )  ? $preguntas[418] : '' }}</td>
                      </tr>
                </tbody>
            </table>


            <hr>

            <center> <strong> <label for="">FIRMA DEL SOLICITANTE</label></strong></center>

            <table>
                <tbody>
                    <tr>
                        <td> <br>
                            ________________________________________________________ Sello: <br>
                            Nombre y firma del solicitante (Propietario, Representante Legal o Apoderado, Profesional responsalbe)
                        </td>
                    </tr>
                </tbody>
            </table>
         
     </div>
 </html>
 
 