<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\ClasesExternas\MetodosGenerales;

class ConsultaPublica extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    { 
        Schema::create(MetodosGenerales::$Esquema . 'consulta_publica', function (Blueprint $table) {
            $table->id('CONP_ID')->comment('ID DE LA TABLA');
            $table->string('CONP_NOTAS')->comment('');
            $table->string('CONP_LUGAR')->comment('');
            $table->string('CONP_ADJUNTO')->comment('');
            $table->string('CONP_TIPO',1)->comment('');
            $table->datetime('CONP_FINICIO')->comment('');
            $table->datetime('CONP_FFIN')->comment('');
            $table->datetime('CONP_FCREACION')->comment('');
            $table->unsignedInteger('CONP_APROBADA')->comment('');
            $table->datetime('CONP_FAPROBADA')->comment('');
            $table->unsignedInteger('CONP_ELABORADO')->comment('');
            $table->datetime('CONP_FELABORADO')->comment('');
            $table->unsignedInteger('CONP_SUPERACION')->comment('');
            $table->datetime('CONP_FSUPERACION')->comment('');
            $table->datetime('CONP_FESTADODOC')->comment('');
            $table->unsignedInteger('EVAL_ID')->comment('');
            $table->unsignedInteger('ESTD_ID')->comment('');
            $table->unsignedInteger('CONP_ACTIVO')->comment('');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(MetodosGenerales::$Esquema . 'consulta_publica');
    }
}
