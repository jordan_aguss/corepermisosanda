<!DOCTYPE html>
<html>
    <head>
        @include('correos.estilos')
    </head>
    <body>
        <div class=" offset-1">
            <div class="card p-4 mt-5 mb-5">

                @yield('Data');
            </div>
        </div>
    </body>
</html>