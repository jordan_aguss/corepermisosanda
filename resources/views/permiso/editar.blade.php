<form class="card" id="FormPermiso">
    <div class="col-12 m-0 p-0" id="tabdoc">
        <div class="card shadow mb-3" >
            <div class="card-body">
                <div class="table-responsive">

                    @include('preguntas.form-preguntas')
                </div>
                <input type="hidden" name="tipo" value="{{$tipo}}">
                <input type="hidden" name="PFormulario" value="{{$PFormulario->perf_id}}">
                <button class="btn btn-success" type="button" onclick="MakeRequestData('{{route('guardarActualizacion')}}', '#bodyContent', true, '', 'POST', 2, '#FormPermiso', true)">
                    <i class="fas fa-save"></i> &nbsp; Editar
                </button>
            </div>
        </div>
    </div>
</form>
