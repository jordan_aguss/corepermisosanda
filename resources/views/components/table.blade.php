<div class=" m-0">
    <div class="bg-white p-0 mt-1 ms-4 mb-3 me-4">
        <div class="row">
            
            <div class="card-header p-0 {{$clases}}">

                @if(isset($Titulo))            
                    <div class="black ms-2 col-12 mb-2 mt-0"><b>{{$Titulo}}</b></div>
                @endif
            </div>

            <div class="card-body p-0 {{$clases}}">
                <div class="table-responsive">
                    <table class="table card-table table-vcenter text-nowrap " id="dataTable" width="100%" cellspacing="0">
                        <thead class="whiteSmoke">
                            <tr class="small black">
                                {{$header}}
                            </tr>
                        </thead>
                        <tbody>
                            {{$body}}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>        