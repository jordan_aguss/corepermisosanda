
<div class="ml-3 mr-2 mt-3 mb-5">
    
    <div class="card-body shadow-lg bg-white mt-3 ml-4 mr-4" style="min-height: 750px;">
        <div class="row col-12 mt-0 ms-1 mb-3">
            {{$content}}
        </div>
    </div>
</div>