<div class="bg-white {{$Clases}} p-2 mb-3" style="min-height: 100px;">
    <div class="card  card-header-actions m-0 p-0">

        <div class="card-header">
            {{$Titulo}}
        </div>

        <div class="card-body">
            {{$Body}}
        </div>


        <div class="card-footer small text-muted text-danger">
            {{$Eventos}}
        </div>

    </div>
</div>
