{{-- @extends('templates.MainTemplate')
@section('body') --}}
<div class="col-11 ml-5 card bg-white shadow m-auto mt-0">
    <div class="card-header">
        Seguimiento de Permiso
    </div>
    <div class="card-body">
        <div class="timeline">
            <div class="timeline-item">
                <div class="timeline-item-marker">
                    <div class="timeline-item-marker-text">Estado 1</div>
                    <div class="timeline-item-marker-indicator bg-primary-soft text-primary"><i class="far fa-clock"></i></div>
                </div>
                <div class="timeline-item-content pt-0">
                    <div class="card shadow-sm">
                        <div class="card-body">
                            <h5 class="text-primary">En Espera</h5>
                            Permiso en ESPERA de aceptacion.
                        </div>
                    </div>
                </div>
            </div>
            <div class="timeline-item">
                <div class="timeline-item-marker">
                    <div class="timeline-item-marker-text">Estado 2</div>
                    <div class="timeline-item-marker-indicator bg-warning-soft text-warning"><i class="fas fa-sync-alt"></i></div>
                </div>
                <div class="timeline-item-content pt-0">
                    <div class="card shadow-sm">
                        <div class="card-body">
                            <h5 class="text-warning">En Proceso</h5>
                            Permiso en PROCESO de aceptación.
                        </div>
                    </div>
                </div>
            </div>
            <div class="timeline-item">
                <div class="timeline-item-marker">
                    <div class="timeline-item-marker-text">Estado 3</div>
                    <div class="timeline-item-marker-indicator bg-secondary-soft text-secondary"><i class="far fa-file-alt"></i></div>
                </div>
                <div class="timeline-item-content pt-0">
                    <div class="card shadow-sm">
                        <div class="card-body">
                            <h5 class="text-secondary">En Revisión</h5>
                            Permiso en REVISIÓN.
                        </div>
                    </div>
                </div>
            </div>
            <div class="timeline-item">
                <div class="timeline-item-marker">
                    <div class="timeline-item-marker-text">Estado 4</div>
                    <div class="timeline-item-marker-indicator bg-success-soft text-success"><i class="fas fa-check"></i></i></div>
                </div>
                <div class="timeline-item-content pt-0">
                    <div class="card shadow-sm">
                        <div class="card-body">
                            <h5 class="text-success">Listo</h5>
                            Permiso REVISADO
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
{{-- @endsection --}}