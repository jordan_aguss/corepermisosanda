<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Sector extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    { 
        Schema::create('sector', function (Blueprint $table) {
            $table->id('SECT_ID');
            $table->string('SECT_CODIGO',100)->nullable();
            $table->string('SECT_NOMBRE',100);
            $table->unsignedInteger('SECT_PADRE')->nullable();
            $table->unsignedInteger('SECT_NIVEL');
            $table->unsignedInteger('SECT_ATOMICO');
            $table->unsignedInteger('SECT_VISIBLE');
            $table->unsignedInteger('SECT_VISIBLE_IOA');
            $table->unsignedInteger('SECT_CATEGORIZA')->nullable();
            $table->unsignedInteger('CATE_ID')->nullable();
            $table->unsignedInteger('SECT_PONDERACION')->nullable();
            $table->unsignedInteger('PREG_AUTOMATICA')->nullable();
            $table->string('SECT_URLDOC')->nullable();
            $table->string('SECT_NOMBREGENERAL')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('sector');
    }
}
