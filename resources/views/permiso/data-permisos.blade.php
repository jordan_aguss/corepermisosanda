


    @foreach ($Permisos as $item)
        <tr>
            <td> {{$item->perm_codigo}} </td>
            <td> {{$item->name}} </td>
            <td> {{$item->form_nombre}} </td>
            <td>{{$item->estg_nombre}}</td>
            <td> {{$item->perm_fecha}}</td>

            <td>

                @switch($item->estc_tipo)
                    @case(1)

                    <a type="button" class="btn btn-warning btn-sm" onclick="LoadPage('{{route('Proyecto.Form', [$item->form_new, $item->perm_id])}}')">{{$item->Nombre}}</a>
                        @break
                    @case(2)
                    <a type="button" class="btn btn-warning btn-sm" onclick="LoadPage('{{route('actualizarPermisos', [$item->perm_id, $item->form_new])}}')">{{$item->Nombre}}</a>
                        @break
                    @default

                @endswitch

                <select name="" id="" class="selectpicker" onchange="window.open(this.value,'_blank')">
                    <option value="" selected disabled>Seleccione el formulario</option>

                    @php
                        $formularios = getForms($item->perm_id);
                    @endphp

                    @foreach ($formularios as $item)
                        <option  value="{{route('permisos.detalle_preguntas', [$item->perm_id, $item->form_id, $item->perf_id])}}"> {{$item->form_nombre}}  <a href=""><i class="fas fa-eye"></i></a></option>
                    @endforeach

                </select>
                {{-- <a type="button" class="btn btn-success btn-sm" href='{{route('permisos.detalle_preguntas', [$item->perm_id, $item->form_id])}}' target="_blank"> --}}
            </td>

        </tr>
    @endforeach

<tr>
    <td colspan="5">
        {!! $Permisos->links() !!}
    </td>
</tr>
