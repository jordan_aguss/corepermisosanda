

<div class="card col-11 m-auto">
    
    <div class="card-header border-bottom">
        <!-- Wizard navigation-->
        <div class="nav nav-pills nav-justified flex-column flex-xl-row nav-wizard" id="cardTab" role="tablist">
            <!-- Wizard navigation item 1-->
            <a class="nav-item nav-link active" id="wizard1-tab" href="#wizard1" data-bs-toggle="tab" role="tab" aria-controls="wizard1" aria-selected="true">
                <div class="wizard-step-icon">1</div>
                <div class="wizard-step-text">
                    <div class="wizard-step-text-name">Account Setup</div>
                    <div class="wizard-step-text-details">Basic details and information</div>
                </div>
            </a>
            <!-- Wizard navigation item 2-->
            <a class="nav-item nav-link" id="wizard2-tab" href="#wizard2" data-bs-toggle="tab" role="tab" aria-controls="wizard2" aria-selected="true">
                <div class="wizard-step-icon">2</div>
                <div class="wizard-step-text">
                    <div class="wizard-step-text-name">Billing Details</div>
                    <div class="wizard-step-text-details">Credit card information</div>
                </div>
            </a>
            <!-- Wizard navigation item 3-->
            <a class="nav-item nav-link" id="wizard3-tab" href="#wizard3" data-bs-toggle="tab" role="tab" aria-controls="wizard3" aria-selected="true">
                <div class="wizard-step-icon">3</div>
                <div class="wizard-step-text">
                    <div class="wizard-step-text-name">Preferences</div>
                    <div class="wizard-step-text-details">Notification and account options</div>
                </div>
            </a>
            <!-- Wizard navigation item 4-->
            <a class="nav-item nav-link" id="wizard4-tab" href="#wizard4" data-bs-toggle="tab" role="tab" aria-controls="wizard4" aria-selected="true">
                <div class="wizard-step-icon">4</div>
                <div class="wizard-step-text">
                    <div class="wizard-step-text-name">Review &amp; Submit</div>
                    <div class="wizard-step-text-details">Review and submit changes</div>
                </div>
            </a>
        </div>
    </div>
    <div class="card-body">
        <div class="tab-content" id="cardTabContent">
           
            <div class="tab-pane fade show active" id="wizard1" role="tabpanel" aria-labelledby="wizard1-tab">
                <div class="row justify-content-center">
                    <div class="col-xxl-6 col-xl-8">
                        <h3 class="text-primary">Step 1</h3>
                        <h5 class="card-title mb-4">Primera vista</h5>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade" id="wizard2" role="tabpanel" aria-labelledby="wizard1-tab">
                <div class="row justify-content-center">
                    <div class="col-xxl-6 col-xl-8">
                        <h3 class="text-primary">Step 1</h3>
                        <h5 class="card-title mb-4">Segunda vista</h5>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade" id="wizard3" role="tabpanel" aria-labelledby="wizard1-tab">
                <div class="row justify-content-center">
                    <div class="col-xxl-6 col-xl-8">
                        <h3 class="text-primary">Step 1</h3>
                        <h5 class="card-title mb-4">Tercera vista</h5>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade" id="wizard4" role="tabpanel" aria-labelledby="wizard1-tab">
                <div class="row justify-content-center">
                    <div class="col-xxl-6 col-xl-8">
                        <h3 class="text-primary">Step 1</h3>
                        <h5 class="card-title mb-4">Cuarta vista</h5>
                    </div>
                </div>
            </div>


        </div>
    </div>

</div>