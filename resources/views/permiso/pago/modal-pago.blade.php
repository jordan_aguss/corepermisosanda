
<div class="modal fade" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Asignar Técnico</h5>
        </div>
        <div class="modal-body">
            <form action="#" id="formPago" name="formPago">
                    <div class="form-group col-md-12">
                      <label for="inputEmail4">N° Tarjeta</label>
                      <input id="NumTarjeta" class="form-control" type="number" placeholder="0000-0000-0000-0000" required>
                    </div>
                    <div class="form-group col-md-12">
                      <label for="inputPassword4">Fecha Expiración</label>
                      <input id="FechaExpiracion" type="text" class="form-control" id="inputPassword4" placeholder="MM/YY" required>
                    </div>
                    <div class="form-group col-md-12">
                        <label for="inputPassword4">CVC</label>
                        <input id="CVC" type="text" class="form-control" id="inputPassword4" placeholder="1234" required>
                      </div>  
                    <hr>
                      <div>
                        <button id="cancel-btn" data-dismiss="modal" class="btn btn-secondary" >Salir</button>
                        <a onclick="validarPago()" type="button" class="btn btn-primary">Realizar Pago</a>
                      </div>
            </form>
        </div>
        <div class="modal-footer">
         
        </div>
      </div>
    </div>
  </div>


  <script>

      function validarPago(){
          if(!$("#NumTarjeta").val() || !$("#FechaExpiracion").val() || !$("#CVC").val() ){
            $("#btnGuardar").prop('disabled', true);
          }else{
            $('#cancel-btn').click();
            $("#btnGuardar").prop('disabled', false);
          }      
      }


  </script>