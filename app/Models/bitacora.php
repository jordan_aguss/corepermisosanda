<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;

class bitacora extends Model
{
    /*Este modelo sera el encargado de llamara a nuestra tabla bitacora
    para llevar el registro de actividad del usuario*/
    use HasFactory;
    protected $table = 'bitacora';
    protected $primaryKey = 'bita_id';
    public $timestamps = false;

    ///metodo estatico instanciable en cualquier lugar donde sea llamado
    public static function MakeBitacora($accion, $tabla, $registro)
    {
        //crearemos una bitacora de cuando un usuario guarda un nuevo rol
        $bitacora = new bitacora();
        $bitacora->bita_id = Str::random(20);
        $bitacora->bita_accion = $accion;
        $bitacora->bita_registro = $registro;
        $bitacora->bita_tabla = $tabla;
        $bitacora->usua_login = Session::get('user-info')->usua_login;
        $bitacora->bita_ip = request()->ip();
        $bitacora->bita_estatus = 0;
        $bitacora->bita_fecha = Carbon::now()->toDateString(); //fecha de creacion de la bitacora hecha con Carbon
        $bitacora->bita_hora = Carbon::now('GMT-6', 'H:i:s')->toTimeString(); //fecha de creacion de la bitacora
        $bitacora->save();
    }
}
