@if(App\Models\User::find(Auth::id())->name != 'AGENCIA')

@else

<div id="FormPermiso">
<div class="row" >
    <div class="col-xl-3 col-md-6 mb-4">
        <div class="card border-start-lg border-start-primary h-100">
            <div class="card-body">
                <div class="d-flex align-items-center">
                    <div class="flex-grow-1">
                        <div class="small fw-bold text-primary mb-1">Total de Solcitudes</div>
                        <div class="h5">{{$totalPermisos}}</div>
                    </div>
                    <div class="ms-2"><i class="fas fa-chart-bar fa-2x text-gray-200"></i></div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xl-3 col-md-6 mb-4">
        <div class="card border-start-lg border-start-secondary h-100">
            <div class="card-body">
                <div class="d-flex align-items-center">
                    <div class="flex-grow-1">
                        <div class="small fw-bold text-secondary mb-1">Proyectos Totales</div>
                        <div class="h5">{{$totalProyectos}}</div>
                    </div>
                    <div class="ms-2"><i class="fas fa-chart-bar fa-2x text-gray-200"></i></div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xl-3 col-md-6 mb-4">
        <div class="card border-start-lg border-start-success h-100">
            <div class="card-body">
                <div class="d-flex align-items-center">
                    <div class="flex-grow-1">
                        <div class="small fw-bold text-success mb-1">Comunidades Totales</div>
                        <div class="h5">{{$totalComunidades}}</div>
                    </div>
                    <div class="ms-2"><i class="fas fa-chart-bar fa-2x text-gray-200"></i></div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xl-3 col-md-6 mb-4">
        <div class="card border-start-lg border-start-info h-100">
            <div class="card-body">
                <div class="d-flex align-items-center">
                    <div class="flex-grow-1">
                        <div class="small fw-bold text-info mb-1">Oto</div>
                        <div class="h5">19</div>
                    </div>
                    <div class="ms-2"><i class="fas fa-chart-bar fa-2x text-gray-200"></i></div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-4 mb-4">
        <div class="card mb-4">
            <div class="card-body text-center p-5">
                <div><i class="fas fa-clipboard-list fa-2x text-gray-200"></i></div>
                <h4>Reporte General</h4>
                <p class="mb-4">Puede visualizar los reportes por fecha y departamento</p>
                <a class="btn btn-primary p-3" href="#!">Reporte</a>
            </div>
        </div>
        <div class="card mb-4">
            <div class="card-header">Reporte</div>
            <div class="list-group list-group-flush small">
                <a class="list-group-item list-group-item-action" href="#!">
                    <i class="fas fa-book fa-fw text-blue me-2"></i>
                    Nombre del dato 12
                </a>
                <a class="list-group-item list-group-item-action" href="#!">
                    <i class="fas fa-book fa-fw text-purple me-2"></i>
                    Nombre del dato 200
                </a>
                <a class="list-group-item list-group-item-action" href="#!">
                    <i class="fas fa-book fa-fw text-green me-2"></i>
                    Nombre del dato 10
                </a>
                <a class="list-group-item list-group-item-action" href="#!">
                    <i class="fas fa-percentage fa-fw text-yellow me-2"></i>
                    Nombre del dato 3
                </a>
                <a class="list-group-item list-group-item-action" href="#!">
                    <i class="fas fa-chart-pie fa-fw text-pink me-2"></i>
                    Nombre del dato 9
                </a>
            </div>
            <div class="card-footer position-relative border-top-0">
                <a class="stretched-link" href="#!">
                    <div class="text-xs d-flex align-items-center justify-content-between">
                        Nombre del dato
                        <i class="fas fa-long-arrow-alt-right"></i>
                    </div>
                </a>
            </div>
        </div>
        <div class="card bg-primary border-0">
            <div class="card-body">
                <h5 class="text-white-50">Nombre del dato</h5>
                <div class="mb-4">
                    <span class="display-4 text-white">48</span>
                    <span class="text-white-50">Cantidad</span>
                </div>
                <div class="progress bg-white-25 rounded-pill" style="height: 0.5rem"><div class="progress-bar bg-white w-75 rounded-pill" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100"></div></div>
            </div>
        </div>
    </div>
    <div class="col-lg-8 mb-4">
        <div class="card mb-4">
            <div class="card-header">Grafico</div>
            <div class="card-body">
                <div class="chart-area"><canvas id="myAreaChart" width="100%" height="30"></canvas></div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6">
                <div class="card h-100">
                    <div class="card-header">Grafico</div>
                    <div class="card-body d-flex flex-column justify-content-center">
                        <div class="chart-bar"><canvas id="myBarChart" width="100%" height="30"></canvas></div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="card h-100">
                    <div class="card-header">Grafico</div>
                    <div class="card-body">
                        <div class="chart-pie mb-4"><canvas id="myPieChart" width="100%" height="50"></canvas></div>
                        <div class="list-group list-group-flush">
                            <div class="list-group-item d-flex align-items-center justify-content-between small px-0 py-2">
                                <div class="me-3">
                                    <i class="fas fa-circle fa-sm me-1 text-blue"></i>
                                    Nombre 1
                                </div>
                                <div class="fw-500 text-dark">55</div>
                            </div>
                            <div class="list-group-item d-flex align-items-center justify-content-between small px-0 py-2">
                                <div class="me-3">
                                    <i class="fas fa-circle fa-sm me-1 text-purple"></i>
                                    Nombre 2
                                </div>
                                <div class="fw-500 text-dark">15</div>
                            </div>
                            <div class="list-group-item d-flex align-items-center justify-content-between small px-0 py-2">
                                <div class="me-3">
                                    <i class="fas fa-circle fa-sm me-1 text-green"></i>
                                    Nombre 3
                                </div>
                                <div class="fw-500 text-dark">30</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div></div>
@endif