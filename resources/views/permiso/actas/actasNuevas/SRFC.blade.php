


<!DOCTYPE html>
<html lang="en">


<html>
    <head>
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css" integrity="sha384-zCbKRCUGaJDkqS1kPbPd7TveP5iyJE0EjAuZQTgFLD2ylzuqKfdKlfG/eSrtxUkn" crossorigin="anonymous">
    </head>
    
    
         <div class="card-body">

           <center><h5 > <p class="font-weight-bold"> SOLICITUD DE RECEPCIÓN FINAL DE CAMPO </p></h5 ></center>
            <center><img src="{{ storage_path() . '/app/public/images/logo_color.png' }}" width="45%" ></center>  <br> 
      

            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>
                            Fecha de Ingreso
                        </th>
                        <th>
                            Expendiente N.°
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td> <strong><p> {{$fecha_creacion}} </p></strong></td>
                        <td> <strong><p>  {{$perm_codigo}}</p></strong></td>
                    </tr>
                </tbody>
            </table>
    
     
           <center> <strong> <label for="">DATOS DEL INFORMANTE</label></strong></center>
 
            <table class="table table-bordered">
               <tbody>
                   <tr>
                       <td colspan="2" >Nombre:{{isset($preguntas[322] )  ? $preguntas[322] : '' }}</td>
                       <td colspan="2"> Apellidos:  {{isset($preguntas[323] )  ? $preguntas[323] : '' }}</td>
                   
                   </tr>
                   <tr>
                       <td colspan="4"  >Tipo de Persona:  {{isset($preguntas[324] )  ? $preguntas[324] : '' }}  </td>
                   </tr>
                   <tr>
                       <td colspan="2">DUI:  {{isset($preguntas[325] )  ? $preguntas[325] : '' }}</td>
                       <td colspan="1" >Tipo de Documento:{{isset($preguntas[326] )  ? $preguntas[326] : '' }}</td>
                       <td colspan="1">NIT: {{isset($preguntas[327] )  ? $preguntas[327] : '' }}</td>
                   </tr>
                   <tr>
                       <td colspan="2">Nacionalidad:  {{isset($preguntas[328] )  ? $preguntas[328] : '' }}</td>
                       <td colspan="1">Domicilio: {{isset($preguntas[329] )  ? $preguntas[329] : '' }}</td>
                       <td colspan="1">Departamento: {{isset($preguntas[330] )  ? $preguntas[330] : '' }}</td>
                   </tr>

                   <tr>
                       <td colspan="2">Correo Eléctronico: {{isset($preguntas[331] )  ? $preguntas[331] : '' }}</td>
                       <td colspan="2">Télefono: {{isset($preguntas[332] )  ? $preguntas[332] : '' }}</td>
                   </tr>

                   <tr>
                       <td colspan="4">Razón Social (Si Aplica): {{isset($preguntas[333] )  ? $preguntas[333] : '' }}</td>
                   </tr>

                   <tr>
                       <td colspan="1">Inscrito al N° CNR: {{isset($preguntas[334] )  ? $preguntas[334] : '' }}</td>
                       <td colspan="1">Libro:{{isset($preguntas[335] )  ? $preguntas[335] : '' }}</td>
                       <td colspan="1">Fecha: {{isset($preguntas[336] )  ? $preguntas[336] : '' }}</td>
                       <td colspan="1">NIT: {{isset($preguntas[337] )  ? $preguntas[337] : '' }} </td>
                   </tr>

                   <tr>
                       <td colspan="2">Nombre del Proyecto:{{isset($preguntas[338] )  ? $preguntas[338] : '' }} </td>
                       <td colspan="2">Tipo de Proyecto:  {{isset($preguntas[339] )  ? $preguntas[339] : '' }}</td>
                   </tr>
                   <tr>
                       <td colspan="1">Propietario:{{isset($preguntas[340] )  ? $preguntas[340] : '' }}</td>
                       <td colspan="1">Ubicación: {{isset($preguntas[341] )  ? $preguntas[341] : '' }} </td>
                       <td colspan="1">Municipio: {{isset($preguntas[342] )  ? $preguntas[342] : '' }}</td>
                       <td colspan="1">Departamento: </td>
                   </tr>
               </tbody>
            </table>

            <hr>


            <center> <strong> <label for="">DATOS COMPLEMENTARIOS (Sí aplica)</label></strong></center>

            <table class="table table-bordered"> 
                <tbody>
                    <tr>
                        <td colspan="4">Fecha Aprobación ANDA: {{isset($preguntas[343] )  ? $preguntas[343] : '' }}</td>
                    </tr>
                    <tr>
                        <td>Nº {{isset($preguntas[345] )  ? $preguntas[345] : '' }}</td>
                        <td>Ref. Urb.  {{isset($preguntas[348] )  ? $preguntas[348] : '' }}</td>
                        <td>Ref. Com.  {{isset($preguntas[350] )  ? $preguntas[350] : '' }}</td>
                        <td>Fecha: {{isset($preguntas[351] )  ? $preguntas[351] : '' }}</td>
                    </tr>
                    <tr>
                        <td colspan="2">Sistema que solicita: {{isset($preguntas[354] )  ? $preguntas[354] : '' }}</td>
                        <td>Cantidad Alcantarillado: {{isset($preguntas[356] )  ? $preguntas[356] : '' }}</td>
                        <td>Cantidad Acueducto: {{isset($preguntas[357] )  ? $preguntas[357] : '' }}</td>
                    </tr>
                </tbody>
            </table>

            <table>
                <tbody>
                    <tr>
                        <td> <br>
                            __________________________________________________________________ <br>
                            Nombre y firma del solicitante (Propietario, Representante Legal o Apoderado, Profesional responsalbe)
                        </td>
                    </tr>
                </tbody>
            </table>
         
     </div>
 </html>
 
 