@extends('templates.html_page')

@section('Data')
<div class="col-10 m-auto my-5">

    <div class="card">
        <div class="card-header border-bottom">
            <ul class="nav nav-tabs card-header-tabs" id="cardTab" role="tablist">

                <li class="nav-item">
                    <a class="nav-link active" id="example-tab" href="#opinion" data-bs-toggle="tab" role="tab" aria-controls="example" aria-selected="false">Opinion tecnica</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" id="overview-tab" href="#overview" data-bs-toggle="tab" role="tab" aria-controls="overview" aria-selected="true">Permiso de factibilidad</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" id="example-tab" href="#certificado" data-bs-toggle="tab" role="tab" aria-controls="example" aria-selected="false">Certificado de factibilidad</a>
                </li>
            </ul>
        </div>
        <div class="card-body">
            <div class="tab-content" id="cardTabContent">
                <div class="tab-pane fade" id="overview" role="tabpanel" aria-labelledby="overview-tab">

                    <form action="" id="FormPermiso">
                        @csrf
                        <input type="hidden" name="tipo" value="1">

                        <div class="form-group">
                            <label for="" class="text-black small"> <strong> Codigo del permiso</strong></label>
                            <input type="text" name="codigo" id="codigo" class="form-control">
                        </div>

                        <div class="form-group">
                            <label for="" class="text-black small"> <strong>Tipo de permiso</strong></label>
                            <select name="campoPregunta[105]" id="" class="form-control"
                            onchange="MakeRequestData('{{route('preguntas-form-anda')}}' + '/' + $(this).val() + '/' + $('#codigo').val(), '#divPermiso', true)">
                                <option value="">--Seleccionar--</option>
                                <option value="7">Proyecto</option>
                                <option value="6">Comunidad</option>
                            </select>
                        </div>


                        <div id="divPermiso" class="row">
                        </div>
                    </form>

                    <div class="text-end">
                        <button class="btn btn-success"
                        onclick="MakeRequestData('{{route('guardar-form-anda')}}', '#divPermiso', true, '', 'POST',
                        2, '#FormPermiso', true)">
                            <i class="fas fa-save"></i> &nbsp; Guardar
                        </button>
                    </div>

                </div>


                <div class="tab-pane fade show active" id="opinion" role="tabpanel" aria-labelledby="example-tab">

                    <div class="form-group">
                        <label for="" class="text-black small"
                       > <strong> Codigo del permiso</strong></label>
                        <input type="text" name="codigo" id="" class="form-control"
                        onkeyup="if(event.keyCode === 13) MakeRequestData('{{route('preguntas-form-anda', [11])}}' + '/' + $(this).val(), '#POpinion', true)">
                    </div>

                    <form action="" id="FormOpinion">
                        @csrf
                        <input type="hidden" name="tipo" value="4">
                        <h5 class="mt-4 mb-4">Preguntas formulario</h5>
                        <div id="POpinion">

                        </div>
                    </form>

                    <div class="text-end">
                        <button class="btn btn-success"
                        onclick="MakeRequestData('{{route('guardar-form-anda')}}', '#POpinion', true, '', 'POST',
                        2, '#FormOpinion', true)">
                            <i class="fas fa-save"></i> &nbsp; Guardar
                        </button>
                    </div>

                </div>


                <div class="tab-pane fade" id="certificado" role="tabpanel" aria-labelledby="example-tab">



                    <form action="" id="FormCertificado">
                        @csrf
                        <input type="hidden" name="tipo" value="3">
                        <h5 class="mt-4 mb-4">Preguntas formulario</h5>


                        <div class="form-group">
                            <label for="" class="text-black small"
                            > <strong> Codigo del permiso</strong></label>
                            <input type="text" name="codigo" id="codigoCertificado" class="form-control"
                            >
                        </div>


                        <div class="form-group">
                            <label for="" class="text-black small"> <strong>Tipo de permiso</strong></label>
                            <select name="campoPregunta[105]" id="" class="form-control"
                            onchange="MakeRequestData('{{route('preguntas-form-anda')}}' + '/' + $(this).val() + '/' + $('#codigoCertificado').val(), '#PCertificado', true)">
                                <option value="">--Seleccionar--</option>
                                <option value="8">Proyecto</option>
                                <option value="12">Comunidad</option>
                            </select>
                        </div>


                        <div id="PCertificado"></div>
                    </form>

                    <div class="text-end">
                        <button class="btn btn-success"
                        onclick="MakeRequestData('{{route('guardar-form-anda')}}', '#PCertificado', true, '', 'POST',
                        2, '#FormCertificado', true)">
                            <i class="fas fa-save"></i> &nbsp; Guardar
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
