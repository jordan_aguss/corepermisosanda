<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\ClasesExternas\MetodosGenerales;

class Proyecto extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    { 
        Schema::create(MetodosGenerales::$Esquema . 'proyecto', function (Blueprint $table) {
            $table->id('PROY_ID')->comment('ID DE LA TABLA');            
            $table->unsignedInteger('TITU_ID')->nullable()->comment('id del titular asignado');
            $table->string('PROY_INICIALIDENTIFICADOR')->nullable()->comment('identificador inicial del año');
            $table->unsignedInteger('PROY_IDENTIFICADOR')->nullable()->comment('identificador del proyecto');
            $table->unsignedInteger('PROY_ANIOIDENTIFICADOR')->nullable()->comment('identificador del año');
            $table->string('PROY_NOMBRE')->nullable()->comment('nombre  del proyecto');
            $table->unsignedInteger('SECT_ID')->nullable()->comment('id del sector del proyecto');
            $table->decimal('PROY_MONTO')->nullable()->comment('monto del proyecto');
            $table->text('PROY_DESCRIPCION', 1000)->nullable()->comment('descripcion del proyecto');
            $table->text('PROY_UBICACION', 1000)->nullable()->comment('coordenadas del proyecto');
            $table->text('PROY_DIRECCION', 1000)->nullable()->comment('direccion del proyecto');
            $table->string('DEPA_ID')->nullable()->comment('id del departamento');
            $table->string('MUNI_ID')->nullable()->comment('id del municipio');
            $table->unsignedInteger('ESTP_ID')->nullable()->comment('id estado proyecto');
            $table->unsignedInteger('ACTI_ID')->nullable()->comment('Id actividad actual');
            $table->datetime('PROY_FACTIVIDAD')->nullable()->comment('FECHA ACTIVIDAD ACTUAL');
            $table->unsignedInteger('CATE_ID')->nullable()->comment('Id categorizacion');
            $table->datetime('PROY_FCREACION')->nullable()->comment('FECHA DE CREACION');
            $table->unsignedInteger('USUA_ID')->nullable()->comment('usuario de creacion');
            $table->unsignedInteger('PROY_TNINTERNAS')->nullable()->comment('total de notificaciones internas');
            $table->unsignedInteger('PROY_TNEXTERNAS')->nullable()->comment('total de notificaciones externas');
            $table->unsignedInteger('PROY_ACTIVO')->nullable()->comment('estado del proyecto');
            $table->string('PROY_TMPID')->nullable()->comment('identificador temporal del proyecto');
            $table->unsignedInteger('PROY_TDR')->nullable()->comment('');
            $table->unsignedInteger('PROY_ANIOTDR')->nullable()->comment('');
            $table->string('PROY_INICIALTDR',20)->nullable()->comment('');
            $table->string('PROY_ARCHIVOKML')->nullable()->comment('');
            $table->unsignedInteger('PROY_PESO')->nullable()->comment('Peso del proyecto');
            $table->unsignedInteger('PROY_D100')->nullable()->comment('');
            $table->unsignedInteger('PROY_IDMODF')->nullable()->comment('Id del proyecto modificado');
            $table->string('PROY_TIPOENTRADA',50)->nullable()->comment('Tipo entrada del proyecto');
            $table->string('PROY_NOMRBENOTY')->nullable()->comment('Nombre de notificacion');
            $table->string('PROY_CORREONOTY')->nullable()->comment('Correo de notificacion');
            $table->unsignedInteger('PROY_NOTYELECTRONICA')->nullable()->comment('notificacion electronica');
            $table->unsignedInteger('PROY_PRIORIDAD')->nullable()->comment('Prioridad del proyecto');
            $table->string('PROY_GEOPOINT')->nullable()->comment('Coordenadas del proyecto');
            $table->unsignedInteger('PROY_D100TITULAR')->nullable()->comment('');
            $table->unsignedInteger('PROY_PRDX')->nullable()->comment('');
            $table->string('PROY_ASIGNACIONTEC',50)->nullable()->comment('');
            $table->string('PROY_EVALUACIONTEC',50)->nullable()->comment('');
            $table->datetime('PROY_EVALUADORTEC')->nullable()->comment('');
            $table->datetime('PROY_USUARIOCCREADO',5)->nullable()->comment('');
            $table->string('PROY_TIPOGEOREFERENCIA',5)->nullable()->comment('');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(MetodosGenerales::$Esquema . 'proyecto');
    }
}
