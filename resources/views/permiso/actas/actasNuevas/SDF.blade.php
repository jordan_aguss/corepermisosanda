


<!DOCTYPE html>
<html lang="en">


<html>
    <head>
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css" integrity="sha384-zCbKRCUGaJDkqS1kPbPd7TveP5iyJE0EjAuZQTgFLD2ylzuqKfdKlfG/eSrtxUkn" crossorigin="anonymous">
    </head>
    
    
         <div class="card-body">

           <center><h5 > <p class="font-weight-bold">  SOLICITUD DE FACTIBILIDAD </p></h5 ></center>
          <center><img src="{{ storage_path() . '/app/public/images/logo_color.png' }}" width="45%" ></center>  <br> 
      

            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>
                            Fecha de Solicitud
                        </th>
                        <th>
                            Expendiente N.°
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td> <strong><p> {{$fecha_creacion}} </p></strong></td>
                        <td> <strong><p>  {{$perm_codigo}}</p></strong></td>
                    </tr>
                </tbody>
            </table>
    
     
           <center> <strong> <label for="">DATOS DEL SOLICITANTE</label></strong></center>
 
            <table class="table table-bordered">
               <tbody>
                   <tr>
                       <td colspan="4" >Nombre:{{isset($preguntas[174] )  ? $preguntas[174] : '' }}</td>
                   
                   </tr>
                   <tr>
                       <td colspan="4"  >Tipo de Persona:  {{isset($preguntas[175] )  ? $preguntas[175] : '' }}</td>
                   </tr>
                   <tr>
                       <td colspan="1">NIT: {{isset($preguntas[176] )  ? $preguntas[176] : '' }}</td>
                       <td colspan="1" >Tipo de Documento: {{isset($preguntas[178] )  ? $preguntas[178] : '' }}</td>
                       <td colspan="1">DUI: {{isset($preguntas[177] )  ? $preguntas[177] : '' }}</td>
                       <td colspan="1"> Pasaporte: {{isset($preguntas[457] )  ? $preguntas[457] : '' }} </td>

                   </tr>
                   <tr>
                       <td colspan="1">Nacionalidad: {{isset($preguntas[179] )  ? $preguntas[179] : '' }}</td>
                       <td colspan="2">Domicilio:  {{isset($preguntas[180] )  ? $preguntas[180] : '' }}</td>
                       <td colspan="1">Departamento:  {{isset($preguntas[181] )  ? $preguntas[181] : '' }}</td>
                   </tr>

                   <tr>
                       <td colspan="2">Correo Eléctronico: {{isset($preguntas[182] )  ? $preguntas[182] : '' }}</td>
                       <td colspan="2">Télefono:  {{isset($preguntas[183] )  ? $preguntas[183] : '' }}</td>
                   </tr>

                   <tr>
                       <td colspan="4">Razón Social (Si Aplica): {{isset($preguntas[184] )  ? $preguntas[184] : '' }}</td>
                   </tr>

                   <tr>
                       <td>Inscrito al N° CNR: {{isset($preguntas[185] )  ? $preguntas[185] : '' }}</td>
                       <td>Libro:  {{isset($preguntas[186] )  ? $preguntas[186] : '' }}</td>
                       <td>Fecha: {{isset($preguntas[187] )  ? $preguntas[187] : '' }}</td>
                       <td>NIT:  {{isset($preguntas[188] )  ? $preguntas[188] : '' }}</td>
                   </tr>

                   <tr>
                       <td colspan="2">Nombre del Proyecto:  {{isset($preguntas[419] )  ? $preguntas[419] : '' }}</td>
                       <td colspan="2">Tipo de Proyecto:  {{isset($preguntas[246] )  ? $preguntas[246] : '' }}</td>
                   </tr>
                   <tr>
                       <td>Propietario:  {{isset($preguntas[193] )  ? $preguntas[193] : '' }}</td>
                       <td>Ubicación: {{isset($preguntas[194] )  ? $preguntas[194] : '' }} </td>
                       <td>Municipio: {{isset($preguntas[195] )  ? $preguntas[195] : '' }}</td>
                       <td>Departamento: {{isset($preguntas[196] )  ? $preguntas[196] : '' }}</td>
                   </tr>
               </tbody>
            </table>

            <hr>


            <center> <strong> <label for="">DATOS COMPLEMENTARIOS</label></strong></center>

            <table class="table table-bordered"> 
                <tbody>
                    <tr>
                        <td>Factibilidad que solicita:{{isset($preguntas[197] )  ? $preguntas[197] : '' }}</td>
                        <td> Cantidad Acueductos:  {{isset($preguntas[198] )  ? $preguntas[198] : '' }}</td>
                        <td>Cantidad Alcantarillado:  {{isset($preguntas[199] )  ? $preguntas[199] : '' }}  </td>
                    </tr>
                    <tr>
                        <th colspan="3">Datos Característicos del Proyecto</th>
                    </tr>
                    <tr>
                        <td colspan="3">Tipo de proyecto a desarrollar: {{isset($preguntas[200] )  ? $preguntas[200] : '' }}</td>
                    </tr>
                    <tr>
                        <td colspan="3">Tipo de medidor que solicita (en caso de Proyecto verticales) {{isset($preguntas[202] )  ? $preguntas[202] : '' }}</td>
                    </tr>
                    <tr>
                        <td>Cantidad Macro {{isset($preguntas[203] )  ? $preguntas[203] : '' }}</td>
                        <td>Cantidad Micro {{isset($preguntas[204] )  ? $preguntas[204] : '' }}</td>
                        <td>Cantidad Telemetrico {{isset($preguntas[205] )  ? $preguntas[205] : '' }}</td>
                    </tr>
                </tbody>
            </table>


            <hr>

            <center> <strong> <label for=""> I PROYECTOS</label></strong></center>

            <table class="table table-bordered">
                <tbody>
                    <tr>
                        <td>Área Total: {{isset($preguntas[206] )  ? $preguntas[206] : '' }}</td>
                    </tr>
                    <tr>
                        <td>Área útil: {{isset($preguntas[207] )  ? $preguntas[207] : '' }}</td>
                    </tr>
                    <tr>
                      <td>Número de lotes: {{isset($preguntas[209] )  ? $preguntas[209] : '' }}</td>
                    </tr>
                    <tr>
                        <td>Área promedio de lotes {{isset($preguntas[215] )  ? $preguntas[215] : '' }}</td>
                    </tr>
                    <tr>
                        <td>Área de construcción total {{isset($preguntas[220] )  ? $preguntas[220] : '' }}</td>
                    </tr>
                    <tr>
                        <td>Área de construcción en restaurantes {{isset($preguntas[221] )  ? $preguntas[221] : '' }}</td>
                    </tr>
                    <tr>
                        <td>Área de construcción en Centro Comerciales  {{isset($preguntas[222] )  ? $preguntas[222] : '' }}</td>
                    </tr>
                    <tr>
                        <td>Área de construcción en mercados rea de construcción en mercados </td>
                    </tr>
                    <tr>
                        <td>Área de construcción en edificios para oficina {{isset($preguntas[224] )  ? $preguntas[224] : '' }}</td>
                    </tr>
                    <tr>
                        <td>Número de alumnos en escuela {{isset($preguntas[225] )  ? $preguntas[225] : '' }}</td>
                    </tr>
                    <tr>
                        <td>Número de trabajadores en industria  {{isset($preguntas[226] )  ? $preguntas[226] : '' }}</td>
                    </tr>
                    <tr>
                        <td>Número de turnos {{isset($preguntas[227] )  ? $preguntas[227] : '' }}</td>
                    </tr>
                    <tr>
                        <td>Número de bombas en gasolinera {{isset($preguntas[228] )  ? $preguntas[228] : '' }}</td>
                    </tr>
                    <tr>
                        <td>Número de camas en hospitales {{isset($preguntas[229] )  ? $preguntas[229] : '' }}</td>
                    </tr>
                    <tr>
                        <td>Número de niveles en edificios {{isset($preguntas[230] )  ? $preguntas[230] : '' }}</td>
                    </tr>
                    <tr>
                        <td>Número de Sótanos para estacionamiento  {{isset($preguntas[231] )  ? $preguntas[231] : '' }}</td>
                    </tr>
                    <tr>
                        <td>Cantidad de edificios  {{isset($preguntas[232] )  ? $preguntas[232] : '' }}</td>
                    </tr>
                    <tr>
                        <td>Número de Clínicas Médicas {{isset($preguntas[233] )  ? $preguntas[233] : '' }}</td>
                    </tr>
                    <tr>
                        <td>Número de Clínicas Dentales {{isset($preguntas[234] )  ? $preguntas[234] : '' }}</td>
                    </tr>
                    <tr>
                        <td>Número de Habitaciones en Hoteles/Pensión {{isset($preguntas[235] )  ? $preguntas[235] : '' }}</td>
                    </tr>
                    <tr>
                        <td>Área de estacionamiento  {{isset($preguntas[236] )  ? $preguntas[236] : '' }}</td>
                    </tr>
                    <tr>
                        <td>Área de jardines  {{isset($preguntas[237] )  ? $preguntas[237] : '' }}</td>
                    </tr>
                    <tr>
                        <td>Capacidad cines, teatros (número de personas) {{isset($preguntas[238] )  ? $preguntas[238] : '' }}</td>
                    </tr>
                </tbody>
            </table>

            <hr>
            <center> <strong> <label for=""> II COMUNIDADES</label></strong></center>

            <table class="table table-bordered">
                <tbody>
                    <tr>
                        <td>Número de viviendas {{isset($preguntas[211] )  ? $preguntas[211] : '' }}</td>
                    </tr>
                    <tr>
                        <td>Área promedio de vivienda {{isset($preguntas[239] )  ? $preguntas[239] : '' }}</td>
                    </tr>
                    <tr>
                        <td>Observaciones {{isset($preguntas[241] )  ? $preguntas[241] : '' }}</td>
                    </tr>
                </tbody>
            </table>


            <hr>

            <center> <strong> <label for=""> En caso cuente con factibilidad vencida, por favor indicar.</label></strong></center>


            <table class="table table-bordered">
                <tbody>
                    <tr>
                        <td>Nº de Factibilidad {{isset($preguntas[243] )  ? $preguntas[243] : '' }}</td>
                        <td>Referencia  {{isset($preguntas[244] )  ? $preguntas[244] : '' }}</td>
                        <td>Fecha de emisión {{isset($preguntas[245] )  ? $preguntas[245] : '' }}</td>
                    </tr>
                </tbody>
            </table>

            <hr>
            <center> <strong> <label for=""> DECLARACIÓN JURADA</label></strong></center>
            <p>
                El suscrito en calidad de titular, propietario o representante legal de la socieda dueña del 
                proyecto doy fe de la veracida de la información detallada en el presente documente y anexos,
                cumpliendo con los quisitos de ley exigidos, razón por la cual asumo la responsabilidad consencuente 
                derivada de esta declaración que tien calida de declaración jurada.
            </p>

            <hr>

            <center> <strong> <label for=""> FIRMA DEL SOLICITANTE</label></strong></center>

            <p>
                    <br>
            ___________________________________________________________<br>
            Nombre y Firma del solicitante (Propietario, Representante legal o Apoderado.)
            </p>



     </div>
 </html>
 
 