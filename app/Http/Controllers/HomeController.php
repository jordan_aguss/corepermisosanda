<?php
namespace App\Http\Controllers;

use App\Models\Formulario;
use App\Models\Mensaje;
use App\Models\Permiso;
use App\Models\Sucursal;
use App\Models\User;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller {

   /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('auth.login');
    }

    public function Home(){



        $Info = Sucursal::find(Config::get('constants.id_sucursal'));

        $Noty = Mensaje::select(DB::raw('count(*) as Total'))->where('usua_receptor', Auth::id())
        ->where('mens_visto')->first()->Total;

        $totalPermisos = Permiso::all()->count();
        $totalProyectos = Permiso::join('permisos_formulario as pf', 'pf.perm_id', 'permisos.perm_id')
                            ->join('permiso_detalle','pf.perf_id', 'permiso_detalle.perf_id')
                                ->where('preg_id', 246)->where('perd_respuesta', 43)->count();
        $totalComunidades = Permiso::join('permisos_formulario as pf', 'pf.perm_id', 'permisos.perm_id')
                            ->join('permiso_detalle','pf.perf_id', 'permiso_detalle.perf_id')
                                ->where('preg_id', 246)->where('perd_respuesta', 23)->count();

       return view('main.home-view', compact('Info', 'totalPermisos', 'totalProyectos', 'totalComunidades', 'Noty'));
    }

    public function Login(){
        return view('main.login');
    }


    public function Mensajes(){

        $Notificaciones = Mensaje::where('usua_receptor', Auth::id())
        ->leftjoin('usuarios as u', 'u.usua_id', 'mensajes.usua_emisor')
        ->orderBy('mens_id', 'desc')->get(['mensajes.*', 'usua_nombre']);

        return view('mensajes.index', compact('Notificaciones'));
    }

    public function ConfirmarLectura($mensaje){
        Mensaje::where('mens_id', $mensaje)->update(['mens_visto' => 1]);
    }

    public function GetVista($Vista){
        switch ($Vista){
            case 0: return view('workflow.index');
            case 1: return view('workflow.seguimiento');
            default: return;
        }
    }

    public function Estadisticas()
    {
        return view('inicio.estadisticas');
    }
}
