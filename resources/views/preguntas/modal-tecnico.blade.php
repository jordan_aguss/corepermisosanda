<div class="modal fade" id="{{$item->perm_id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Asignar Técnico</h5>
        </div>
        <div class="modal-body">
            <form name="asignacionTecnico" id="asignacionTecnico">
                <select class="form-control" name="tecnicos" id="tecnicos">
                    <option value="" selected>Selecciones el Tecnico</option>
                    <option value="">Jose Gomez</option>
                    <option value="">Vladimir Bonilla</option>
                    <option value="">Javier Perez</option>
                </select>
            </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Salir</button>
          <a type="button" href='{{route('aprobarPermiso', $item->perm_id)}}' class="btn btn-primary">Aprobar y Asignar Tecnico</a>
        </div>
      </div>
    </div>
  </div>