<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\ClasesExternas\MetodosGenerales;

class Rol extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
     { 
         Schema::create(MetodosGenerales::$Esquema . 'rol', function (Blueprint $table) {
             $table->id('ROL_ID');
             $table->string('ROL_NOMBRE',50);
             $table->unsignedInteger('ROL_VISIBLE')->nullable();
             $table->string('ROL_TIPO', 1)->nullable();
             $table->unsignedInteger('TECNICO_INTERNO')->nullable();
         });
     }
 
     /**
      * Reverse the migrations.
      *
      * @return void
      */
     public function down()
     {
         Schema::drop(MetodosGenerales::$Esquema . 'rol');
     }
}
