<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\ClasesExternas\MetodosGenerales;

class Observacion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    { 
        Schema::create(MetodosGenerales::$Esquema . 'observacion', function (Blueprint $table) {
            $table->id('OBSE_ID')->comment('ID DE LA TABLA');
            $table->string('OBSE_NOTAS')->comment('');
            $table->string('OBSE_ADJUNTO')->comment('');
            $table->datetime('OBSE_FCREACION')->comment('FECHA DE CREACION');
            $table->datetime('OBSE_FAPROBACION')->comment('FECHA DE CREACION');
            $table->unsignedInteger('OBSE_APROBADA')->comment('ESTADO');
            $table->unsignedInteger('OBSE_SUPERADA')->comment('ESTADO');
            $table->datetime('OBSE_FSUPERACION')->comment('FECHA DE CREACION');
            $table->unsignedInteger('OBSE_ELABORADO')->comment('ESTADO');
            $table->datetime('OBSE_FELABORADO')->comment('FECHA DE CREACION');
            $table->unsignedInteger('OBSE_LEGALES')->comment('ESTADO');
            $table->unsignedInteger('OBSE_TECNICAS')->comment('ESTADO');
            $table->unsignedInteger('TECN_ID')->comment('ESTADO');
            $table->unsignedInteger('EVAL_ID')->comment('ESTADO');
            $table->unsignedInteger('TIPO_ID')->comment('ESTADO');
            $table->unsignedInteger('OBSE_ACTIVO')->comment('ESTADO');
            $table->unsignedInteger('ESTD_ID')->comment('ESTADO');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(MetodosGenerales::$Esquema . 'observacion');
    }
}
