<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\ClasesExternas\MetodosGenerales;

class Fondo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    { 
        Schema::create(MetodosGenerales::$Esquema . 'fondo', function (Blueprint $table) {
            $table->id('FOND_ID')->comment('ID DE LA TABLA');
            $table->string('FOND_NOMBRE')->comment('NOMBRE DEL FONDO');
            $table->unsignedInteger('FOND_ACTIVO')->comment('ESTADO');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(MetodosGenerales::$Esquema . 'fondo');
    }
}
