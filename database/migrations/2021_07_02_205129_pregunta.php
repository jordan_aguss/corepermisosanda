<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Pregunta extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    { 
        Schema::create('pregunta', function (Blueprint $table) {
            $table->id('PREG_ID')->comment('Id de la pregunta');
            $table->unsignedInteger('SECT_ID')->comment('Id del sector de la pregunta');
            $table->string('PREG_NOMBRE')->comment('Nombre descriptivo');
            $table->unsignedInteger('TIPE_ID')->nullable()->comment('Tipo de entrada de la pregunta');
            $table->string('PREG_LONGITUD', 6)->comment('Longitud del campo');
            $table->unsignedInteger('PRED_ID')->nullable()->comment('Id de la pregunta padre');
            $table->unsignedInteger('CATE_ID')->comment('Id de la caracterizacion');
            $table->unsignedInteger('SECF_ID')->comment('Id seccion del formulario');
            $table->string('PREG_AYUDA')->comment('Dialogo de ayuda');
            $table->unsignedInteger('PREG_ORDEN')->comment('Orden de aparicion');
            $table->unsignedInteger('PREG_ACTIVO')->comment('Estado');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('pregunta');
    }
}
