<div class="card mb-4" id="body">
  <div class="card-header">Lista de Permisos</div>
    <div class="card-body">
        <div class="dataTable-top">
            <button class="btn btn-green" type="button" onclick="LoadPage('{{route('Proyecto.Form', [7])}}')">Nuevo&nbsp;&nbsp;<i class="fas fa-plus"></i></button>
            <div class="dataTable-search">
                <input onkeyup="if(event.keyCode == 13) MakeRequestData('{{route('buscar-permisos')}}' + '/' + $(this).val() , '#TablePageDivs', true)"  type="text" class="dataTable-input" placeholder="Buscar">
              </div>
            </div>

                <table class="table">

                  <header>
                      <th id="perm_id" name="0" onclick="MakeRequestData('{{route('ordenar-permisos')}}' + '/' + $(this).attr('name'), '#TablePageDivs', true)" > <i class="fas fa-sort-alt"></i> Nº</th>
                      <th id="name" name="1" onclick="MakeRequestData('{{route('ordenar-permisos')}}' + '/' + $(this).attr('name'), '#TablePageDivs', true)"  > <i class="fas fa-sort-alt"></i>Solicitante</th>
                      <th id="nombrePermiso" name="2" onclick="MakeRequestData('{{route('ordenar-permisos')}}' + '/' + $(this).attr('name'), '#TablePageDivs', true)"><i class="fas fa-sort-alt"></i>Nombre permiso</th>
                      <th>Proceso</th>
                      <th id="fecha" name="3" onclick="MakeRequestData('{{route('ordenar-permisos')}}' + '/' + $(this).attr('name'), '#TablePageDivs', true)"><i class="fas fa-sort-alt"></i>Fecha creación</th>
                      <th>Acciones</th>
                  </header>

                <tbody id="TablePageDivs">
                    @include('permiso.data-permisos')
                </tbody>
            </table>
        </div>
    </div>

</div>
